package project.raaz.alwalimasjid;

import java.util.List;

import project.raaz.alwalimasjid.pojo_class.Calendarinfo;
import project.raaz.alwalimasjid.pojo_class.Iftharmenu;
import project.raaz.alwalimasjid.pojo_class.MasjidInfoList;
import project.raaz.alwalimasjid.pojo_class.ReportList;
import project.raaz.alwalimasjid.pojo_class.RequestInfo;
import project.raaz.alwalimasjid.pojo_class.ShuttleList;

/**
 * Created by rasheed on 10/20/2017.
 */

public class MSG_N {

    private Integer success;
    private String message;
    private String status;
    private List<User> userinfo;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<User> getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(List<User> userinfo) {
        this.userinfo = userinfo;
    }
}
