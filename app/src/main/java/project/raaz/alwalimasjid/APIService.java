package project.raaz.alwalimasjid;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by rasheed on 10/20/2017.
 */

public interface APIService {

    //User Register 1
    @FormUrlEncoded
    @POST("register")
    Call<MSG> userSignUp(@Field("name") String name,
                         @Field("mobile") String mobile,
                         @Field("password") String password,
                         @Field("area") String area);

    //User Login 2
    @FormUrlEncoded
    @POST("login")
    Call<MSG> userLogIn(@Field("mobile") String mobile,
                        @Field("password") String password);

    //User ADD Requst & Offer 3
    @FormUrlEncoded
    @POST("addrequest")
    Call<MSG> userAddRequest(@Field("name") String name,
                             @Field("mobile") String mobile,
                             @Field("prayer") String prayer,
                             @Field("time") String time,
                             @Field("street") String street,
                             @Field("area") String area,
                             @Field("ride_type") String request_type,
                             @Field("seat") String seat);

    //List Requst & Offer 4
    @FormUrlEncoded
    @POST("requestlist")
    Call<MSG> userRequestList(@Field("ride_type") String ride_type,
                              @Field("limit") String limit,
                              @Field("offset") String offset);

    //List My Requst & Offer 5
    @FormUrlEncoded
    @POST("myrequest")
    Call<MSG> myRequestList(@Field("mobile") String mobile,
                            @Field("ride_type") String ride_type,
                            @Field("limit") String limit,
                            @Field("offset") String offset);

    //get Requst & Offer 6
    @FormUrlEncoded
    @POST("getrequest")
    Call<MSG> userSingleRequest(@Field("ride_type") String ride_type);

    // Accept Request 7
    @FormUrlEncoded
    @POST("acceptrequest")
    Call<MSG> acceptRequest(@Field("id") String id,
                            @Field("mobile") String mobile);

    //Add Report 8
    @Multipart
    @POST("addreport")
    Call<MSG> userReport(@Part("description") RequestBody description,
                         @Part MultipartBody.Part image);

    //List Report 9
    @FormUrlEncoded
    @POST("reportlist")
    Call<MSG> userReportList(@Field("limit") String limit,
                             @Field("offset") String offset);

    //List Event 10
    @FormUrlEncoded
    @POST("eventlist")
    Call<MSG> eventsList(@Field("masjid_id") String masjid_id,
                         @Field("limit") String limit,
                         @Field("offset") String offset);

    //List Prayer 11
    @FormUrlEncoded
    @POST("prayerlist")
    Call<MSG> prayerList(@Field("masjid_id") String masjid_id);

    //Update Prayer 12
    @FormUrlEncoded
    @POST("prayerupdate")
    Call<MSG> prayerUpdate(@Field("masjid_id") String masjid_id,
                           @Field("fajar") String fajir,
                           @Field("luzhar") String zhuhar,
                           @Field("azar") String azar,
                           @Field("maghrib") String magrib,
                           @Field("isha") String isha,
                           @Field("jumma") String jumma,
                           @Field("ifthar") String iftar);

    //Add Shuttle 13
    @FormUrlEncoded
    @POST("addshuttle")
    Call<MSG> busUpdate(@Field("masjid_id") String masjid_id,
                        @Field("startingpoint,") String startingpoint,
                        @Field("destination") String destination,
                        @Field("pickuppoint") String pickuppoint,
                        @Field("description") String description,
                        @Field("drivername") String drivername,
                        @Field("drivernumber") String driver_number,
                        @Field("eventname") String event_name,
                        @Field("eventplace") String event_place);

    //List Shuttle 14
    @FormUrlEncoded
    @POST("shuttlelist")
    Call<MSG> shuttleList(@Field("limit") String limit,
                          @Field("offset") String offset);

    //Add Event 15
    @FormUrlEncoded
    @POST("addevent")
    Call<MSG> eventsUpdate(@Field("masjid_id") String masjid_id,
                           @Field("event_title") String event_title,
                           @Field("description") String description,
                           @Field("date") String date,
                           @Field("time") String time,
                           @Field("location") String location,
                           @Field("landmark") String landmark,
                           @Field("image") String image);


    //Admin Register 16
    @FormUrlEncoded
    @POST("addmasjid")
    Call<MSG> addDefaultMasjid(@Field("masjid_name") String masjid_name,
                               @Field("random_id") String random_id,
                               @Field("address") String address,
                               @Field("latitude") String latitude,
                               @Field("longitude") String longitude,
                               @Field("unique_id") String unique_id,
                               @Field("place_id") String place_id);


    //Search Masjid 17
    @FormUrlEncoded
    @POST("searchmasjid")
    Call<MSG> addSearchMasjid(@Field("masjid_name") String masjid_name,
                              @Field("address") String address,
                              @Field("latitude") String latitude,
                              @Field("longitude") String longitude,
                              @Field("unique_id") String unique_id,
                              @Field("place_id") String place_id);

    //Check Masjid 18
    @FormUrlEncoded
    @POST("checkmasjid")
    Call<MSG> getMasjidInfo(@Field("random_id") String random_id);

    //Admin Register 19
    @FormUrlEncoded
    @POST("adminregister")
    Call<MSG> adminSignUp(@Field("user_id") String user_id,
                          @Field("masjid_id") String masjid_id,
                          @Field("mobile") String mobile,
                          @Field("email") String email,
                          @Field("password") String password,
                          @Field("area") String area);

    //Get User Request 20
    @FormUrlEncoded
    @POST("getrequest")
    Call<MSG> userRequestDetaile(@Field("id") String id);


    //Add Ramadan Masjid 21
    @FormUrlEncoded
    @POST("addramzanmasjid")
    Call<MSG> AddRamzanMasjid(@Field("masjid_name") String masjid_name,
                              @Field("address") String address,
                              @Field("latitude") String latitude,
                              @Field("longitude") String longitude,
                              @Field("unique_id") String unique_id,
                              @Field("place_id") String place_id);

    //Ramadan Calendar 22
    @FormUrlEncoded
    @POST("getramzancalendar")
    Call<MSG> getMasjidRamadanCalander(@Field("masjid_id") String masjid_id);


    //Add  Iftar 23
    @FormUrlEncoded
    @POST("addramzanifthar")
    Call<MSG> AddRamzanMasjidIftarCount(@Field("masjid_id") String masjid_id,
                                        @Field("fastingday") String fastingday,
                                        @Field("count") String count,
                                        @Field("name") String name,
                                        @Field("mobile") String mobile);

    //Get IftarDeatil 24
    @FormUrlEncoded
    @POST("getramzanifthardetails")
    Call<MSG> getMasjidIftarCount(@Field("masjid_id") String masjid_id,
                                  @Field("fastingday") String fastingday);

    //Get Ramadan Calendar 25
    @FormUrlEncoded
    @POST("getramzansharecalendar")
    Call<MSG> getMasjidRamadanShareCalander(@Field("masjid_id") String masjid_id);


    //Add Ramadan Share 26
    @FormUrlEncoded
    @POST("addramzanshare")
    Call<MSG> AddIftarShareAmount(@Field("masjid_id") String masjid_id,
                                  @Field("fastingday") String fastingday,
                                  @Field("amount") String amount,
                                  @Field("name") String longitude,
                                  @Field("mobile") String mobile,
                                  @Field("email") String email);

    //get Iftar Menu 27
    @FormUrlEncoded
    @POST("getramzaniftharmenu")
    Call<MSG> ViewRamadanMenu(@Field("masjid_id") String masjid_id,
                              @Field("fastingday") String fastingday);

    //Get Ramadan Event List 28
    @FormUrlEncoded
    @POST("getramzaneventlist")
    Call<MSG> userRamadanEvent(@Field("masjid_id") String masjid_id,
                               @Field("limit") String limit,
                               @Field("offset") String offset);

    //Add Ramadan Event 29
    @FormUrlEncoded
    @POST("addramzanevent")
    Call<MSG> ramadaneventsUpdate(@Field("masjid_id") String masjid_id,
                                  @Field("event_title") String event_title,
                                  @Field("description") String description,
                                  @Field("date") String date,
                                  @Field("time") String time,
                                  @Field("location") String location,
                                  @Field("chiefguest") String chiefguest,
                                  @Field("organizername") String organizername,
                                  @Field("organizernumber") String organizernumber);

    //Add Ramadan Iftar Menu 30
    @FormUrlEncoded
    @POST("addramzaniftharmenu")
    Call<MSG> ramadanIftarAddMenu(@Field("masjid_id") String str_masjid_id, @Field("fastingday") String str_day, @Field("menu") String str_items);

    //Get Ramadan prayer list 31
    @FormUrlEncoded
    @POST("ramzanprayerlist")
    Call<MSG> ramadanPrayerList(@Field("masjid_id") String masjid_id);

    //Add Prayer list 32
    @FormUrlEncoded
    @POST("ramzanprayerupdate")
    Call<MSG> ramadanPrayerUpdate(@Field("masjid_id") String masjid_id,
                                  @Field("fajar") String fajir,
                                  @Field("luzhar") String zhuhar,
                                  @Field("azar") String azar,
                                  @Field("maghrib") String magrib,
                                  @Field("isha") String isha,
                                  @Field("jumma") String jumma,
                                  @Field("ifthar") String iftar,
                                  @Field("sahar") String sahar,
                                  @Field("sunrise") String sunrise,
                                  @Field("sunset") String sunset,
                                  @Field("qiamul") String qiamul);

    //Add Iftar Daily Amount 33
    @FormUrlEncoded
    @POST("addramzanifthardailyamount")
    Call<MSG> ramadanIftarAmount(@Field("masjid_id") String masjid_id,
                                 @Field("fastingday") String fasting_day,
                                 @Field("amount") String amount);


    /// new Service

    // add offer
    @FormUrlEncoded
    @POST("addoffer")
    Call<MSG> AddOffer(@Field("user_id") String user_id,
                       @Field("startingpoint") String startingpoint,
                       @Field("destination") String destination,
                       @Field("pickuppoint") String pickuppoint,
                       @Field("prayer") String prayer,
                       @Field("date") String date,
                       @Field("time") String time,
                       @Field("totalseats") String totalseats);


    //Get All Offer / My Offer
    @FormUrlEncoded
    @POST("offerlist")
    Call<MSG> OfferList(@Field("user_id") String user_id,
                        @Field("limit") String limit,
                        @Field("offset") String offset);


    //View Offer
    @FormUrlEncoded
    @POST("viewoffer")
    Call<MSG> ViewOffer(@Field("id") String id);


    //Join Ride
    @FormUrlEncoded
    @POST("joinride")
    Call<MSG> JointRide(@Field("user_id") String user_id,
                        @Field("offerid") String offerid,
                        @Field("pickuppoint") String pickuppoint,
                        @Field("reqseats") String reqseats);


    // add Request
    @FormUrlEncoded
    @POST("addrequest")
    Call<MSG> AddRequest(@Field("user_id") String user_id,
                         @Field("startingpoint") String startingpoint,
                         @Field("destination") String destination,
                         @Field("pickuppoint") String pickuppoint,
                         @Field("prayer") String prayer,
                         @Field("date") String date,
                         @Field("time") String time,
                         @Field("reqseats") String totalseats);


    //Get All Offer / My Offer
    @FormUrlEncoded
    @POST("requestlist")
    Call<MSG> RequestList(@Field("user_id") String user_id,
                          @Field("limit") String limit,
                          @Field("offset") String offset);


    //View Offer
    @FormUrlEncoded
    @POST("viewrequest")
    Call<MSG> ViewRequest(@Field("id") String id);


    //Accept Request
    @FormUrlEncoded
    @POST("acceptrequest")
    Call<MSG> AcceptRequest(@Field("user_id") String user_id,
                            @Field("requestid") String requestid,
                            @Field("offerid") String offerid);


    //Get Offer Join Ride List

    @FormUrlEncoded
    @POST("offerjoinride")
    Call<MSG> offer_join_ride_info(@Field("offerid") String offerid,
                            @Field("rlimit") String limit,
                            @Field("offset") String offset);


    //profile info
    @FormUrlEncoded
    @POST("profileinfo")
    Call<MSG_N> getProfileInfo(@Field("user_id") String user_id);


    //Get Request Accept Offer Info
    @FormUrlEncoded
    @POST("requestacceptinfo")
    Call<MSG> request_join_ride_info(@Field("id") String id);



    //Profile Register
    @FormUrlEncoded
    @POST("register")
    Call<MSG> ProfileRegister(@Field("name") String name,
                              @Field("mobile") String mobile,
                              @Field("email") String email,
                              @Field("password") String password,
                              @Field("area") String area);

    //Profile Login
    @FormUrlEncoded
    @POST("login")
    Call<MSG> ProfileLogin(@Field("mobile") String mobile,
                           @Field("password") String password);

    //Admin Validation
    @FormUrlEncoded
    @POST("validateadmin")
    Call<MSG> AdminValidation(@Field("user_id") String user_id,
                                @Field("masjid_id") String masjid_id);


    //Jumma Time update
    @FormUrlEncoded
    @POST("addprayerjumma")
    Call<MSG> JummaUpdate(@Field("masjid_id") String masjid_id,
                              @Field("date") String date,
                          @Field("khutbah_time") String khutbah_time,
                          @Field("salah_time") String salah_time,
                          @Field("khateeb_name") String khateeb_name);


    //Jumma Time Info
    @FormUrlEncoded
    @POST("getprayerjumma")
    Call<MSG> JummaInfo(@Field("masjid_id") String masjid_id,
                          @Field("date") String date,
                          @Field("limit") String limit,
                          @Field("offset") String offset);


    //For Push notification
    @FormUrlEncoded
    @POST("device")
    Call<MSG> adddevice(@Field("user_id") String user_id,
                        @Field("token") String token,
                        @Field("device_type") String device_type);


}




