package project.raaz.alwalimasjid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.List;

import project.raaz.alwalimasjid.activities.CategoryAdmin;
import project.raaz.alwalimasjid.new_activities.AdminActivity;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 10/16/2017.
 */

public class Register extends AppCompatActivity {
    private static final String TAG = "Register";
    private ProgressDialog pDialog;
    TextInputLayout lName,lMobile,lPassword,lCPassword,lArea,lemail;
    TextInputEditText txtName,txtMobile,txtPassword,txtCPassword,txtArea,txtEmail;
    Button submit;
    Toolbar toolbar;
    String action;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        toolbar = (Toolbar) findViewById(R.id.register_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Profile");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Bundle bundle = getIntent().getExtras();
        action = bundle.getString("activity");

        submit = findViewById(R.id.reg_btn_submit);
        lName = findViewById(R.id.reg_lName);
        lMobile= findViewById(R.id.reg_lMobile);
        lemail = findViewById(R.id.reg_lmail);
        lPassword = findViewById(R.id.reg_lPassword);
        lCPassword = findViewById(R.id.reg_lCPassword);
        lArea = findViewById(R.id.reg_lArea);
        txtName = findViewById(R.id.reg_txtName);
        txtMobile = findViewById(R.id.reg_txtMobile);
        txtPassword = findViewById(R.id.reg_txtPassword);
        txtCPassword = findViewById(R.id.reg_txtCPassword);
        txtArea = findViewById(R.id.reg_txtArea);
        txtEmail = findViewById(R.id.reg_txtmail);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(Register.this,Login
//                        .class));
//                finish();
                signup();
            }
        });
    }
    public void signup() {
        Log.d(TAG, "Signup");

        if (validate() == false) {
            onSignupFailed();
            return;
        }

        saveToServerDB();
    }

    public void onSignupSuccess() {
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
    }


    public boolean validate() {
        boolean valid = true;

        String name = txtName.getText().toString();
        String mobile = txtMobile.getText().toString();
        String password = txtPassword.getText().toString();
        String reEnterPassword = txtCPassword.getText().toString();
        String area = txtArea.getText().toString();
        String mail = txtEmail.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            lName.setError("at least 3 characters");
            valid = false;
        } else {
            lName.setError(null);
        }


        if (mobile.isEmpty()) {
            lMobile.setError("enter a mobile number");
            valid = false;
        } else {
            lMobile.setError(null);
        }

        if(mail.isEmpty()){
            lemail.setError("enter email id");

        }else{
            lemail.setError(null);
        }


        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            lPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            lPassword.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 4 || reEnterPassword.length() > 10 || !(reEnterPassword.equals(password))) {
            lCPassword.setError("Password Do not match");
            valid = false;
        } else {
            lCPassword.setError(null);
        }

        if (area.isEmpty()) {
            lName.setError("Please Enter ther Area");
            valid = false;
        } else {
            lName.setError(null);
        }

        return valid;
    }



    private void saveToServerDB() {
        pDialog = new ProgressDialog(Register.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);

        showpDialog();

        final String name = txtName.getText().toString();
        final String mobile = txtMobile.getText().toString();
        String password = txtPassword.getText().toString();
        String area = txtArea.getText().toString();
        String mail = txtEmail.getText().toString();


        APIService service = ApiClinet.getClient().create(APIService.class);
        //User user = new User(name, email, password);


        Call<MSG> userCall = service.ProfileRegister(name,mobile,mail,password,area);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                    if(response.body().getStatus().equals("success")) {
                        List<UserValues> re = response.body().getUserinfo();
                        String user_id = re.get(0).user_id;
                        AppPreferences.setUserId(Register.this,user_id);
                        AppPreferences.setMobileNumber(Register.this,mobile);
                        AppPreferences.setUserName(Register.this,name);

                        callAddDevice();

                }else {
                    Toast.makeText(Register.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });


    }


    private void callAddDevice() {
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();


        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> usercall = service.adddevice(AppPreferences.getUserId(Register.this), refreshedToken, "android");

        usercall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {

                Log.d("onResponse", "" + response.body());
                if (response.body().getStatus().equals("success")) {
                    finish();
                } else {
                    finish();
                }

            }


            @Override
            public void onFailure(Call<MSG> call, Throwable t) {

            }
        });
    }



    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
