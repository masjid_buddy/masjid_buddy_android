package project.raaz.alwalimasjid;


import project.raaz.alwalimasjid.utilities.Prefs;

/**
 * Created by AndroidBash on 11/05/16
 */
public class CustomApplication extends android.app.Application {
    private Prefs prefs;
    private static CustomApplication app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        prefs = new Prefs(this);
    }

    public Prefs getPrefs() {
        return prefs;
    }

    public void setPrefs(Prefs prefs) {
        this.prefs = prefs;
    }

    public static CustomApplication getApp() {
        return app;
    }

    public static void setApp(CustomApplication app) {
        CustomApplication.app = app;
    }
}
