package project.raaz.alwalimasjid.new_fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.RequestList;
import project.raaz.alwalimasjid.new_activities.AcceptRequest;
import project.raaz.alwalimasjid.new_activities.AcceptRequestStatus;
import project.raaz.alwalimasjid.new_adapters.CarPoolingMyRequestListAdapter;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyRequest extends Fragment {
    private ProgressDialog pDialog;
    Activity activity;
    private RecyclerView recyclerView;
    Context context;
    Toolbar toolbar;
    public static MyRequest newInstance() {
        MyRequest fragment = new MyRequest();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_my_request, container, false);
        activity = getActivity();
        context = getContext();
        toolbar = (Toolbar) view.findViewById(R.id.join_ride_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);


        recyclerView = (RecyclerView) view.findViewById(R.id.my_request_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));

            callingRequest();

        // Inflate the layout for this fragment
        return view;
    }



    private void callingRequest() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);
        String str_mobile = AppPreferences.getMobileNumber(context);


        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.RequestList(AppPreferences.getUserId(activity),"","");

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<RequestList> requestlist = response.body().getRequestlist();

                    recyclerView.setAdapter(new CarPoolingMyRequestListAdapter(requestlist, R.layout.new_offer_list_itmes, context,MyRequest.this));





//                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else {
//                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public void GotoOfferInfo(String id) {
        Intent intent = new Intent(activity, AcceptRequestStatus.class);
        intent.putExtra("Id",id);
        startActivity(intent);
    }
}


