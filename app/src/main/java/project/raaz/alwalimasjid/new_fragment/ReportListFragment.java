/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package project.raaz.alwalimasjid.new_fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.ReportListActivity;
import project.raaz.alwalimasjid.activities.WebViewActivity;
import project.raaz.alwalimasjid.adapters.ReportListAdapter;
import project.raaz.alwalimasjid.pojo_class.ReportList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportListFragment extends Fragment {
    Activity activity;
    private RecyclerView recyclerView;
    private ProgressDialog pDialog;

    CardView car_parking,car_pooling,bus_shuttle,events,prayer_time,complaints;
    public static ReportListFragment newInstance() {
        ReportListFragment fragment = new ReportListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.report_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.report_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        callingReportList();
        return view;
    }
    private void callingReportList() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        String limit = "0",offset="0";
        Call<MSG> userCall = service.userReportList(limit,offset);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<ReportList> reportlist = response.body().getReportList();

                    recyclerView.setAdapter(new ReportListAdapter(reportlist, R.layout.repor_list_item, activity));

//                    Toast.makeText(ReportListFragment.this, "" + response.body().getReportList(), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void imageView(String link) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra("image_link",link);
        startActivity(intent);
    }

}
