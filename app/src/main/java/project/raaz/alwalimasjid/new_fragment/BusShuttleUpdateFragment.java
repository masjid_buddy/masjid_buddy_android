package project.raaz.alwalimasjid.new_fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/24/2017.
 */

public class BusShuttleUpdateFragment extends Fragment {
   Activity activity;
    private Toolbar toolbar;
    TextInputEditText vehicle,boading_place,stops,driver,number,event_name,event_place;
    TextView time;
    Button submit;
    private ProgressDialog pDialog;

    public static BusShuttleUpdateFragment newInstance() {
        BusShuttleUpdateFragment fragment = new BusShuttleUpdateFragment();
        return fragment;
    }
   @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.bus_shuttle_update, container, false);

        vehicle = (TextInputEditText) view.findViewById(R.id.shuttle_update_tiet_vname);
        boading_place = (TextInputEditText)view.findViewById(R.id.shuttle_update_tiet_boading_place);
        event_place = (TextInputEditText)view.findViewById(R.id.shuttle_update_tiet_event_place);
        stops = (TextInputEditText)view.findViewById(R.id.shuttle_update_tiet_stops);
        driver = (TextInputEditText)view.findViewById(R.id.shuttle_update_tiet_driver_name);
        number = (TextInputEditText)view.findViewById(R.id.shuttle_update_tiet_driver_mobile_numbe);
        event_name = (TextInputEditText)view.findViewById(R.id.shuttle_update_tiet_event_name);
        submit= (Button)view.findViewById(R.id.bus_shuttle_update_btn_submit);
        time = (TextView)view.findViewById(R.id.shuttle_update_txt_time);

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String format;
                        if (selectedHour == 0) {
                            selectedHour += 12;
                            format = "AM";

                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour == 12) {
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour > 12) {
                            selectedHour -= 12;
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else {
                            format = "AM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        }

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callWebservice();
            }
        });

        return view;
    }

    private void callWebservice() {
//        Toast.makeText(PrayerTimeUpdate.this, "Waiting For Webservice", Toast.LENGTH_SHORT).show();

        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);

        showpDialog();
        String str_masjid_id = AppPreferences.getMasjidId(activity);
        String str_vehicle = vehicle.getText().toString();
        String str_place = boading_place.getText().toString();
        String str_time = time.getText().toString();
        String str_stops = stops.getText().toString();
        String str_dname   = driver.getText().toString();
        String str_dnumber = number.getText().toString();
        String str_event = event_name.getText().toString();
        String str_event_place = event_place.getText().toString();


        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.busUpdate(str_masjid_id,str_vehicle,str_place,str_time,str_stops,str_dname,str_dnumber,str_event,str_event_place);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });



    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



}
