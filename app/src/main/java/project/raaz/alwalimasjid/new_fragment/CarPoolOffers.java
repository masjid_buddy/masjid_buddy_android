/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package project.raaz.alwalimasjid.new_fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.Report;
import project.raaz.alwalimasjid.activities.BusShuttleListActivity;
import project.raaz.alwalimasjid.activities.CarParkingMapMyMasjid;
import project.raaz.alwalimasjid.activities.EventListActivity;
import project.raaz.alwalimasjid.activities.PrayerActivity;
import project.raaz.alwalimasjid.fagments.CarPoolingOfferRide;
import project.raaz.alwalimasjid.fagments.CarPoolingRequestRide;
import project.raaz.alwalimasjid.fagments.MyRequest;
import project.raaz.alwalimasjid.fagments.PoolingListOfferRide;
import project.raaz.alwalimasjid.fagments.PoolingListRequestRide;
import project.raaz.alwalimasjid.new_activities.CarPoolActivity;

public class CarPoolOffers extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Activity activity;
    public static CarPoolOffers newInstance() {
        CarPoolOffers fragment = new CarPoolOffers();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.car_pooling_fragment, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.carpool_fragment_viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.carpool_fragment_tabs);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }


    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {


        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new PoolingListOfferRide(), "Offers");
        adapter.addFragment(new PoolingListRequestRide(), "Requests");
        viewPager.setAdapter(adapter);

    }
    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



}
