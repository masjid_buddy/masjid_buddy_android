/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package project.raaz.alwalimasjid.new_fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.Report;
import project.raaz.alwalimasjid.activities.BusShuttleListActivity;
import project.raaz.alwalimasjid.activities.CarParkingMapMyMasjid;
import project.raaz.alwalimasjid.activities.EventListActivity;
import project.raaz.alwalimasjid.activities.PrayerActivity;
import project.raaz.alwalimasjid.activities.RamadanEventListActivity;
import project.raaz.alwalimasjid.activities.RamadanHome;
import project.raaz.alwalimasjid.activities.RamadanPrayerTimeUpdate;
import project.raaz.alwalimasjid.new_activities.CarPoolActivity;
import project.raaz.alwalimasjid.new_activities.JummaUpdate;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IqamaTimeUpdate extends Fragment implements View.OnClickListener {
    Activity activity;
    TextView fajir, zuhr,ashar,magrib,isha,jummah;
    Button submit;
    private ProgressDialog pDialog;
    LinearLayout jummaInfo;

    int FLAG_FAJIR = 1,
            FLAG_ZHUHAR = 2,
            FLAG_AZAR = 3,
            FLAG_MAGRIB = 4,
            FLAG_ISHA = 5,
            FLAG_JUMMA = 6,
            FLAG_QIYAMUL = 7,
            FLAG_SUHOOR = 8,
            FLAG_IFTAR = 9,
            FLAG_SUNSET = 10,
            FLAG_SUNRISE = 11;    public static IqamaTimeUpdate newInstance() {
        IqamaTimeUpdate fragment = new IqamaTimeUpdate();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.iqama_time_update, container, false);
        fajir =view.findViewById(R.id.iqama_fajir_update);
        zuhr = view.findViewById(R.id.iqama_zuhr_update);
        ashar = view.findViewById(R.id.iqama_asr_update);
        magrib = view.findViewById(R.id.iqama_magrib_update);
        isha = view.findViewById(R.id.iqama_isha_update);
        jummah = view.findViewById(R.id.iqama_jumuah_update);
        submit = view.findViewById(R.id.iqama_update_submit);
        jummaInfo = view.findViewById(R.id.jumma_info_btn);

        fajir.setOnClickListener(this);
        zuhr.setOnClickListener(this);
        ashar.setOnClickListener(this);
        magrib.setOnClickListener(this);
        isha.setOnClickListener(this);
        jummah.setOnClickListener(this);
        submit.setOnClickListener(this);
        jummaInfo.setOnClickListener(this);
        return view;
    }
    private void callWebservice() {
//        Toast.makeText(PrayerTimeUpdate.this, "Waiting For Webservice", Toast.LENGTH_SHORT).show();

        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Updating...");
        pDialog.setCancelable(false);

        showpDialog();
        String str_masjid_id = AppPreferences.getMasjidId(activity);
        String str_fajir = fajir.getText().toString();
        String str_zhuhar = zuhr.getText().toString();
        String str_azar = ashar.getText().toString();
        String str_magrib = magrib.getText().toString();
        String str_isha   = isha.getText().toString();
        String str_jumma = jummah.getText().toString();



        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.prayerUpdate(str_masjid_id,str_fajir,str_zhuhar,str_azar,str_magrib,str_isha,str_jumma,"");

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });



    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.iqama_fajir_update:
                timePicker(FLAG_FAJIR);
                break;
            case R.id.iqama_zuhr_update:
                timePicker(FLAG_ZHUHAR);
                break;
            case R.id.iqama_asr_update:
                timePicker(FLAG_AZAR);
                break;
            case R.id.iqama_magrib_update:
                timePicker(FLAG_MAGRIB);
                break;
            case R.id.iqama_isha_update:
                timePicker(FLAG_ISHA);
                break;
            case R.id.iqama_jumuah_update:
                timePicker(FLAG_JUMMA);
                break;
            case R.id.iqama_update_submit:
                callWebservice();
                break;
            case R.id.jumma_info_btn:
                startActivity(new Intent(activity, JummaUpdate.class));
                activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
        }
    }

    private void timePicker(final int FLAG) {
        // TODO Auto-generated method stub

        final Calendar[] mcurrentTime = {Calendar.getInstance()};
        int hour = mcurrentTime[0].get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime[0].get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                switch (FLAG){
                    case 1:
                        fajir.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 2:
                        zuhr.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                    case 3:
                        ashar.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                    case 4:
                        magrib.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                    case 5:
                        isha.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 6:
                        jummah.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                }



            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }


    public String getTimeFormate(int selectedHour, int selectedMinute) {
        String format;
        String time;
        if (selectedHour == 0) {
            selectedHour += 12;
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour == 12) {
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour > 12) {
            selectedHour -= 12;
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else {
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        }
        return time;
    }
}
