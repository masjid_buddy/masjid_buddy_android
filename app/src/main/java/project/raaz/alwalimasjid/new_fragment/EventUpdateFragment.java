package project.raaz.alwalimasjid.new_fragment;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.Report;
import project.raaz.alwalimasjid.activities.BusShuttleListActivity;
import project.raaz.alwalimasjid.activities.CarParkingMapMyMasjid;
import project.raaz.alwalimasjid.activities.EventListActivity;
import project.raaz.alwalimasjid.activities.EventsUpdate;
import project.raaz.alwalimasjid.activities.PrayerActivity;
import project.raaz.alwalimasjid.new_activities.CarPoolActivity;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventUpdateFragment extends Fragment {
    Activity activity;
    TextInputEditText event_name,place,organizer;
    Button submit;
    TextView time,date;
    EditText description;

    private ProgressDialog pDialog;

    private int mYear, mMonth, mDay, mHour, mMinute;

    public static EventUpdateFragment newInstance() {
        EventUpdateFragment fragment = new EventUpdateFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.new_event_update, container, false);
        event_name = view.findViewById(R.id.fragment_event_update_tiet_Name);
        description = view.findViewById(R.id.fragment_evet_description);
        place = (TextInputEditText)view.findViewById(R.id.fragment_event_update_tiet_Location);
        date = (TextView) view.findViewById(R.id.fragment_event_update_txt_date);
        organizer = (TextInputEditText)view.findViewById(R.id.fragment_event_update_tiet_landmark);
        time = (TextView)view.findViewById(R.id.fragment_event_update_txt_time);
        submit = (Button)view.findViewById(R.id.fragmet_event_update);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callWebservice();
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String format;
                        if (selectedHour == 0) {
                            selectedHour += 12;
                            format = "AM";

                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour == 12) {
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour > 12) {
                            selectedHour -= 12;
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else {
                            format = "AM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        }


                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        return view;
    }

    private void callWebservice() {
//        Toast.makeText(PrayerTimeUpdate.this, "Waiting For Webservice", Toast.LENGTH_SHORT).show();

        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);

        showpDialog();
        String str_masjid_id = AppPreferences.getMasjidId(activity);
        String str_event = event_name.getText().toString();
        String str_description = description.getText().toString();
        String str_date = date.getText().toString();
        String str_time = time.getText().toString();
        String str_place = place.getText().toString();
        String str_organizer =organizer.getText().toString();
        String image = null;



        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.eventsUpdate(str_masjid_id,str_event,str_description,str_date,str_time,str_place,"none",str_organizer);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body());


                if(response.body().getStatus().equals("success")) {

                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });



    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
