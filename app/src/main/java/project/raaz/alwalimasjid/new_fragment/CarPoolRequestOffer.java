/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package project.raaz.alwalimasjid.new_fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import mabbas007.tagsedittext.TagsEditText;
import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.activities.CarPoolingOfferRide;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarPoolRequestOffer extends Fragment implements View.OnClickListener {
    Activity activity;
    private Button[] btn = new Button[6];
    private Button btn_unfocus;
    private int[] btn_id = {R.id.btn_fajr, R.id.btn_zhur, R.id.btn_asr, R.id.btn_mahrib,R.id.btn_isha,R.id.btn_jumma};

    private Button[] btn_seat = new Button[6];
    private int[] btn_id_seat = {R.id.btn_one, R.id.btn_two, R.id.btn_three, R.id.btn_four,R.id.btn_five,R.id.btn_six};
    private Button btn_unfocus_seat;


    TextView start,end,startTime,startDate;
    Button request,offer;

    String Waqth="Fajir",Seat="1";
    private ProgressDialog pDialog;
    TagsEditText tagText;


    public static CarPoolRequestOffer newInstance() {
        CarPoolRequestOffer fragment = new CarPoolRequestOffer();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.new_car_pooling_request_offer, container, false);
        start =view.findViewById(R.id.my_location);
        end = view.findViewById(R.id.my_destination);
        startTime = view.findViewById(R.id.start_time);
        startDate =view.findViewById(R.id.start_date);
        request = view.findViewById(R.id.new_btn_carpool_request);
        offer = view.findViewById(R.id.new_btn_carpool_offer);
        tagText = view.findViewById(R.id.tagsEditText);





        for(int i = 0; i < btn.length; i++){
            btn[i] = (Button) view.findViewById(btn_id[i]);
            btn[i].setBackgroundColor(Color.rgb(207, 207, 207));
            btn[i].setOnClickListener(this);

            btn_seat[i] = (Button) view.findViewById(btn_id_seat[i]);
            btn_seat[i].setBackgroundColor(Color.rgb(207, 207, 207));
            btn_seat[i].setOnClickListener(this);

        }

        for(int i = 0; i < btn_seat.length; i++){
            btn_seat[i] = (Button) view.findViewById(btn_id_seat[i]);
            btn_seat[i].setBackgroundColor(Color.rgb(207, 207, 207));
            btn_seat[i].setOnClickListener(this);

        }



        btn_unfocus = btn[0];
        btn_unfocus_seat = btn_seat[0];

        startTime.setOnClickListener(this);
        offer.setOnClickListener(this);
        request.setOnClickListener(this);
        startDate.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_fajr :
                setFocus(btn_unfocus, btn[0]);
                Waqth = "Fajir";
                break;

            case R.id.btn_zhur :
                setFocus(btn_unfocus, btn[1]);
                Waqth = "Zhur";

                break;

            case R.id.btn_asr :
                setFocus(btn_unfocus, btn[2]);
                Waqth = "Asr";

                break;

            case R.id.btn_mahrib :
                setFocus(btn_unfocus, btn[3]);
                Waqth = "Mahrib";

                break;
            case R.id.btn_isha :
                setFocus(btn_unfocus, btn[4]);
                Waqth = "Isha";

                break;
            case R.id.btn_jumma :
                setFocus(btn_unfocus, btn[5]);
                Waqth = "Jumma";

                break;
            case R.id.btn_one:
                setFocus_seat(btn_unfocus_seat, btn_seat[0]);
                Seat = "1";
                break;
            case R.id.btn_two:
                setFocus_seat(btn_unfocus_seat, btn_seat[1]);
                Seat = "2";
                break;
            case R.id.btn_three:
                setFocus_seat(btn_unfocus_seat, btn_seat[2]);
                Seat = "3";
                break;
            case R.id.btn_four:
                setFocus_seat(btn_unfocus_seat, btn_seat[3]);
                Seat = "4";
                break;
            case R.id.btn_five:
                setFocus_seat(btn_unfocus_seat, btn_seat[4]);
                Seat = "5";
                break;
            case R.id.btn_six:
                setFocus_seat(btn_unfocus_seat, btn_seat[5]);
                Seat = "6";
                break;

            case R.id.new_btn_carpool_offer:
                WebService(1);

                break;

            case R.id.new_btn_carpool_request:
                WebService(2);
                break;

            case R.id.start_time:
                openDateDialog();
                break;
            case R.id.start_date:
                datePickerDialog();
                break;
        }

    }
    private void WebService(int type) {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Request...");
        pDialog.setCancelable(false);

        showpDialog();

        final String str_prayer;
        final String str_time;
        final String str_startPoint;
        final String str_destination;
        final String str_seat;
        final String str_date;
        String str_picUp="";


        String user_id = AppPreferences.getUserId(activity);
        str_time = startTime.getText().toString();
        str_startPoint = start.getText().toString();
        str_destination = end.getText().toString();
        str_seat = Seat;
        str_prayer = Waqth;
        str_date = startDate.getText().toString();


        if(type==1){

            if(!tagText.getTags().isEmpty()){
                int size = tagText.getTags().size();
                str_picUp = tagText.getTags().get(0);

                for (int i = 1; i<size;i++){
                    str_picUp = str_picUp+":"+tagText.getTags().get(i);
                }
            }else{
                Toast.makeText(activity, "Please enter pickup point", Toast.LENGTH_SHORT).show();
            }

        }else{
            if(!tagText.getTags().isEmpty()){
                str_picUp = tagText.getTags().get(0);
                }else{
                Toast.makeText(activity, "Please enter pickup point", Toast.LENGTH_SHORT).show();
            }

        }



        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall;
        if(type==1) {
            userCall = service.AddOffer(user_id, str_startPoint, str_destination, str_picUp, str_prayer, str_date, str_time, str_seat);
        }else{
            userCall = service.AddRequest(user_id, str_startPoint, str_destination, str_picUp, str_prayer, str_date, str_time, str_seat);

        }
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    activity.finish();



                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });
    }

    private void datePickerDialog() {
        // Process to get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        // Launch Date Picker Dialog
        DatePickerDialog datePickerDialog = new DatePickerDialog(activity,android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        startDate.setText((monthOfYear + 1)+ "-"
                                + dayOfMonth  + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        datePickerDialog.show();
    }

    private void openDateDialog() {
        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String format;
                if (selectedHour == 0) {
                    selectedHour += 12;
                    format = "AM";

                    startTime.setText( selectedHour + ":" + selectedMinute+" "+format);
                } else if (selectedHour == 12) {
                    format = "PM";
                    startTime.setText( selectedHour + ":" + selectedMinute+" "+format);
                } else if (selectedHour > 12) {
                    selectedHour -= 12;
                    format = "PM";
                    startTime.setText( selectedHour + ":" + selectedMinute+" "+format);
                } else {
                    format = "AM";
                    startTime.setText( selectedHour + ":" + selectedMinute+" "+format);
                }


            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void requestByServer(String ride_type) {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Request...");
        pDialog.setCancelable(false);

        showpDialog();

        final String str_prayer, str_time, str_street, str_area, str_request_type,str_user_name,str_user_mobile,str_seat;



        str_time = startTime.getText().toString();
        str_street = start.getText().toString();
        str_area = end.getText().toString();
        str_user_name = AppPreferences.getUserName(activity);
        str_user_mobile = AppPreferences.getMobileNumber(activity);
        str_seat = Seat;
        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.userAddRequest(str_user_name,str_user_mobile,Waqth, str_time, str_street, str_area, ride_type,str_seat);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    AppPreferences.setMobileNumber(activity,str_user_mobile);
                    activity.finish();



                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });
    }




    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void setFocus(Button btn_unfocus, Button btn_focus){
        btn_unfocus.setTextColor(Color.rgb(49, 50, 51));
        btn_unfocus.setBackgroundColor(Color.rgb(207, 207, 207));
        btn_focus.setTextColor(Color.rgb(255, 255, 255));
        btn_focus.setBackgroundColor(Color.rgb(3, 106, 150));
        this.btn_unfocus = btn_focus;
    }

    private void setFocus_seat(Button btn_unfocus_seat, Button btn_focus_seat){
        btn_unfocus.setTextColor(Color.rgb(49, 50, 51));
        btn_unfocus.setBackgroundColor(Color.rgb(207, 207, 207));
        btn_focus_seat.setTextColor(Color.rgb(255, 255, 255));
        btn_focus_seat.setBackgroundColor(Color.rgb(3, 106, 150));
        this.btn_unfocus_seat = btn_focus_seat;
    }


}
