/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package project.raaz.alwalimasjid.new_fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import project.raaz.alwalimasjid.Login;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.Register;
import project.raaz.alwalimasjid.Report;
import project.raaz.alwalimasjid.activities.BusShuttleListActivity;
import project.raaz.alwalimasjid.activities.CarParkingMapMyMasjid;
import project.raaz.alwalimasjid.activities.EventListActivity;
import project.raaz.alwalimasjid.activities.PrayerActivity;
import project.raaz.alwalimasjid.activities.RamadanHome;
import project.raaz.alwalimasjid.activities.RamadanPrayerActivity;
import project.raaz.alwalimasjid.new_activities.AdminActivity;
import project.raaz.alwalimasjid.new_activities.CarPoolActivity;
import project.raaz.alwalimasjid.new_activities.MasjidActivity;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTime;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeApiService;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment implements View.OnClickListener {
    Activity activity;
    CardView car_parking,car_pooling,bus_shuttle,events,prayer_time,complaints;
    TextView majid_name,masjid_place;
    private ProgressDialog pDialog;
    TextView sun_set,sun_rise,islamic_month,islamic_day_year,day,day_year;
    LinearLayout ramadan_handbook;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        View view = inflater.inflate(R.layout.new_fragment_home, container, false);
        car_parking = view.findViewById(R.id.home_carparking);
        car_pooling = view.findViewById(R.id.home_carpooling);
        bus_shuttle = view.findViewById(R.id.home_shuttleservice);
        events = view.findViewById(R.id.home_events);
        prayer_time = view.findViewById(R.id.home_prayertime);
        complaints = view.findViewById(R.id.home_camplaints);
        majid_name = view.findViewById(R.id.home_fragment_masjid_name);
        masjid_place = view.findViewById(R.id.home_fragment_masjid_place);
        sun_set = view.findViewById(R.id.home_sun_set);
        sun_rise = view.findViewById(R.id.home_sun_rise);
        islamic_month = view.findViewById(R.id.home_islamic_month);
        islamic_day_year = view.findViewById(R.id.home_islamic_day_year);
        day = view.findViewById(R.id.home_day);
        day_year = view.findViewById(R.id.home_day_year);

        ramadan_handbook = view.findViewById(R.id.btn_ramadan2019);

        car_parking.setOnClickListener(this);
        car_pooling.setOnClickListener(this);
        bus_shuttle.setOnClickListener(this);
        events.setOnClickListener(this);
        prayer_time.setOnClickListener(this);
        complaints.setOnClickListener(this);
        majid_name.setText(AppPreferences.getMajidName(activity));
        masjid_place.setText(AppPreferences.getMajidAddress(activity));
        getPrayerTime();


        ramadan_handbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, RamadanHome
                        .class).putExtra("lat",AppPreferences.getLatitiude(activity)).putExtra("long",AppPreferences.getLogtitude(activity)));
                activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        return view;
    }

    @Override
    public void onClick(View view) {
     switch (view.getId()){
         case R.id.home_carparking:
             startActivity(new Intent(activity,CarParkingMapMyMasjid
                     .class));
             activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
             break;
         case R.id.home_carpooling:
             if (AppPreferences.getUserId(activity).equals("")){
                 startActivity(new Intent(activity,Login.class).putExtra("activity","carpool"));
             }else{
                 startActivity(new Intent(activity,CarPoolActivity
                         .class));
                 activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

             }
            break;
         case R.id.home_shuttleservice:
             startActivity(new Intent(activity,BusShuttleListActivity
                     .class));
             activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
             break;
         case R.id.home_events:
             startActivity(new Intent(activity,EventListActivity
                     .class));
             activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
             break;
         case R.id.home_prayertime:
             startActivity(new Intent(activity,PrayerActivity
                     .class));
             activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
             break;
         case R.id.home_camplaints:
             startActivity(new Intent(activity,Report
                     .class));
             activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
             break;

     }
    }

    public void getPrayerTime() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting Data...");
        pDialog.setCancelable(false);
        showpDialog();
        String ENDPOINT = "http://api.aladhan.com/";

        final PrayerTimeApiService mService;



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mService = retrofit.create(PrayerTimeApiService.class);

        String Lattiude = AppPreferences.getLatitiude(activity);
        String Logtitude = AppPreferences.getLogtitude(activity);


        mService.getPrayerTime(Lattiude,Logtitude,"2")
                .enqueue(new Callback<PrayerTime>() {


                    @Override
                    public void onResponse(Call<PrayerTime> call, Response<PrayerTime> response) {
                        hidepDialog();
                        Log.v("OnRsponsePrayerTime",response.body().toString());



                        String[] str_wsunset = response.body().getData().getTimings().getSunset().split(":");
                        int wsunsetHours = Integer.parseInt(str_wsunset[0]);
                        String wsunsetMinutes = str_wsunset[1];

                        String[] str_wsunrise = response.body().getData().getTimings().getSunrise().split(":");
                        int wsunriseHours = Integer.parseInt(str_wsunrise[0]);
                        String wsunriseMinutes = str_wsunrise[1];

                        String str_islamic_month = response.body().getData().getDate().getHijri().getMonth().getEn();
                        String str_islamic_day = response.body().getData().getDate().getHijri().getDay();
                        String str_islamic_year = response.body().getData().getDate().getHijri().getYear();
                        String str_weekday = response.body().getData().getDate().getGregorian().getWeekday().getEn();
                        String str_day = response.body().getData().getDate().getGregorian().getDay();
                        String str_year = response.body().getData().getDate().getGregorian().getYear();

                        sun_set.setText(getTimeFormate(wsunsetHours,wsunsetMinutes));
                        sun_rise.setText(getTimeFormate(wsunriseHours,wsunriseMinutes));
                        islamic_month.setText(str_islamic_month);
                        islamic_day_year.setText(str_islamic_day+"/"+str_islamic_year);
                        day.setText(str_weekday);
                        day_year.setText(str_day+"/"+str_year);
                    }

                    @Override
                    public void onFailure(Call<PrayerTime> call, Throwable t) {
                        hidepDialog();

                    }

                });
    }


    public String getTimeFormate(int selectedHour, String selectedMinute) {
        String format;
        String time;
        if (selectedHour == 0) {
            selectedHour += 12;
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour == 12) {
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour > 12) {
            selectedHour -= 12;
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else {
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        }
        return time;
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
