package project.raaz.alwalimasjid;

import android.widget.ListView;

import java.util.List;

import project.raaz.alwalimasjid.pojo_class.Calendarinfo;
import project.raaz.alwalimasjid.pojo_class.Iftharmenu;
import project.raaz.alwalimasjid.pojo_class.MasjidInfoList;
import project.raaz.alwalimasjid.pojo_class.PrayerInfo;
import project.raaz.alwalimasjid.pojo_class.ReportList;
import project.raaz.alwalimasjid.pojo_class.RequestInfo;
import project.raaz.alwalimasjid.pojo_class.ShuttleList;

/**
 * Created by rasheed on 10/20/2017.
 */

public class MSG {

    private Integer success;
    private String message;
    private String status;
    private String iftharcount;
    private List<UserValues> userinfo;
    private List<RequestList> requestlist;
    private List<ReportList>  reportlist;
    private List<EventsList>  eventlist;
    private List<PrayerList> prayerlist;
    private List<MasjidInfoList>masjidinfo;
    private List<ShuttleList>shuttlelist;
    private List<RequestInfo>requestinfo;
    private List<Calendarinfo>calendarinfo;
    private List<Iftharmenu>iftharmenu;
    private List<OfferList>offerlist;
    private List<OfferInfo> offerinfo;
    private List<PrayerInfo> prayerinfo;




    String user_id;
    /**
     * No args constructor for use in serialization
     */
    public MSG() {
    }


    public List<RequestList> getRequestlist() {
        return requestlist;
    }

    public void setRequestlist(List<RequestList> requestlist) {
        this.requestlist = requestlist;
    }

    /**
     * @param message
     * @param success
     */
    public MSG(Integer success, String message, String status, String iftharcount, List<UserValues> userinfo, List<RequestList> requestlist, List<ReportList> reportlist, List<EventsList>  eventlist, List<PrayerList> prayerlist, List<MasjidInfoList> masjidinfo, List<RequestInfo> requestinfo,
               String user_id,List<Calendarinfo> calendarinfo,List<Iftharmenu> iftharmenu,List<PrayerInfo>prayerinfo) {
        super();
        this.success = success;
        this.message = message;
        this.status = status;
        this.userinfo = userinfo;
        this.user_id = user_id;
        this.requestlist = requestlist;
        this.reportlist = reportlist;
        this.eventlist = eventlist;
        this.prayerlist = prayerlist;
        this.masjidinfo = masjidinfo;
        this.calendarinfo=calendarinfo;
        this.iftharcount=iftharcount;
        this.iftharmenu=iftharmenu;
        this.prayerinfo=prayerinfo;

    }

    public List<Iftharmenu> getIftharmenu() {
        return iftharmenu;
    }

    public void setIftharmenu(List<Iftharmenu> iftharmenu) {
        this.iftharmenu = iftharmenu;
    }

    public String getIftharcount() {
        return iftharcount;
    }

    public void setIftharcount(String iftharcount) {
        this.iftharcount = iftharcount;
    }

    public List<Calendarinfo> getCalendarinfo() {
        return calendarinfo;
    }

    public void setCalendarinfo(List<Calendarinfo> calendarinfo) {
        this.calendarinfo = calendarinfo;
    }

    public List<ReportList> getReportlist() {
        return reportlist;
    }

    public void setReportlist(List<ReportList> reportlist) {
        this.reportlist = reportlist;
    }

    public List<MasjidInfoList> getMasjidinfo() {
        return masjidinfo;
    }

    public void setMasjidinfo(List<MasjidInfoList> masjidinfo) {
        this.masjidinfo = masjidinfo;
    }

    public List<PrayerList> getPrayerlist() {
        return prayerlist;
    }

    public void setPrayerlist(List<PrayerList> prayerlist) {
        this.prayerlist = prayerlist;
    }

    public List<EventsList> getEventlist() {
        return eventlist;
    }

    public void setEventlist(List<EventsList> eventlist) {
        this.eventlist = eventlist;
    }

    public String getUser_id() {
        return user_id;
    }

    public List<UserValues> getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(List<UserValues> userinfo) {
        this.userinfo = userinfo;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ReportList> getReportList() {
        return reportlist;
    }

    public void setReportList(List<ReportList> reportList) {
        this.reportlist = reportList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ShuttleList> getShuttlelist() {
        return shuttlelist;
    }

    public void setShuttlelist(List<ShuttleList> shuttlelist) {
        this.shuttlelist = shuttlelist;
    }

    public List<RequestInfo> getRequestinfo() {
        return requestinfo;
    }

    public void setRequestinfo(List<RequestInfo> requestinfo) {
        this.requestinfo = requestinfo;
    }


    public List<OfferList> getOfferlist() {
        return offerlist;
    }

    public List<OfferInfo> getOfferinfo() {
        return offerinfo;
    }

    public void setOfferinfo(List<OfferInfo> offerinfo) {
        this.offerinfo = offerinfo;
    }

    public void setOfferlist(List<OfferList> offerlist) {
        this.offerlist = offerlist;
    }

    public List<PrayerInfo> getPrayerinfo() {
        return prayerinfo;
    }

    public void setPrayerinfo(List<PrayerInfo> prayerinfo) {
        this.prayerinfo = prayerinfo;
    }
}
