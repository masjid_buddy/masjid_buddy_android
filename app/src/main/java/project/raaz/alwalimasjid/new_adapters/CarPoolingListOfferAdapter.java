package project.raaz.alwalimasjid.new_adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.OfferList;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.RequestList;
import project.raaz.alwalimasjid.fagments.PoolingListOfferRide;

public class CarPoolingListOfferAdapter extends RecyclerView.Adapter<CarPoolingListOfferAdapter.ViewHolder> {

    private List<OfferList> rq_list;
    private int rowLayout;
    private Context context;
    PoolingListOfferRide myFragment;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout listLayout;
        TextView source;
        TextView destination;
        TextView picup_points;
        TextView time;
        TextView prayer;
        Button join;


        public ViewHolder(View v) {
            super(v);
            listLayout =  v.findViewById(R.id.new_offer_list_items_layout);
            source = v.findViewById(R.id.new_offer_list_items_source);
            destination =v.findViewById(R.id.new_offer_list_items_destination);
            picup_points = v.findViewById(R.id.new_offer_list_items_pickup_poits);
            time = v.findViewById(R.id.new_offer_list_items_join_btn);
            prayer = v.findViewById(R.id.new_offer_list_items_prayer);
            join = v.findViewById(R.id.new_offer_list_items_join_btn);


        }
    }

    public CarPoolingListOfferAdapter(List<OfferList> rq_list, int rowLayout, Context context, PoolingListOfferRide myFragment) {
        this.rq_list = rq_list;
        this.rowLayout = rowLayout;
        this.context = context;
        this.myFragment = myFragment;
    }

    @Override
    public CarPoolingListOfferAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                    int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final String  Prayer =  rq_list.get(position).getPrayer().toString();
        final String Source =  rq_list.get(position).getStartingpoint();
        final String destination =  rq_list.get(position).getDestination();
        final String Seats =  rq_list.get(position).getTotalseats();
        final String id =rq_list.get(position).getId().toString();
        final  String available_seats = rq_list.get(position).getAvailableseat();
//        final String Accept_status = rq_list.get(position).getAccept_status().toString();
//        final String Accepted_by = rq_list.get(position).getAccepted_by().toString();
        String picupPoints = rq_list.get(position).getPickuppoint();

        if(picupPoints.equals("")){
            String[] picupList = picupPoints.split(":");
            holder.picup_points.setText(" No Pickup Point");
        }else {
            String[] picupList = picupPoints.split(":");
            holder.picup_points.setText(" "+picupList.length + " Pickup Point");

        }

        holder.source.setText(Source);
        holder.destination.setText(destination);
        holder.prayer.setText(Prayer);






//        int available_seat = Integer.parseInt(Seats);
//        for(int i=0;i<available_seat;i++){
//            holder.available_seat[i].setVisibility(View.VISIBLE);
//        }


        if (available_seats.equals("0")){
            holder.join.setBackgroundColor(0xFF9196A7);
        }
        holder.join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (available_seats.equals("0")){
                    Toast.makeText(context, "No more seats to join", Toast.LENGTH_SHORT).show();
                }else{
                    myFragment.GotoDetaile(id);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return rq_list.size();
    }
}