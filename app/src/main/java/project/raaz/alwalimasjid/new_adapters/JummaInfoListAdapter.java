package project.raaz.alwalimasjid.new_adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.pojo_class.PrayerInfo;
import project.raaz.alwalimasjid.pojo_class.ReportList;

public class JummaInfoListAdapter extends RecyclerView.Adapter<JummaInfoListAdapter.ViewHolder> {

    private List<PrayerInfo> rp_list;
    private int rowLayout;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView listLayout;
        TextView Date;
        TextView salaTime;
        TextView KuthubaTime;
        TextView KatheemName;


        public ViewHolder(View v) {
            super(v);
            listLayout = (CardView) v.findViewById(R.id.new_masjid_jumma_layout);
            Date = (TextView) v.findViewById(R.id.new_masjid_jumma_item_date);
            salaTime = (TextView) v.findViewById(R.id.txt_salah_time);
            KuthubaTime = (TextView) v.findViewById(R.id.txt_khutbah_time);
            KatheemName = (TextView) v.findViewById(R.id.txt_khateeb_name);
        }
    }

    public JummaInfoListAdapter(List<PrayerInfo> movies, int rowLayout, Context context) {
        this.rp_list = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public JummaInfoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.Date.setText(rp_list.get(position).getDate());
        holder.KatheemName.setText(rp_list.get(position).getKhateeb_name());
        holder.KuthubaTime.setText(rp_list.get(position).getKhutbah_time());
        holder.salaTime.setText(rp_list.get(position).getSalah_time());

    }

    @Override
    public int getItemCount()
    {

        return rp_list.size();
    }
}