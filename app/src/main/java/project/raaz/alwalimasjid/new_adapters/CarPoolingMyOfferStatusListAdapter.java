package project.raaz.alwalimasjid.new_adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import project.raaz.alwalimasjid.OfferList;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.RequestList;
import project.raaz.alwalimasjid.new_fragment.MyOffer;

public class CarPoolingMyOfferStatusListAdapter extends RecyclerView.Adapter<CarPoolingMyOfferStatusListAdapter.ViewHolder> {

    private List<RequestList> rq_list;
    private int rowLayout;
    private Context context;
    MyOffer myFragment;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView listLayout;
        TextView name;
        TextView email;
        TextView picup_points;
        TextView mobile;
        TextView seats;


        public ViewHolder(View v) {
            super(v);
            listLayout =  v.findViewById(R.id.new_offer_status_list_items_layout);
            name = v.findViewById(R.id.new_offer_status_list_items_name);
            email = v.findViewById(R.id.new_offer_status_list_items_email);
            mobile = v.findViewById(R.id.new_offer_status_list_items_mobile);
            picup_points = v.findViewById(R.id.new_offer_status_list_items_picup_point);
            seats = v.findViewById(R.id.new_offer_status_list_items_request_seat);


        }
    }

    public CarPoolingMyOfferStatusListAdapter(List<RequestList> rq_list, int rowLayout, Context context) {
        this.rq_list = rq_list;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public CarPoolingMyOfferStatusListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.name.setText(rq_list.get(position).getName());
        holder.email.setText(rq_list.get(position).getEmail());
        holder.mobile.setText(rq_list.get(position).getMobile());
        holder.picup_points.setText(rq_list.get(position).getPickuppoint());
        holder.seats.setText("Seats : "+rq_list.get(position).getReqseats());



    }

    @Override
    public int getItemCount() {
        return rq_list.size();
    }
}