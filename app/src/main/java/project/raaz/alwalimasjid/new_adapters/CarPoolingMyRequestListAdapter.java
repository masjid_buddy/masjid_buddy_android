package project.raaz.alwalimasjid.new_adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import project.raaz.alwalimasjid.OfferList;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.RequestList;
import project.raaz.alwalimasjid.new_fragment.MyOffer;
import project.raaz.alwalimasjid.new_fragment.MyRequest;

public class CarPoolingMyRequestListAdapter extends RecyclerView.Adapter<CarPoolingMyRequestListAdapter.ViewHolder> {

    private List<RequestList> rq_list;
    private int rowLayout;
    private Context context;
    MyRequest myFragment;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout listLayout;
        TextView source;
        TextView destination;
        TextView picup_points;
        TextView time;
        TextView prayer;
        Button staus;


        public ViewHolder(View v) {
            super(v);
            listLayout =  v.findViewById(R.id.new_offer_list_items_layout);
            source = v.findViewById(R.id.new_offer_list_items_source);
            destination =v.findViewById(R.id.new_offer_list_items_destination);
            picup_points = v.findViewById(R.id.new_offer_list_items_pickup_poits);
            time = v.findViewById(R.id.new_offer_list_items_join_btn);
            prayer = v.findViewById(R.id.new_offer_list_items_prayer);
            staus = v.findViewById(R.id.new_offer_list_items_join_btn);
            staus.setText("Status");

        }
    }

    public CarPoolingMyRequestListAdapter(List<RequestList> rq_list, int rowLayout, Context context, MyRequest myFragment) {
        this.rq_list = rq_list;
        this.rowLayout = rowLayout;
        this.context = context;
        this.myFragment = myFragment;
    }

    @Override
    public CarPoolingMyRequestListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                        int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.source.setText(rq_list.get(position).getStartingpoint());
        holder.destination.setText(rq_list.get(position).getDestination());
        String picup = rq_list.get(position).getPickuppoint();

        List<String> picupPoints = Arrays.asList(picup.split(":"));
        holder.picup_points.setText(picupPoints.size()+" Pickup Points");
        holder.prayer.setText(rq_list.get(0).getPrayer());
        holder.time.setText(rq_list.get(0).getTime());
        holder.staus.setText("Status");


        holder.staus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myFragment.GotoOfferInfo(rq_list.get(position).getId());
            }
        });




    }

    @Override
    public int getItemCount() {
        return rq_list.size();
    }
}