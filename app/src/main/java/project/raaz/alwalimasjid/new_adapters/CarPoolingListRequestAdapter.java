package project.raaz.alwalimasjid.new_adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.RequestList;
import project.raaz.alwalimasjid.fagments.PoolingListOfferRide;
import project.raaz.alwalimasjid.fagments.PoolingListRequestRide;

public class CarPoolingListRequestAdapter extends RecyclerView.Adapter<CarPoolingListRequestAdapter.ViewHolder> {

    private List<RequestList> rq_list;
    private int rowLayout;
    private Context context;
    PoolingListRequestRide myFragment;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout listLayout;
        TextView source;
        TextView destination;
        TextView picup_points;
        TextView time;
        TextView prayer;
        Button status;


        public ViewHolder(View v) {
            super(v);
            listLayout =  v.findViewById(R.id.new_offer_list_items_layout);
            source = v.findViewById(R.id.new_offer_list_items_source);
            destination =v.findViewById(R.id.new_offer_list_items_destination);
            picup_points = v.findViewById(R.id.new_offer_list_items_pickup_poits);
            time = v.findViewById(R.id.new_offer_list_items_join_btn);
            prayer = v.findViewById(R.id.new_offer_list_items_prayer);
            status = v.findViewById(R.id.new_offer_list_items_join_btn);

        }
    }

    public CarPoolingListRequestAdapter(List<RequestList> rq_list, int rowLayout, Context context, PoolingListRequestRide myFragment) {
        this.rq_list = rq_list;
        this.rowLayout = rowLayout;
        this.context = context;
        this.myFragment = myFragment;
    }

    @Override
    public CarPoolingListRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                      int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String  Prayer =  rq_list.get(position).getPrayer().toString();
        final String Street =  rq_list.get(position).getStartingpoint();
        final String Location =  rq_list.get(position).getDestination();
        final String Seats =  rq_list.get(position).getReqseats();
        holder.source.setText(Street);
        holder.destination.setText(Location);
        holder.picup_points.setText(Street);
        holder.prayer.setText(Prayer);
        holder.status.setText("View");



        if(rq_list.get(position).getAccept_status().equals("Accepted")){
            holder.status.setBackgroundColor(0xFF9196A7);
        }

        holder.status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rq_list.get(position).getAccept_status().equals("Accepted")){
                    Toast.makeText(context, "Request completed", Toast.LENGTH_SHORT).show();
                }else{
                    myFragment.goTo(rq_list.get(position).getId());

                }

            }
        });




    }

    @Override
    public int getItemCount() {
        return rq_list.size();
    }
}