package project.raaz.alwalimasjid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import project.raaz.alwalimasjid.map.HomeActivity;
import project.raaz.alwalimasjid.utilities.AppPreferences;


public class SplashScreen extends AppCompatActivity {
    Activity activity;

    private long SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        activity = SplashScreen.this;
        Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
        Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
        Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
        Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
        Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
        Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
        Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
        Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
        Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
        Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
        Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this,HomeActivity
                        .class));
                finish();
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        }, SPLASH_DISPLAY_LENGTH);
    }


}
