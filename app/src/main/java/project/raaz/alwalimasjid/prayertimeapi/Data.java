package project.raaz.alwalimasjid.prayertimeapi;


public class Data {
    Timings timings;
    Date date;


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Timings getTimings() {
        return timings;
    }

    public void setTimings(Timings timings) {
        this.timings = timings;
    }


    public class Date {
        Gregorian gregorian;
        Hijri hijri;

        public Hijri getHijri() {
            return hijri;
        }

        public void setHijri(Hijri hijri) {
            this.hijri = hijri;
        }

        public Gregorian getGregorian() {
            return gregorian;
        }

        public void setGregorian(Gregorian gregorian) {
            this.gregorian = gregorian;
        }
    }

    public class Gregorian {
        String date;
        String day;
        Weekday weekday;
        String year;

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public Weekday getWeekday() {
            return weekday;
        }

        public void setWeekday(Weekday weekday) {
            this.weekday = weekday;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }
    }

    public class Hijri {
        String day;
        String year;
        Weekday weekday;
        Month month;

        public Month getMonth() {
            return month;
        }

        public void setMonth(Month month) {
            this.month = month;
        }

        public Weekday getWeekday() {
            return weekday;
        }

        public void setWeekday(Weekday weekday) {
            this.weekday = weekday;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }
    }


    public class Weekday{
        String en;

        public String getEn() {
            return en;
        }

        public void setEn(String en) {
            this.en = en;
        }
    }

    public class Month{
        String en;

        public String getEn() {
            return en;
        }

        public void setEn(String en) {
            this.en = en;
        }
    }
}
