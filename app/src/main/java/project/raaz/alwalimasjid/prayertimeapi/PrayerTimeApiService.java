package project.raaz.alwalimasjid.prayertimeapi;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PrayerTimeApiService {
    @GET("v1/hijriCalendar")
    Call<PrayerTimeData> getAllPrayerTime(@Query("latitude") String latitude,
                                          @Query("longitude") String longitude,
                                          @Query("method") String method,
                                          @Query("month") String month,
                                          @Query("year") String year);

    @GET("v1/timings")
    Call<PrayerTime> getPrayerTime(@Query("latitude") String latitude,
                                          @Query("longitude") String longitude,
                                          @Query("method") String method);
}
