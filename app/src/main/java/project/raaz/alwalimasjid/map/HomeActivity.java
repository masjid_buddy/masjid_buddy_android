package project.raaz.alwalimasjid.map;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Random;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.CategoryApp;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.activities.DefaultMasjid;
import project.raaz.alwalimasjid.activities.RamadanHome;
import project.raaz.alwalimasjid.activities.RamadanNearByMasjid;
import project.raaz.alwalimasjid.new_activities.NearByMasjid;
import project.raaz.alwalimasjid.pojo_class.MasjidInfoList;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {
    public static final String TAG = "GooglePlaces";
    public static final int TYPE_MASJID = 1;
    public static final int TYPE_DEFAULT_MASJID = 2;
    public static final int TYPE_MASJID_PARKING = 3;
    public static final int TYPE_RAMADAN=4;
    String RANDOM_STRING;


    public static int SELECTED_TYPE_FLAG = 0;

    public static final String TYPE_PEACE = "mosque";


    private LocationManager manager;
    String latitude = null, longitude = null;
    private int PROXIMITY_RADIUS = 2000;

    protected static GoogleApiClient mGoogleApiClient;
    protected static LocationSettingsRequest mLocationSettingsRequest;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;

    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected static final long EXPIRE_INTERVAL_IN_MILLISECONDS = 40;
    protected static LocationRequest mLocationRequest;

    protected Location mLastLocation;
    protected static final int REQUEST_CHECK_SETTINGS = 1000;
    private Context ctx;
    private static boolean isNoGPS = false;
    private static final int GET_LOCATION_CODE = 1;
    private ProgressDialog pDialog;
    private String randomString;
    private HomeActivity activity;
    TextView ramadan_special;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page_a);
        ramadan_special = findViewById(R.id.ramadan_special);
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        ctx = this.getApplicationContext();
        char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        randomString = sb.toString();
        ramadan_special.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    showAlertMessageIfNoGps();
                }else {
                    if (isNetworkAvailable()) {
                        if (latitude != null && longitude != null) {
                            SELECTED_TYPE_FLAG = TYPE_RAMADAN;
                            // Fetching location from places URL
                            getGooglePlacesData(latitude, longitude, TYPE_PEACE);
                        }

                    } else {
                        Toast.makeText(HomeActivity.this, getResources().getString(R.string.strNoInternet), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showAlertMessageIfNoGps();
        } else {
            buildGoogleApiClient();
            createLocationRequest();
            buildLocationSettingsRequest();
            checkLocationSettings();
        }
    }


    private void showAlertMessageIfNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        finish();
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void onRegiterListener(View view){
        Toast.makeText(HomeActivity.this, "For Future Purposes", Toast.LENGTH_SHORT).show();
    }

    public void onFavoriteMasjidListener(View view){

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showAlertMessageIfNoGps();
        } else {
            if (isNetworkAvailable()) {
                if (latitude != null && longitude != null) {
                    SELECTED_TYPE_FLAG = TYPE_MASJID_PARKING;
                    // Fetching location from places URL
                    getGooglePlacesData(latitude, longitude, TYPE_PEACE);
                }

            } else {
                Toast.makeText(HomeActivity.this, getResources().getString(R.string.strNoInternet), Toast.LENGTH_LONG).show();
            }
        }
    }
    public void onMasjidBuddyListener(View view) {

        if (AppPreferences.getRandomId(HomeActivity.this)=="") {
            Toast.makeText(ctx, "Please select your Default Masjid", Toast.LENGTH_SHORT).show();
        } else {
            callMasjidDetaile();
        }
    }

    private void callMasjidDetaile() {

            pDialog = new ProgressDialog(HomeActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            pDialog.setIndeterminate(true);
            pDialog.setMessage("Getting Info...");
            pDialog.setCancelable(false);

            showpDialog();

            RANDOM_STRING = AppPreferences.getRandomId(HomeActivity.this);

            APIService service = ApiClinet.getClient().create(APIService.class);


            Call<MSG> userCall = service.getMasjidInfo(RANDOM_STRING);
            userCall.enqueue(new Callback<MSG>() {
                @Override
                public void onResponse(Call<MSG> call, Response<MSG> response) {
                    hidepDialog();
                    //onSignupSuccess();
                    Log.d("onResponse", "" + response.body().getMessage());


                    if(response.body().getStatus().equals("success")) {
                       try {
                           List<MasjidInfoList> listList = response.body().getMasjidinfo();
                           AppPreferences.setUserId(HomeActivity.this, listList.get(0).getUser_id());
                           AppPreferences.setMasjidId(HomeActivity.this, listList.get(0).getMasjid_id());
                           AppPreferences.setAdminStatus(HomeActivity.this, listList.get(0).getAdmin_status());
                           AppPreferences.setMasjidName(HomeActivity.this,listList.get(0).getMasjid_name());
                           AppPreferences.setMasjidAddress(HomeActivity.this,listList.get(0).getAddress());
                           Intent intent = new Intent(HomeActivity.this, CategoryApp.class);
                           startActivity(intent);
                           overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);



                           activity = HomeActivity.this;

                       }catch (NullPointerException e){
                           e.printStackTrace();
                       }

                    }else {
                        Toast.makeText(HomeActivity.this, "Please Select Your Default Masjid" , Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<MSG> call, Throwable t) {
                    hidepDialog();
                    Log.d("onFailure", t.toString());
                }
            });

    }


    public void onMasjidMapListener(View v) {
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showAlertMessageIfNoGps();
        } else {
            if (isNetworkAvailable()) {
                if (latitude != null && longitude != null) {
                    SELECTED_TYPE_FLAG = TYPE_MASJID;
                    // Fetching location from places URL
                    getGooglePlacesData(latitude, longitude, TYPE_PEACE);
                }
            } else {
                Toast.makeText(HomeActivity.this, getResources().getString(R.string.strNoInternet), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onDefaultMasjidListener(View v) {
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showAlertMessageIfNoGps();
        } else {
            if (isNetworkAvailable()) {
                if (latitude != null && longitude != null) {
                    SELECTED_TYPE_FLAG = TYPE_DEFAULT_MASJID;
                    // Fetching location from places URL
                    getGooglePlacesData(latitude, longitude, TYPE_PEACE);
                }

            } else {
                Toast.makeText(HomeActivity.this, getResources().getString(R.string.strNoInternet), Toast.LENGTH_LONG).show();
            }
        }
    }


    private void MobileRegister() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(HomeActivity.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.get_mobile_dialog);
        // Set dialog title
        dialog.setTitle("Mobile Register");
        dialog.show();


        // set values for custom dialog components - text, image and button
        final EditText text = (EditText) dialog.findViewById(R.id.ext_Mobile_Dialog);


        Button declineButton = (Button) dialog.findViewById(R.id.getMobileButton);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!text.getText().toString().isEmpty()) {
                    randomString = text.getText().toString();
                    SELECTED_TYPE_FLAG = TYPE_DEFAULT_MASJID;
                    // Fetching location from places URL
                    getGooglePlacesData(latitude, longitude, TYPE_PEACE);


                    dialog.dismiss();
                } else {
                    Toast.makeText(HomeActivity.this, "Please Enter Your Mobile", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;

        latitude = String.valueOf(mLastLocation.getLatitude());
        longitude = String.valueOf(mLastLocation.getLongitude());
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mGoogleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }

            //if (mGoogleApiClient.isConnected() && location==null){
            if (mLastLocation != null) {
                latitude = String.valueOf(mLastLocation.getLatitude());
                longitude = String.valueOf(mLastLocation.getLongitude());
            }
            getLocationUpdate();
        }
    }


    private void getLocationUpdate() {
        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.getPermissionInstance().requestPermission(this, PERMISSIONS, ApplicationUtils.PERMISSION_REQUEST_LOCATION_ID, ApplicationUtils.PERMISSION_LOCATION_MESSAGE);
        } else {
            try {
                if (!mGoogleApiClient.isConnected())
                    return;
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient,
                        mLocationRequest,
                        this
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "onConnectionSuspended");
        if (mGoogleApiClient != null) mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed");
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {

        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                try {
                    // Show the dialog by calling startResolutionForResult(), and
                    // check the result
                    // in onActivityResult().
                    status.startResolutionForResult(this,
                            REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {
                    //	Log.i(GEO_TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("FETCH", "Location settings are inadequate, and cannot be fixed here");
                break;
        }
    }

    protected void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest
                .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setExpirationDuration(EXPIRE_INTERVAL_IN_MILLISECONDS);
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(mGoogleApiClient,
                        mLocationSettingsRequest);
        result.setResultCallback(this);
    }


    public String readTextFileFromRaw(InputStream inputStream) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte buf[] = new byte[1024];
        int len;
        try {
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.e(TAG, "IOException:" + e.getMessage());
        }
        return outputStream.toString();
    }


    private void getGooglePlacesData(String latitude, String longitude, String TYPES) {

        PROXIMITY_RADIUS = 2000;


        String GOOGLE_API_KEY = getResources().getString(R.string.google_places_api_key);

        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&types=" + TYPES);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&hasNextPage=true");
        googlePlacesUrl.append("&nextPage()=true");
        googlePlacesUrl.append("&key=" + GOOGLE_API_KEY);
        String strPlacesUrl = googlePlacesUrl.toString();
        Log.d(TAG, "getGooglePlacesData-URL:" + strPlacesUrl);
        GooglePlacesReadTask googlePlacesReadTask = new GooglePlacesReadTask(ctx);
        Object[] toPass = new Object[1];
        //toPass[0] = "";
        toPass[0] = strPlacesUrl;
        googlePlacesReadTask.execute(toPass);
    }

    private void getGooglePlacesData(String latitude, String longitude, String TYPES, int radius) {

        PROXIMITY_RADIUS = radius;


        String GOOGLE_API_KEY = getResources().getString(R.string.google_places_api_key);

        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&types=" + TYPES);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&hasNextPage=true");
        googlePlacesUrl.append("&nextPage()=true");
        googlePlacesUrl.append("&key=" + GOOGLE_API_KEY);
        String strPlacesUrl = googlePlacesUrl.toString();
        Log.d(TAG, "getGooglePlacesData-URL:" + strPlacesUrl);
        GooglePlacesReadTask googlePlacesReadTask = new GooglePlacesReadTask(ctx);
        Object[] toPass = new Object[1];
        //toPass[0] = "";
        toPass[0] = strPlacesUrl;
        googlePlacesReadTask.execute(toPass);
    }

    public void onRamadanClick(View view) {

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showAlertMessageIfNoGps();
        } else {
            if (isNetworkAvailable()) {
                if (latitude != null && longitude != null) {
                    SELECTED_TYPE_FLAG = TYPE_RAMADAN;
                    // Fetching location from places URL
                    getGooglePlacesData(latitude, longitude, TYPE_PEACE);
                }

            } else {
                Toast.makeText(HomeActivity.this, getResources().getString(R.string.strNoInternet), Toast.LENGTH_LONG).show();
            }
        }

    }

    class GooglePlacesReadTask extends AsyncTask<Object, Integer, String> {
        String googlePlacesData = null;
        Context context;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(HomeActivity.this);
            dialog.setMessage("Loading...");
            dialog.show();
        }

        public GooglePlacesReadTask(Context cxt) {
            this.context = cxt;
        }


        @Override
        protected String doInBackground(Object... inputObj) {
            try {
                String googlePlacesUrl = (String) inputObj[0];
                Http http = new Http();
                googlePlacesData = http.read(googlePlacesUrl);

            } catch (Exception e) {
                Log.d("GooglePlaces", e.toString());
            }


            return googlePlacesData;
        }


        @Override
        protected void onPostExecute(String result) {
            String state = null;
            Log.d(TAG, "Map result:" + result);

            try {
                JSONObject obj = new JSONObject(result);

                state = obj.getString("status");

                Log.d(TAG, "Status:" + state);


            } catch (JSONException e) {
                e.printStackTrace();
            }


            if(!state.equals("ZERO_RESULTS")) {
                if (dialog != null) {
                    if (dialog.isShowing()) dialog.dismiss();
                }
                if (result != null) {

                    switch (SELECTED_TYPE_FLAG) {
                        case 1:
                            Intent intent = new Intent(HomeActivity.this, MapActivity.class);
                            intent.putExtra("Response", result);
                            intent.putExtra("latitude", latitude);
                            intent.putExtra("longitude", longitude);
                            intent.putExtra("SelectedType", String.valueOf(SELECTED_TYPE_FLAG));
                            startActivity(intent);
                            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                            activity = HomeActivity.this;

                            break;
                        case 2:
                            intent = new Intent(HomeActivity.this, NearByMasjid.class);
                            intent.putExtra("Response", result);
                            intent.putExtra("latitude", latitude);
                            intent.putExtra("longitude", longitude);
                            intent.putExtra("random_string", randomString);
                            intent.putExtra("SelectedType", String.valueOf(SELECTED_TYPE_FLAG));
                            startActivity(intent);
                            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                            activity = HomeActivity.this;

                            break;
                        case 3:
                            intent = new Intent(HomeActivity.this, DefaultMasjid.class);
                            intent.putExtra("Response", result);
                            intent.putExtra("latitude", latitude);
                            intent.putExtra("longitude", longitude);
                            intent.putExtra("random_string", randomString);
                            intent.putExtra("SelectedType", String.valueOf(SELECTED_TYPE_FLAG));
                            startActivity(intent);
                            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                            activity = HomeActivity.this;


                            break;

                        case 4:
                            intent = new Intent(HomeActivity.this, RamadanNearByMasjid.class);
                            intent.putExtra("Response", result);
                            intent.putExtra("latitude", latitude);
                            intent.putExtra("longitude", longitude);
                            intent.putExtra("random_string", randomString);
                            intent.putExtra("SelectedType", String.valueOf(SELECTED_TYPE_FLAG));
                            startActivity(intent);
                            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                    }

                }
            }else {
                if (dialog != null) {
                    if (dialog.isShowing()) dialog.dismiss();
                }
                getGooglePlacesData(latitude, longitude, TYPE_PEACE,20000);
            }
        }
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}