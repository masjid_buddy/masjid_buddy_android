package project.raaz.alwalimasjid.map;

import java.io.Serializable;

/**
 * Created by augray on 12/16/2015.
 */
public class PlacesList implements Serializable {

    public String masjidName =null;
    public String masjidAddress =null;
    public String masjidLatitude =null;
    public String masjidLongitude =null;
    public String masjidDistance =null;
    public String masjidRating =null;
    public String masjidWorkingStatus =null;
    public String masjidUniqueID=null;
    public String masjidPlaceID=null;
    public boolean isMyMasjid =false;
}
