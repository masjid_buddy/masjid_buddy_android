package project.raaz.alwalimasjid.pojo_class;

/**
 * Created by rasheed on 11/28/2017.
 */

public class PrayerInfo {
    String date;
    String khateeb_name;
    String salah_time;
    String khutbah_time;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKhateeb_name() {
        return khateeb_name;
    }

    public void setKhateeb_name(String khateeb_name) {
        this.khateeb_name = khateeb_name;
    }

    public String getSalah_time() {
        return salah_time;
    }

    public void setSalah_time(String salah_time) {
        this.salah_time = salah_time;
    }

    public String getKhutbah_time() {
        return khutbah_time;
    }

    public void setKhutbah_time(String khutbah_time) {
        this.khutbah_time = khutbah_time;
    }
}
