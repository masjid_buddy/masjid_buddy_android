package project.raaz.alwalimasjid.pojo_class;

/**
 * Created by rasheed on 12/4/2017.
 */

public class ShuttleList {

    String id,vehicle, boardingplace, boardingtime, noofstops, drivername, drivernumber, eventname,eventplace;

    public String getId() {
        return id;
    }

    public String getEventplace() {
        return eventplace;
    }

    public void setEventplace(String eventplace) {
        this.eventplace = eventplace;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getBoardingplace() {
        return boardingplace;
    }

    public void setBoardingplace(String boardingplace) {
        this.boardingplace = boardingplace;
    }

    public String getBoardingtime() {
        return boardingtime;
    }

    public void setBoardingtime(String boardingtime) {
        this.boardingtime = boardingtime;
    }

    public String getNoofstops() {
        return noofstops;
    }

    public void setNoofstops(String noofstops) {
        this.noofstops = noofstops;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getDrivernumber() {
        return drivernumber;
    }

    public void setDrivernumber(String drivernumber) {
        this.drivernumber = drivernumber;
    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }
}
