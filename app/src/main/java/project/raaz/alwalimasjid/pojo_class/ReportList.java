package project.raaz.alwalimasjid.pojo_class;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rasheed on 11/10/2017.
 */

public class ReportList {
@SerializedName("user_id")
    private String user_id;
    @SerializedName("image")
    private String image;
    @SerializedName("description")
    private String description;
    @SerializedName("created_date")
    private String created_date;


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }
}
