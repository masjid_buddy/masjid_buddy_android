package project.raaz.alwalimasjid.pojo_class;

/**
 * Created by raazmd on 15/04/18.
 */

public class Calendarinfo {
    String id, masjid_id,date,fastingday,sahar,ifthar,receivedamount,totalamount;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMasjid_id() {
        return masjid_id;
    }

    public void setMasjid_id(String masjid_id) {
        this.masjid_id = masjid_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFastingday() {
        return fastingday;
    }

    public void setFastingday(String fastingday) {
        this.fastingday = fastingday;
    }

    public String getSahar() {
        return sahar;
    }

    public void setSahar(String sahar) {
        this.sahar = sahar;
    }

    public String getIfthar() {
        return ifthar;
    }

    public String getReceivedamount() {
        return receivedamount;
    }

    public void setReceivedamount(String receivedamount) {
        this.receivedamount = receivedamount;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public void setIfthar(String ifthar) {
        this.ifthar = ifthar;
    }
}
