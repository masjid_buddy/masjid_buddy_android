package project.raaz.alwalimasjid.pojo_class;

/**
 * Created by rasheed on 12/6/2017.
 */

public class Iftharmenu {
  String masjid_id,masjid_name,fastingday,menu;

    public String getMasjid_id() {
        return masjid_id;
    }

    public void setMasjid_id(String masjid_id) {
        this.masjid_id = masjid_id;
    }

    public String getMasjid_name() {
        return masjid_name;
    }

    public void setMasjid_name(String masjid_name) {
        this.masjid_name = masjid_name;
    }

    public String getFastingday() {
        return fastingday;
    }

    public void setFastingday(String fastingday) {
        this.fastingday = fastingday;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }
}
