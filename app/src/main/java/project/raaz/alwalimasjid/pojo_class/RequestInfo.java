package project.raaz.alwalimasjid.pojo_class;

/**
 * Created by rasheed on 12/6/2017.
 */

public class RequestInfo {
   String id,name,mobile,prayer,time,street,area,ride_type,seat,accept_status, accepted_by,accepted_offer_id,date, destination,pickuppoint,
           reqseats,requestype,startingpoint,user_id,email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPrayer() {
        return prayer;
    }

    public void setPrayer(String prayer) {
        this.prayer = prayer;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRide_type() {
        return ride_type;
    }

    public void setRide_type(String ride_type) {
        this.ride_type = ride_type;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getAccept_status() {
        return accept_status;
    }

    public void setAccept_status(String accept_status) {
        this.accept_status = accept_status;
    }

    public String getAccepted_by() {
        return accepted_by;
    }

    public void setAccepted_by(String accepted_by) {
        this.accepted_by = accepted_by;
    }

    public String getAccepted_offer_id() {
        return accepted_offer_id;
    }

    public void setAccepted_offer_id(String accepted_offer_id) {
        this.accepted_offer_id = accepted_offer_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getPickuppoint() {
        return pickuppoint;
    }

    public void setPickuppoint(String pickuppoint) {
        this.pickuppoint = pickuppoint;
    }

    public String getReqseats() {
        return reqseats;
    }

    public void setReqseats(String reqseats) {
        this.reqseats = reqseats;
    }

    public String getRequestype() {
        return requestype;
    }

    public void setRequestype(String requestype) {
        this.requestype = requestype;
    }

    public String getStartingpoint() {
        return startingpoint;
    }

    public void setStartingpoint(String startingpoint) {
        this.startingpoint = startingpoint;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
