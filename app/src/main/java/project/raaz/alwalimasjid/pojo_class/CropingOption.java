package project.raaz.alwalimasjid.pojo_class;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by DP on 7/12/2016.
 */
public class CropingOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}
