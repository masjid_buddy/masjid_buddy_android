package project.raaz.alwalimasjid.pojo_class;

/**
 * Created by rasheed on 11/28/2017.
 */

public class MasjidInfoList {
    String masjid_id;
    String masjid_name;
    String address;
    String latitude;
    String longitude;
    String unique_id;
    String place_id;
    String user_id;
    String mobile;
    String admin_status;

    public String getMasjid_id() {
        return masjid_id;
    }

    public void setMasjid_id(String masjid_id) {
        this.masjid_id = masjid_id;
    }

    public String getMasjid_name() {
        return masjid_name;
    }

    public void setMasjid_name(String masjid_name) {
        this.masjid_name = masjid_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAdmin_status() {
        return admin_status;
    }

    public void setAdmin_status(String admin_status) {
        this.admin_status = admin_status;
    }
}
