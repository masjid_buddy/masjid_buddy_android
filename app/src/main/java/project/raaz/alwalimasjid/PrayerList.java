package project.raaz.alwalimasjid;

/**
 * Created by rasheed on 11/3/2017.
 */

public class PrayerList {
    String masjid_id,
            fajar,
            luzhar,
            azar,
            maghrib,
            isha,
            jumma,
            ifthar,
            sahar,
            sunrise,
            sunset,
            qiamul;

    public String getIfthar() {
        return ifthar;
    }

    public void setIfthar(String ifthar) {
        this.ifthar = ifthar;
    }

    public String getSahar() {
        return sahar;
    }

    public void setSahar(String sahar) {
        this.sahar = sahar;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getQiamul() {
        return qiamul;
    }

    public void setQiamul(String qiamul) {
        this.qiamul = qiamul;
    }

    public String getMasjid_id() {
        return masjid_id;
    }

    public void setMasjid_id(String masjid_id) {
        this.masjid_id = masjid_id;
    }

    public String getFajar() {
        return fajar;
    }

    public void setFajar(String fajar) {
        this.fajar = fajar;
    }

    public String getLuzhar() {
        return luzhar;
    }

    public void setLuzhar(String luzhar) {
        this.luzhar = luzhar;
    }

    public String getAzar() {
        return azar;
    }

    public void setAzar(String azar) {
        this.azar = azar;
    }

    public String getMaghrib() {
        return maghrib;
    }

    public void setMaghrib(String maghrib) {
        this.maghrib = maghrib;
    }

    public String getIsha() {
        return isha;
    }

    public void setIsha(String isha) {
        this.isha = isha;
    }

    public String getJumma() {
        return jumma;
    }

    public void setJumma(String jumma) {
        this.jumma = jumma;
    }
}
