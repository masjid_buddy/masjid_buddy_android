package project.raaz.alwalimasjid.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.CategoryApp;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.adapters.MasjidListAdapter;
import project.raaz.alwalimasjid.map.Places;
import project.raaz.alwalimasjid.map.PlacesList;
import project.raaz.alwalimasjid.pojo_class.DefaultMasjidList;
import project.raaz.alwalimasjid.pojo_class.MasjidInfoList;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/28/2017.
 */

public class DefaultMasjid extends AppCompatActivity {
    private static final String TAG = "DefaultMasjidList";

    public static final int TYPE_DEFAULT_MASJID =2;

    boolean isMarkerAdded=false;
    MarkerOptions marker = new MarkerOptions();
    private TextView locationTv;
    String strPlaceResponse=null;
    int selectedType=0;
    public static boolean hasNextToken=true;
    AppCompatActivity activity;
    private Context context;
    private RecyclerView recyclerView;
    List<DefaultMasjidList> placesList;
    private Toolbar toolbar;
    private ProgressDialog pDialog;
    public String RANDOM_STRING;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.default_masjid);
        activity = DefaultMasjid.this;
        context = getApplicationContext();
        toolbar = (Toolbar) findViewById(R.id.default_masjid_list_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Near By Masjid");
        Bundle bundle = getIntent().getExtras();
        RANDOM_STRING = bundle.getString("random_string");

        recyclerView = (RecyclerView)findViewById(R.id.default_masjid_recyler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(DefaultMasjid.this));
        if(getIntent().getExtras()!=null){
            double latitude= Double.valueOf(getIntent().getStringExtra("latitude"));
            double longitude= Double.valueOf(getIntent().getStringExtra("longitude"));
            strPlaceResponse=getIntent().getStringExtra("Response");
            selectedType= Integer.valueOf(getIntent().getStringExtra("SelectedType"));
            //addingMarkerOnMap(new LatLng(latitude, longitude), "My location!");
            hasNextToken=true;
            showGooglePlaces(strPlaceResponse,latitude,longitude,selectedType);
        }



    }



    private void showGooglePlaces(String result, double currentLat, double currentLong, int selectedType) {
        try {
            JSONObject googlePlacesJson;
            googlePlacesJson = new JSONObject(result);
            Places placeJsonParser = new Places();
            List<HashMap<String, String>> googlePlacesList = null;
            googlePlacesList = placeJsonParser.parse(googlePlacesJson);

            placesList = new ArrayList<DefaultMasjidList>();
            for (int i = 0; i < googlePlacesList.size(); i++) {
                HashMap<String, String> googlePlace = googlePlacesList.get(i);
                PlacesList place = getPlaceInfo(googlePlace, currentLat, currentLong, true);
               DefaultMasjidList defaultMasjid = new DefaultMasjidList();
                defaultMasjid.setName(place.masjidName);
                defaultMasjid.setAddress(place.masjidAddress);
                defaultMasjid.setDistance(place.masjidDistance);
                defaultMasjid.setLatitude(place.masjidLatitude);
                defaultMasjid.setLongitude(place.masjidLongitude);
                defaultMasjid.setId(place.masjidUniqueID);
                defaultMasjid.setPlace_id(place.masjidPlaceID);
                placesList.add(defaultMasjid);

            }
            Collections.sort(placesList, new Comparator<DefaultMasjidList>() {

                @Override
                public int compare(DefaultMasjidList t1, DefaultMasjidList t2) {
                    return (t1.getDistance()).compareTo(t2.getDistance());

                }
            });
            recyclerView.setAdapter(new MasjidListAdapter(placesList, R.layout.default_masjid_items,activity,1));
        }catch (Exception ex){
        }
    }
    private PlacesList getPlaceInfo(HashMap<String, String> googlePlace, double currentLat, double currentLong, boolean isMyRest){
        PlacesList place = new PlacesList();
        place.masjidName = googlePlace.get("place_name");
        place.masjidAddress = googlePlace.get("vicinity");
        place.masjidLatitude = googlePlace.get("lat");
        place.masjidLongitude = googlePlace.get("lng");
        place.masjidPlaceID=googlePlace.get("place_id");
        place.masjidUniqueID=googlePlace.get("id");
        place.masjidRating = googlePlace.get("rating");
        place.masjidWorkingStatus = googlePlace.get("is_open");
        place.isMyMasjid = isMyRest;
        double lat = Double.parseDouble(googlePlace.get("lat"));
        double lng = Double.parseDouble(googlePlace.get("lng"));
        Log.d(TAG, "Place name :" + place.masjidName + "--" + place.masjidRating +
                "--" + place.masjidWorkingStatus);
        double dist = 0;
        String distKM = null;
        if (lat != 0 && lng != 0) {
            dist = distanceBtnLocations(currentLat, currentLong, lat, lng);
            dist = dist / 1000;
            distKM = new DecimalFormat("#0.0").format(dist);
        }
        place.masjidDistance = distKM ;
        return place;
    }

    private double distanceBtnLocations(double lat1, double lon1, double lat2, double lon2) {
        Location selected_location=new Location("locationA");
        selected_location.setLatitude(lat1);
        selected_location.setLongitude(lon1);
        Location near_locations=new Location("locationB");
        near_locations.setLatitude(lat2);
        near_locations.setLongitude(lon2);
        double distance=selected_location.distanceTo(near_locations);
        return distance;
    }


    public void callWebservice(int posstion) {
            if(selectedType==2) {
                String name = placesList.get(posstion).getName();
                String address = placesList.get(posstion).getAddress();
                final String Latitiude = placesList.get(posstion).getLatitude();
                final String Logtitude = placesList.get(posstion).getLongitude();
                String place_id = placesList.get(posstion).getPlace_id();
                String unique_id = placesList.get(posstion).getId();

                pDialog = new ProgressDialog(DefaultMasjid.this,
                        R.style.AppTheme_Dark_Dialog);
                pDialog.setIndeterminate(true);
                pDialog.setMessage("Setting Up...");
                pDialog.setCancelable(false);

                showpDialog();
                if (AppPreferences.getRandomId(DefaultMasjid.this) != "") {
                    RANDOM_STRING = AppPreferences.getRandomId(DefaultMasjid.this);
                }

                APIService service = ApiClinet.getClient().create(APIService.class);


                Call<MSG> userCall = service.addDefaultMasjid(name, RANDOM_STRING, address, Latitiude, Logtitude, unique_id, place_id);

                userCall.enqueue(new Callback<MSG>() {
                    @Override
                    public void onResponse(Call<MSG> call, Response<MSG> response) {
                        //onSignupSuccess();
                        hidepDialog();
                        Log.d("onResponse", "" + response.body().getMessage());
                        AppPreferences.setRandomId(DefaultMasjid.this, RANDOM_STRING);
                        if (response.body().getStatus().equals("success")) {
                            try {
                                List<MasjidInfoList> listList = response.body().getMasjidinfo();
                                AppPreferences.setUserId(DefaultMasjid.this, listList.get(0).getUser_id());
                                AppPreferences.setMasjidId(DefaultMasjid.this, listList.get(0).getMasjid_id());
                                AppPreferences.setAdminStatus(DefaultMasjid.this, listList.get(0).getAdmin_status());
                                AppPreferences.setMasjidName(DefaultMasjid.this, listList.get(0).getMasjid_name());
                                AppPreferences.setMasjidAddress(DefaultMasjid.this, listList.get(0).getAddress());
                                AppPreferences.setLatitiude(DefaultMasjid.this, Latitiude);
                                AppPreferences.setLogtitude(DefaultMasjid.this, Logtitude);
                                String s = AppPreferences.getLatitiude(DefaultMasjid.this);
                                String s2 = AppPreferences.getLogtitude(DefaultMasjid.this);
                                Intent intent = new Intent(DefaultMasjid.this, CategoryApp.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                finish();




                                activity = DefaultMasjid.this;
                                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));


                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(DefaultMasjid.this, "Please try Again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MSG> call, Throwable t) {
                        hidepDialog();
                        Log.d("onFailure", t.toString());
                    }
                });
            }else{
                String name = placesList.get(posstion).getName();
                String address = placesList.get(posstion).getAddress();
                final String Latitiude = placesList.get(posstion).getLatitude();
                final String Logtitude = placesList.get(posstion).getLongitude();
                String place_id = placesList.get(posstion).getPlace_id();
                String unique_id = placesList.get(posstion).getId();

                pDialog = new ProgressDialog(DefaultMasjid.this,
                        R.style.AppTheme_Dark_Dialog);
                pDialog.setIndeterminate(true);
                pDialog.setMessage("Setting Up...");
                pDialog.setCancelable(false);

                showpDialog();

                APIService service = ApiClinet.getClient().create(APIService.class);


                Call<MSG> userCall = service.addSearchMasjid(name,address, Latitiude, Logtitude, unique_id, place_id);

                userCall.enqueue(new Callback<MSG>() {
                    @Override
                    public void onResponse(Call<MSG> call, Response<MSG> response) {
                        //onSignupSuccess();
                        hidepDialog();
                        Log.d("onResponse", "" + response.body().getMessage());
                        if (response.body().getStatus().equals("success")) {
                            try {
                                List<MasjidInfoList> listList = response.body().getMasjidinfo();
                                AppPreferences.setMasjidId(DefaultMasjid.this, listList.get(0).getMasjid_id());
                                AppPreferences.setAdminStatus(DefaultMasjid.this, listList.get(0).getAdmin_status());
                                AppPreferences.setMasjidName(DefaultMasjid.this, listList.get(0).getMasjid_name());
                                AppPreferences.setMasjidAddress(DefaultMasjid.this, listList.get(0).getAddress());
                                AppPreferences.setLatitiudea(DefaultMasjid.this, Latitiude);
                                AppPreferences.setLogtitudea(DefaultMasjid.this, Logtitude);
                                Intent intent = new Intent(DefaultMasjid.this, CategoryParkingInfo.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);




                                activity = DefaultMasjid.this;
                                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));


                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(DefaultMasjid.this, "Please try Again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MSG> call, Throwable t) {
                        hidepDialog();
                        Log.d("onFailure", t.toString());
                    }
                });
            }


    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}


