package project.raaz.alwalimasjid.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by raazmd on 29/04/18.
 */

public class RamadanSponcerAmount extends AppCompatActivity {
    EditText day,items;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_sponsor_amount);
        day =findViewById(R.id.ramadan_iftar_sponcer_fasting_day);
        items=findViewById(R.id.ramadan_iftar_add_amount);
    }



    private void callAddIftarAmount(String str_day, String str_amount) {
        pDialog = new ProgressDialog(RamadanSponcerAmount.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);

        showpDialog();

        String str_masjid_id = AppPreferences.getMasjidId(RamadanSponcerAmount.this);

        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.ramadanIftarAmount(str_masjid_id,str_day,str_amount);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body());


                if(response.body().getStatus().equals("success")) {
                    finish();
                    Toast.makeText(RamadanSponcerAmount.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(RamadanSponcerAmount.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });




    }




    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void onButtonSubmitSponcer(View view) {
        if(day.getText().toString().isEmpty()){

        }else if(items.getText().toString().isEmpty()){

        }else{
            callAddIftarAmount(day.getText().toString(),items.getText().toString());
        }
    }
}

