package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.adapters.RamadanCalendarAdapter;
import project.raaz.alwalimasjid.adapters.RamadanCalendarSponcerAdapter;
import project.raaz.alwalimasjid.pojo_class.Calendarinfo;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * Created by raazmd on 26/03/18.
 */
public class RamadanSponcerCalendar extends Activity {
    Activity activity;
    ProgressDialog pDialog;
    List<Calendarinfo> calendarinfos;
    GridView gridView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_calendar_sponcer);
        activity = RamadanSponcerCalendar.this;
        gridView = (GridView) findViewById(R.id.grid_sponcer);
        CallWebService();


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String fastingday = calendarinfos.get(i).getFastingday();
                String date = RamadanHome.data.get(i).getDate().getGregorian().getDate();
                String todayAmount = calendarinfos.get(i).getTotalamount();

                Intent intent = new Intent(RamadanSponcerCalendar.this,RamadanShareAmount.class);
                intent.putExtra("fastindDay",fastingday);
                intent.putExtra("date",date);
                intent.putExtra("totalAmount",todayAmount);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

    }
    private void CallWebService() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("getting data...");
        pDialog.setCancelable(false);
        String str_masjid = AppPreferences.getMasjidId(activity);
        showpDialog();


        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.getMasjidRamadanShareCalander(str_masjid);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {
                    calendarinfos = response.body().getCalendarinfo();

                    gridView.setAdapter(new RamadanCalendarSponcerAdapter(activity,calendarinfos));


                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });



    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
