package project.raaz.alwalimasjid.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.CarPooling;
import project.raaz.alwalimasjid.CarPoolingOffer;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.pojo_class.RequestInfo;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/16/2017.
 */

public class CarPoolingDetaileView extends AppCompatActivity {
    TextView txt_name,txt_mobile,txt_prayer,txt_time,txt_area,txt_location,txt_seats,acceptMobile;
    private Toolbar toolbar;
    LinearLayout layout_button,layout_mobile,layout_progress;
    Button acceptButton;
    String id,accept_status,accepted_by;
    private ProgressDialog pDialog;
    private String name;
    private String mobile;
    private String prayer;
    private String time;
    private String street;
    private String location;
    private String seats;
    String intentId;
    private CarPoolingDetaileView activity;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_pooling_details);
        toolbar = (Toolbar) findViewById(R.id.detaile_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Details");

        Bundle bundle = getIntent().getExtras();
        intentId = bundle.getString("intentID");
        id = bundle.getString("Id");
        accept_status = bundle.getString("Accept_status");
        accepted_by  = bundle.getString("Accepted_by");
        name  = bundle.getString("Name");
        mobile  = bundle.getString("Mobile");
        prayer  = bundle.getString("Prayer");
        time  = bundle.getString("Time");
        street = bundle.getString("Street");
        location = bundle.getString("Location");
        seats = bundle.getString("Seat");

        txt_name = (TextView)findViewById(R.id.view_pooling_name);
        txt_mobile = (TextView)findViewById(R.id.view_pooling_mobile);
        txt_prayer = (TextView)findViewById(R.id.view_pooling_prayer);
        txt_time = (TextView)findViewById(R.id.view_pooling_time);
        txt_area = (TextView)findViewById(R.id.view_pooling_area);
        txt_location = (TextView)findViewById(R.id.view_pooling_location);
        txt_seats = (TextView)findViewById(R.id.view_pooling_seat);
        acceptMobile = (TextView)findViewById(R.id.view_pooling_accept_mobile);
        layout_button = (LinearLayout)findViewById(R.id.view_pooling_layout_button);
        layout_mobile = (LinearLayout)findViewById(R.id.view_pooling_layout_Number);
        layout_progress = (LinearLayout)findViewById(R.id.view_pooling_layout_progress_bar);
        acceptButton = (Button)findViewById(R.id.view_pooling_accept);
        callGetDetaile();

        txt_name.setText(name);
        txt_mobile.setText(mobile);
        txt_prayer.setText(prayer);
        txt_time.setText(time);
        txt_area.setText(street);
        txt_location.setText(location);
        txt_seats.setText(seats);


        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user_mobile = AppPreferences.getMobileNumber(CarPoolingDetaileView.this);

                if (user_mobile.isEmpty()) {
                    MobileRegister();
                }else{
                    callAcceptService(user_mobile);
                }


            }


        });

        if(accept_status.equals("Pending")){
            layout_mobile.setVisibility(View.INVISIBLE);
            layout_button.setVisibility(View.VISIBLE);
            layout_progress.setVisibility(View.INVISIBLE);

        }else {
            layout_mobile.setVisibility(View.VISIBLE);
            layout_button.setVisibility(View.INVISIBLE);
            layout_progress.setVisibility(View.INVISIBLE);
            acceptMobile.setText(accepted_by);
        }

    }

    private void callGetDetaile() {
        pDialog = new ProgressDialog(CarPoolingDetaileView.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        String ride_type = id;
        Call<MSG> userCall = service.userRequestDetaile(id);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<RequestInfo> requestDetails = response.body().getRequestinfo();


                }else {
                    Toast.makeText(CarPoolingDetaileView.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });
    }

    private void callAcceptService(final String user_mobile) {
        layout_mobile.setVisibility(View.INVISIBLE);
        layout_button.setVisibility(View.INVISIBLE);
        layout_progress.setVisibility(View.VISIBLE);
        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.acceptRequest(id, user_mobile);

        final String finalUser_mobile = user_mobile;
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                Log.d("onResponse", "" + response.body());
                if (response.body().getStatus().equals("success")) {
                    layout_mobile.setVisibility(View.VISIBLE);
                    acceptMobile.setText(user_mobile);
                    layout_button.setVisibility(View.INVISIBLE);
                    layout_progress.setVisibility(View.INVISIBLE);
//                    if (intentId.equals("1")) {
//                        startActivity(new Intent(CarPoolingDetaileView.this, CarPoolingOffer
//                                .class));
//                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
//
//                        finish();
//                        activity = CarPoolingDetaileView.this;
//                        Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
//                        Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
//                        Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
//                        Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
//                        Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
//                        Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
//                        Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
//                        Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
//                        Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
//                        Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
//                        Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));
//                    } else {
//                        startActivity(new Intent(CarPoolingDetaileView.this, CarPooling
//                                .class));
//                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
//                        finish();
//                        activity = CarPoolingDetaileView.this;
//                        Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
//                        Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
//                        Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
//                        Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
//                        Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
//                        Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
//                        Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
//                        Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
//                        Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
//                        Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
//                        Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));
//                    }
                } else {
                    layout_mobile.setVisibility(View.INVISIBLE);
                    layout_button.setVisibility(View.VISIBLE);
                    layout_progress.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                layout_mobile.setVisibility(View.INVISIBLE);
                layout_button.setVisibility(View.VISIBLE);
                layout_progress.setVisibility(View.INVISIBLE);
                Log.d("onFailure", t.toString());
            }
        });
    }



    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void MobileRegister() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(CarPoolingDetaileView.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.get_mobile_dialog);
        // Set dialog title
        dialog.setTitle("Mobile Register");
        dialog.show();


        // set values for custom dialog components - text, image and button
        final EditText text = (EditText) dialog.findViewById(R.id.ext_Mobile_Dialog);


        Button declineButton = (Button) dialog.findViewById(R.id.getMobileButton);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!text.getText().toString().isEmpty()) {
                    callAcceptService(text.getText().toString());
                    dialog.dismiss();

                } else {
                    Toast.makeText(CarPoolingDetaileView.this, "Please Enter Your Mobile", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

}
