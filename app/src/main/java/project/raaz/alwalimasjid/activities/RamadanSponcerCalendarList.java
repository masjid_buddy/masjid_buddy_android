package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.adapters.RamadanCalendarSponcerAdapter;
import project.raaz.alwalimasjid.adapters.RamadanCalendarSponcerAdapterListView;

/**
 * Created by raazmd on 26/03/18.
 */

public class RamadanSponcerCalendarList extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_calendar_sponcer_list);
        ListView listView = (ListView)findViewById(R.id.ramadan_sponcer_lsit_calander_listView);

        // Instance of ImageAdapter Class
        listView.setAdapter(new RamadanCalendarSponcerAdapterListView(this));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(RamadanSponcerCalendarList.this,RamadanShareAmount.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

    }
}
