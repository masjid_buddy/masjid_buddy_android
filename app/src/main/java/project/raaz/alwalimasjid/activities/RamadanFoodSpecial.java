package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by raazmd on 19/03/18.
 */

public class RamadanFoodSpecial extends AppCompatActivity {
    Activity activity;
    ProgressDialog pDialog;
    TextView menuItems;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_menu);
        activity = RamadanFoodSpecial.this;
        toolbar= findViewById(R.id.ramadan_menu_toolbar);

        Bundle  bundle = getIntent().getExtras();
        String str_fasting_day = bundle.getString("fastingDay");
        String str_date = bundle.getString("date");

        menuItems = findViewById(R.id.ifthar_menu_items);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ramadan "+str_fasting_day+"     date : "+str_date);

        callWebservice(str_fasting_day);
    }

    private void callWebservice(String str_fasting_day) {



        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Setting Up...");
        pDialog.setCancelable(false);

        showpDialog();
;
        String str_masjidid = AppPreferences.getMasjidId(activity);

        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.ViewRamadanMenu(str_masjidid,str_fasting_day);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //onSignupSuccess();
                hidepDialog();
                Log.d("onResponse", "" + response.body());
                if (response.body().getStatus().equals("success") && !response.body().getIftharmenu().isEmpty()) {
                        try {
                            int j =1;
                            String menu = response.body().getIftharmenu().get(0).getMenu();
                            List<String> items = Arrays.asList(menu.split(","));
                                for(int i=0; i<items.size();i++){

                                    menuItems.append("\n "+""+j+". "+items.get(i));
                                    j++;
                                }

                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }

                } else {
                    Toast.makeText(activity, "Please try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });


    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
