package project.raaz.alwalimasjid.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.pojo_class.MasjidInfoList;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by raazmd on 19/03/18.
 */

public class RamadanRegister extends AppCompatActivity {
    String amount,fastingDay;
    EditText name,mobile,email;
    private ProgressDialog pDialog;
    Activity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_register);
        activity = RamadanRegister.this;
        Bundle bundle = getIntent().getExtras();
        amount = bundle.getString("Amount");
        fastingDay = bundle.getString("fastingDay");
        name = findViewById(R.id.ramadan_register_name);
        mobile = findViewById(R.id.ramadan_register_mobile);
        email = findViewById(R.id.ramadan_register_name);

    }

    public void onRegiterSubmit(View view) {

        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Setting Up...");
        pDialog.setCancelable(false);

        showpDialog();

        String str_name = name.getText().toString();
        String str_mobile = mobile.getText().toString();
        String str_email = email.getText().toString();
        String str_masjidid = AppPreferences.getMasjidId(activity);
        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.AddIftarShareAmount(str_masjidid,fastingDay,amount,str_name,str_mobile,str_email);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //onSignupSuccess();
                hidepDialog();
                Log.d("onResponse", "" + response.body().getMessage());
                if (response.body().getStatus().equals("success")) {

                    Toast.makeText(activity, "Your Amount add successfully", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(activity, "Please try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
