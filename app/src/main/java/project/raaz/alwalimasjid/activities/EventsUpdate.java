package project.raaz.alwalimasjid.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/24/2017.
 */

public class EventsUpdate extends AppCompatActivity {
    private Toolbar toolbar;
    TextInputEditText event_name,description,place,gust,organizer,number;
    Button submit;
    TextView time,date;

    private ProgressDialog pDialog;

    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_update);
        toolbar= (Toolbar) findViewById(R.id.event_update_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Event Update");

        event_name = (TextInputEditText)findViewById(R.id.event_update_tiet_Name);
        description = (TextInputEditText)findViewById(R.id.event_update_tiet_Description);
        place = (TextInputEditText)findViewById(R.id.event_update_tiet_place);
        date = (TextView) findViewById(R.id.event_update_txt_date);
        gust = (TextInputEditText)findViewById(R.id.event_update_tiet_gust);
        organizer = (TextInputEditText)findViewById(R.id.event_update_tiet_organizer_number);
        number = (TextInputEditText)findViewById(R.id.event_update_tiet_organizer_number);
        time = (TextView)findViewById(R.id.event_update_txt_time);

        submit = (Button)findViewById(R.id.event_update_btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callWebservice();
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(EventsUpdate.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(EventsUpdate.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String format;
                        if (selectedHour == 0) {
                            selectedHour += 12;
                            format = "AM";

                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour == 12) {
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour > 12) {
                            selectedHour -= 12;
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else {
                            format = "AM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        }


                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
    }


    private void callWebservice() {
//        Toast.makeText(PrayerTimeUpdate.this, "Waiting For Webservice", Toast.LENGTH_SHORT).show();

        pDialog = new ProgressDialog(EventsUpdate.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);

        showpDialog();
        String str_masjid_id = AppPreferences.getMasjidId(EventsUpdate.this);
        String str_event = event_name.getText().toString();
        String str_description = description.getText().toString();
        String str_date = date.getText().toString();
        String str_time = time.getText().toString();
        String str_place = place.getText().toString();
        String str_gust   = gust.getText().toString();
        String str_organizer =organizer.getText().toString();
        String str_dnumber = number.getText().toString();
        String image = null;



        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.eventsUpdate(str_masjid_id,str_event,str_description,str_date,str_time,str_place,str_gust,str_organizer);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body());


                if(response.body().getStatus().equals("success")) {
                    finish();

                    Toast.makeText(EventsUpdate.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(EventsUpdate.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });



    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
