package project.raaz.alwalimasjid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.net.URLEncoder;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeApiService;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeData;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by raazmd on 29/04/18.
 */

public class RamadanAdmin extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_admin);
    }

    public void onClickDailyAmountCalander(View view) {
        startActivity(new Intent(RamadanAdmin.this,RamadanSponcerAmount.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void onClickAddDish(View view) {
        startActivity(new Intent(RamadanAdmin.this,RamadanIftarAddMenu.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }


    public void onClickAddRamadanIqqamaTimming(View view) {
        startActivity(new Intent(RamadanAdmin.this,RamadanPrayerTimeUpdate.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void onClickAddRamadamEvents(View view) {
        startActivity(new Intent(RamadanAdmin.this,RamadanEventsUpdate.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }





}
