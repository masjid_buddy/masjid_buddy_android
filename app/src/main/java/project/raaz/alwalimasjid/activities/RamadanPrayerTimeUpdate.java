package project.raaz.alwalimasjid.activities;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/24/2017.
 */

public class RamadanPrayerTimeUpdate extends AppCompatActivity implements View.OnClickListener{
    TextView fajir, zuhr,ashar,magrib,isha,jummah,sunrise,sunset,iftar,suhoor,qiyamul_layl;
    Toolbar toolbar;
    Button submit;
    private ProgressDialog pDialog;

    int FLAG_FAJIR = 1,
            FLAG_ZHUHAR = 2,
            FLAG_AZAR = 3,
            FLAG_MAGRIB = 4,
            FLAG_ISHA = 5,
            FLAG_JUMMA = 6,
            FLAG_QIYAMUL = 7,
            FLAG_SUHOOR = 8,
            FLAG_IFTAR = 9,
            FLAG_SUNSET = 10,
            FLAG_SUNRISE = 11;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_prayer_update);
        toolbar= findViewById(R.id.ramadan_prayer_timming_toolbar_update);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Iqama Time Update");
        fajir =findViewById(R.id.ramadan_prayer_fajir_update);
        zuhr = findViewById(R.id.ramadan_prayer_zuhr_update);
        ashar = findViewById(R.id.ramadan_prayer_asr_update);
        magrib = findViewById(R.id.ramadan_prayer_magrib_update);
        isha = findViewById(R.id.ramadan_prayer_isha_update);
        jummah = findViewById(R.id.ramadan_prayer_jumuah_update);
        iftar = findViewById(R.id.ramadan_prayer_iftar_update);
        sunrise = findViewById(R.id.ramadan_prayer_surise_update);
        sunset = findViewById(R.id.ramadan_prayer_sunset_update);
        suhoor = findViewById(R.id.ramadan_prayer_suhoor_update);
        qiyamul_layl = findViewById(R.id.ramadan_prayer_qiyamul_layle_update);



        fajir.setOnClickListener(this);
        zuhr.setOnClickListener(this);
        ashar.setOnClickListener(this);
        magrib.setOnClickListener(this);
        isha.setOnClickListener(this);
        jummah.setOnClickListener(this);
        qiyamul_layl.setOnClickListener(this);
        suhoor.setOnClickListener(this);
        iftar.setOnClickListener(this);
        sunset.setOnClickListener(this);
        sunrise.setOnClickListener(this);


    }

    private void callWebservice() {
//        Toast.makeText(PrayerTimeUpdate.this, "Waiting For Webservice", Toast.LENGTH_SHORT).show();

        pDialog = new ProgressDialog(RamadanPrayerTimeUpdate.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Updating...");
        pDialog.setCancelable(false);

         showpDialog();
        String str_masjid_id = AppPreferences.getMasjidId(RamadanPrayerTimeUpdate.this);
        String str_fajir = fajir.getText().toString();
        String str_zhuhar = zuhr.getText().toString();
        String str_azar = ashar.getText().toString();
        String str_magrib = magrib.getText().toString();
        String str_isha   = isha.getText().toString();
        String str_jumma = jummah.getText().toString();
        String str_iftar = iftar.getText().toString();
        String str_suhoor = suhoor.getText().toString();
        String str_qiyamul = qiyamul_layl.getText().toString();
        String str_sunset = sunset.getText().toString();
        String str_sunrise = sunrise.getText().toString();



        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.ramadanPrayerUpdate(str_masjid_id,str_fajir,str_zhuhar,str_azar,str_magrib,str_isha,str_jumma,str_iftar,str_suhoor,str_sunrise,str_sunset,str_qiyamul);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {
                    finish();

                    Toast.makeText(RamadanPrayerTimeUpdate.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(RamadanPrayerTimeUpdate.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });



    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.ramadan_prayer_fajir_update:
                timePicker(FLAG_FAJIR);
                break;
            case R.id.ramadan_prayer_zuhr_update:
                timePicker(FLAG_ZHUHAR);
                break;
            case R.id.ramadan_prayer_asr_update:
                timePicker(FLAG_AZAR);
                break;
            case R.id.ramadan_prayer_magrib_update:
                timePicker(FLAG_MAGRIB);
                break;
            case R.id.ramadan_prayer_isha_update:
                timePicker(FLAG_ISHA);
                break;
            case R.id.ramadan_prayer_jumuah_update:
                timePicker(FLAG_JUMMA);
                break;
            case R.id.ramadan_prayer_iftar_update:
                timePicker(FLAG_IFTAR);
                break;
            case R.id.ramadan_prayer_suhoor_update:
                timePicker(FLAG_SUHOOR);
                break;
            case R.id.ramadan_prayer_sunset_update:
                timePicker(FLAG_SUNSET);
                break;
            case R.id.ramadan_prayer_surise_update:
                timePicker(FLAG_SUNRISE);
                break;
            case R.id.ramadan_prayer_qiyamul_layle_update:
                timePicker(FLAG_QIYAMUL);
                break;

        }
    }

    private void timePicker(final int FLAG) {
        // TODO Auto-generated method stub

        final Calendar[] mcurrentTime = {Calendar.getInstance()};
        int hour = mcurrentTime[0].get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime[0].get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(RamadanPrayerTimeUpdate.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                switch (FLAG){
                    case 1:
                        fajir.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 2:
                        zuhr.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                    case 3:
                        ashar.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                    case 4:
                        magrib.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                    case 5:
                        isha.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 6:
                        jummah.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 7:
                        qiyamul_layl.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 8:
                        suhoor.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 9:
                        iftar.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 10:
                        sunset.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 11:
                        sunrise.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                }



            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }


    public String getTimeFormate(int selectedHour, int selectedMinute) {
        String format;
        String time;
        if (selectedHour == 0) {
            selectedHour += 12;
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour == 12) {
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour > 12) {
            selectedHour -= 12;
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else {
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        }
        return time;
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_option_translate_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_translate) {
            callWebservice();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
