package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class CarPoolingRequestRide extends AppCompatActivity {
    Spinner spinner_type;
    TextView time;
    EditText area,street,seat;
    Button request;
    Activity activity;
    ArrayAdapter<CharSequence> spinner_adapter;
    private ProgressDialog pDialog;
    private Toolbar toolbar;
    private EditText name;
    private EditText mobile;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_pooling_ride);
        activity = CarPoolingRequestRide.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        toolbar = (Toolbar) findViewById(R.id.ride_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Car Pooling Request Ride");

        spinner_type = (Spinner) findViewById(R.id.ride_spinner_type);
        name = (EditText) findViewById(R.id.ride_tiet_Name);
        mobile  = (EditText) findViewById(R.id.ride_tiet_mobile);
        time = (TextView) findViewById(R.id.ride_txt_time);
        seat = (EditText) findViewById(R.id.ride_tiet_seat);
        area = (EditText) findViewById(R.id.ride_tiet_area);
        street = (EditText) findViewById(R.id.ride_tiet_street);
        request = (Button) findViewById(R.id.ride_btn_request);
        spinner_adapter = ArrayAdapter.createFromResource(activity, R.array.prayer_type,R.layout.spinner_item);
        spinner_type.setSelection(spinner_adapter.getCount());
        spinner_type.setAdapter(spinner_adapter);

        time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String format;
                        if (selectedHour == 0) {
                            selectedHour += 12;
                            format = "AM";

                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour == 12) {
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour > 12) {
                            selectedHour -= 12;
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else {
                            format = "AM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        }

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Request();
            }
        });
    }

    private void Request() {
        Log.d(TAG, "Pooling_Request");

        if (validate() == false) {
            onRequestFailed();
            return;
        }
        requestByServer();
    }

    private void requestByServer() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Request...");
        pDialog.setCancelable(false);

        showpDialog();

        final String str_prayer, str_time, str_street, str_area, str_request_type,str_user_name,str_user_mobile,str_seat;



        str_time = time.getText().toString();
        str_street = street.getText().toString();
        str_area = area.getText().toString();
        str_request_type = "1";
        str_user_name = name.getText().toString();
        str_user_mobile = mobile.getText().toString();

        str_seat = seat.getText().toString();


        String spinnerValue = spinner_type.getSelectedItem().toString();
        if(spinnerValue.equals("Choose Prayer Time"))
        {
            str_prayer="";

        }else{
            str_prayer = spinnerValue;
        }

        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.userAddRequest(str_user_name,str_user_mobile,str_prayer, str_time, str_street, str_area, str_request_type,str_seat);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {
                    AppPreferences.setMobileNumber(CarPoolingRequestRide.this,str_user_mobile);

                    activity.finish();



                    activity = CarPoolingRequestRide.this;
                    Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                    Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                    Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                    Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                    Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                    Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                    Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                    Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                    Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                    Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                    Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));
                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });
    }

    private void onRequestFailed() {
        Toast.makeText(activity, "request failed", Toast.LENGTH_LONG).show();
    }

    private boolean validate() {
        boolean valid = true;
        String str_prayer, str_time, str_street, str_area, str_request_type;
        str_time = time.getText().toString();
        str_street = street.getText().toString();
        str_area = area.getText().toString();

        return valid;
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
