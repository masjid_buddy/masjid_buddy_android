package project.raaz.alwalimasjid.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;

import project.raaz.alwalimasjid.R;


/**
 * Created by rasheed on 7/21/2017.
 */

public class WebViewActivity extends AppCompatActivity {
    WebView webView;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        Bundle bundle = getIntent().getExtras();
        String url = bundle.getString("image_link");
        toolbar = (Toolbar) findViewById(R.id.WebView_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Reported Image");
        webView = (WebView) findViewById(R.id.webImage);
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.setInitialScale(40);
        webView.loadUrl(url);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
    }



}

