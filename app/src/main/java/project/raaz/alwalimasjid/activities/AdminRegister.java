package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 10/16/2017.
 */

public class AdminRegister extends Activity {
    private static final String TAG = "Register";
    private ProgressDialog pDialog;
    TextInputLayout lMobile,lEmail,lPassword,lCPassword,lArea;
    TextInputEditText txtMobile,txtEmail,txtPassword,txtCPassword,txtArea;
    Button submit;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_register);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        submit = findViewById(R.id.admin_reg_btn_submit);
        lMobile=findViewById(R.id.admin_reg_lMobile);
        lEmail= findViewById(R.id.admin_reg_lEmail);
        lPassword = findViewById(R.id.admin_reg_lPassword);
        lCPassword = findViewById(R.id.admin_reg_lCPassword);
        lArea = findViewById(R.id.admin_reg_lArea);
        txtEmail = findViewById(R.id.admin_reg_txtEmail);
        txtPassword = findViewById(R.id.admin_reg_txtPassword);
        txtCPassword = findViewById(R.id.admin_reg_txtCPassword);
        txtArea = findViewById(R.id.admin_reg_txtArea);
        txtMobile = findViewById(R.id.admin_reg_txtMobile);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(Register.this,Login
//                        .class));
//                finish();
                signup();
            }
        });
    }
    public void signup() {
        Log.d(TAG, "Signup");

        if (validate() == false) {
            onSignupFailed();
            return;
        }

        saveToServerDB();
    }

    public void onSignupSuccess() {
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
    }


    public boolean validate() {
        boolean valid = true;
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();
        String reEnterPassword = txtCPassword.getText().toString();
        String area = txtArea.getText().toString();
        String mobile = txtMobile.getText().toString();


        if (email.isEmpty()) {
            lEmail.setError("enter a email number");
            valid = false;
        } else {
            lEmail.setError(null);
        }


        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            lPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            lPassword.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 4 || reEnterPassword.length() > 10 || !(reEnterPassword.equals(password))) {
            lCPassword.setError("Password Do not match");
            valid = false;
        } else {
            lCPassword.setError(null);
        }

        if (area.isEmpty()) {
            lArea.setError("Please Enter ther Area");
            valid = false;
        } else {
            lArea.setError(null);
        }

        return valid;
    }



    private void saveToServerDB() {
        pDialog = new ProgressDialog(AdminRegister.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);

        showpDialog();
        String user_id = AppPreferences.getUserId(AdminRegister.this);
        String masjid_id = AppPreferences.getMasjidId(AdminRegister.this);
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();
        String area = txtArea.getText().toString();
        String mobile = txtMobile.getText().toString();


        APIService service = ApiClinet.getClient().create(APIService.class);
        //User user = new User(name, email, password);


        Call<MSG> userCall = service.adminSignUp(user_id,masjid_id, mobile,email, password, area);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body());


                    if(response.body().getStatus().equals("success")) {

                    try{
                    if(response.body().getMessage().equals("Already another user registered as admin with this masjid!")){
                        AppPreferences.setAdminStatus(AdminRegister.this,"N");
                        Toast.makeText(AdminRegister.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }else{
                        AppPreferences.setAdminStatus(AdminRegister.this,"Y");
                    }
                    }catch (NullPointerException e){
                        e.printStackTrace();
                        AppPreferences.setAdminStatus(AdminRegister.this,"Y");
                    }

                        finish();
                    }else {
                    Toast.makeText(AdminRegister.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
