package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.GridView;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.adapters.RamadanCalendarAdapter;
import project.raaz.alwalimasjid.pojo_class.Calendarinfo;
import project.raaz.alwalimasjid.prayertimeapi.Data;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeApiService;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeData;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by raazmd on 26/03/18.
 */

public class CalendarAcrivity extends Activity {
    private ProgressDialog pDialog;
    Activity activity;
    List<Calendarinfo> calendarinfos;
    GridView gridView;
    List<Data> data;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_calendar);
        activity = CalendarAcrivity.this;
        gridView = (GridView) findViewById(R.id.grid_view);

        getPrayerTime();
        // Instance of ImageAdapter Class

    }


    public void getPrayerTime() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);
        String str_masjid = AppPreferences.getMasjidId(activity);
        showpDialog();
        String ENDPOINT = "http://api.aladhan.com/";

        final PrayerTimeApiService mService;



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mService = retrofit.create(PrayerTimeApiService.class);

        String Lattiude = AppPreferences.getLatitiude(CalendarAcrivity.this);
        String Logtitude = AppPreferences.getLogtitude(CalendarAcrivity.this);


        mService.getAllPrayerTime(Lattiude,Logtitude,"2","9","1439")
                .enqueue(new Callback<PrayerTimeData>() {


                    @Override
                    public void onResponse(Call<PrayerTimeData> call, Response<PrayerTimeData> response) {
                        hidepDialog();
                        Log.v("ResPonseData",response.body().getStatus());
                        data = response.body().getData();
                        gridView.setAdapter(new RamadanCalendarAdapter(activity,data));

                    }

                    @Override
                    public void onFailure(Call<PrayerTimeData> call, Throwable t) {
                        hidepDialog();

                    }

                });
    }


    private void CallWebService() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);
        String str_masjid = AppPreferences.getMasjidId(activity);
        showpDialog();


        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.getMasjidRamadanCalander(str_masjid);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {
                    calendarinfos = response.body().getCalendarinfo();

//                    gridView.setAdapter(new RamadanCalendarAdapter(activity,calendarinfos));


                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });



    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
