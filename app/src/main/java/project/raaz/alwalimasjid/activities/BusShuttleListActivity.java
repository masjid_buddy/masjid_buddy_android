package project.raaz.alwalimasjid.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.adapters.ShuttleListAdapter;
import project.raaz.alwalimasjid.pojo_class.ShuttleList;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/6/2017.
 */

public class BusShuttleListActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ProgressDialog pDialog;
    private Toolbar toolbar;
    List<ShuttleList> shuttlelist;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bus_shuttle_list);
        toolbar = (Toolbar) findViewById(R.id.bus_list_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bus Shuttle List");
        recyclerView = (RecyclerView)findViewById(R.id.bus_shuttle_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(BusShuttleListActivity.this));
        callingReportList();
    }


    private void callingReportList() {
        pDialog = new ProgressDialog(BusShuttleListActivity.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);
        String masjid_id = AppPreferences.getMasjidId(BusShuttleListActivity.this);

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        String limit = "0",offset="0";
        Call<MSG> userCall = service.shuttleList(limit,offset);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());
                if(response.body().getStatus().equals("success")) {

                    shuttlelist = response.body().getShuttlelist();

                    recyclerView.setAdapter(new ShuttleListAdapter(shuttlelist, R.layout.bus_shuttle_list_items, BusShuttleListActivity.this));
                }else {
                    Toast.makeText(BusShuttleListActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
