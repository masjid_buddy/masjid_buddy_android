package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import project.raaz.alwalimasjid.R;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

/**
 * Created by raazmd on 26/03/18.
 */

public class RamadanShareAmount extends AppCompatActivity{
    TextView todayAmount,totalAmount;
    LinearLayout increase,decrease;
    EditText Amount;
    Button submit;
    String fastingDay,todatAmt,date;
    Toolbar toolbar;
    int i=0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_sponcers);
        toolbar= findViewById(R.id.ramadan_sponcer_toolbar);
        todayAmount = findViewById(R.id.ramadan_sponcers_today_aount);
        totalAmount = findViewById(R.id.ramadan_sponcers_your_share_amount);
        Amount = findViewById(R.id.ramadan_sponcers_ext_amount);
        setSupportActionBar(toolbar);
//        Amount = findViewById(R.id.ramadan_sponcers_amount);
//        increase = findViewById(R.id.ramadan_sponcers_increase);
//        decrease = findViewById(R.id.ramadan_sponcers_decrease);
        Bundle bundle = getIntent().getExtras();
        fastingDay = bundle.getString("fastindDay");
        todatAmt = bundle.getString("totalAmount");
        date = bundle.getString("date");
        todayAmount.setText(todatAmt+"$");
        submit = findViewById(R.id.ramadan_sponcers_btn_proceed);
        getSupportActionBar().setTitle("Ramadan "+fastingDay+"     date : "+date);


        Amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                totalAmount.setText(Amount.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



    }

    public void onClickProceed(View view) {
        String strAmount = Amount.getText().toString();
        String strTotalAmount = totalAmount.getText().toString();

        if(!TextUtils.isEmpty(strTotalAmount)) {
            Intent intent = new Intent(RamadanShareAmount.this, RamadanRegister.class);
            intent.putExtra("Amount", strTotalAmount);
            intent.putExtra("fastingDay",fastingDay);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }
}
