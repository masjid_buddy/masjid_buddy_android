package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;

import static project.raaz.alwalimasjid.R.id.map;

/**
 * Created by rasheed on 10/16/2017.
 */

public class CarParkingMapMyMasjid extends Activity implements OnMapReadyCallback {


    LatLng Parking;
    static final LatLng startParking = new LatLng(40.566439, -74.388271);
    static final LatLng endParking = new LatLng(40.566418, -74.388523);

    static final LatLng startParking1 = new LatLng(40.566736, -74.387994);
    static final LatLng endParking1 = new LatLng(40.566703, -74.388641);

    static final LatLng startParking2 = new LatLng(40.566659, -74.388822);
    static final LatLng endParking2 = new LatLng(40.566459, -74.388797);

    static final LatLng startParking3 = new LatLng(40.566629, -74.388120);
    static final LatLng endParking3 = new LatLng(40.566601, -74.388667);

    static final LatLng startParking4 = new LatLng(40.566629, -74.388120);
    static final LatLng endParking4 = new LatLng(40.566601, -74.388667);

    static final LatLng startParking5 = new LatLng(40.566569, -74.388116);
    static final LatLng endParking5 = new LatLng(40.566538, -74.388663);

    static final LatLng startParking6 = new LatLng(40.566169, -74.388129);
    static final LatLng endParking6 = new LatLng(40.566185, -74.387836);

    static final LatLng startParking7 = new LatLng(40.566293, -74.387941);
    static final LatLng endParking7 = new LatLng(40.566283, -74.388069);


    static final LatLng no_Parking = new LatLng(40.566505, -74.387841);
    static final LatLng start_noParking = new LatLng(40.566427, -74.388186);
    static final LatLng end_noParking = new LatLng(40.566148, -74.388160);


    static final LatLng start_noParking1 = new LatLng(40.566818, -74.387998);
    static final LatLng end_noParking1 = new LatLng(40.566778, -74.388812);

    private GoogleMap googleMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_parking_map);
        String lat = AppPreferences.getLatitiude(CarParkingMapMyMasjid.this);
        String log = AppPreferences.getLogtitude(CarParkingMapMyMasjid.this);
        double latitude = Double.parseDouble(lat);
        double logitude = Double.parseDouble(log);
        Parking = new LatLng(latitude,logitude);
        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {

            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(map);
            mapFragment.getMapAsync(CarParkingMapMyMasjid.this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

            googleMap.addMarker(new MarkerOptions().position(startParking).title("Parking").icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.GREEN).add(startParking, endParking));
        googleMap.addMarker(new MarkerOptions().position(endParking).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));

        googleMap.addMarker(new MarkerOptions().position(startParking1).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.GREEN).add(startParking1, endParking1));
        googleMap.addMarker(new MarkerOptions().position(endParking1).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));

        googleMap.addMarker(new MarkerOptions().position(startParking2).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.GREEN).add(startParking2, endParking2));
        googleMap.addMarker(new MarkerOptions().position(endParking2).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));

        googleMap.addMarker(new MarkerOptions().position(startParking3).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.GREEN).add(startParking3, endParking3));
        googleMap.addMarker(new MarkerOptions().position(endParking3).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));


        googleMap.addMarker(new MarkerOptions().position(startParking4).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.GREEN).add(startParking4, endParking4));
        googleMap.addMarker(new MarkerOptions().position(endParking4).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));

        googleMap.addMarker(new MarkerOptions().position(startParking5).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.GREEN).add(startParking5, endParking5));
        googleMap.addMarker(new MarkerOptions().position(endParking5).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));


        googleMap.addMarker(new MarkerOptions().position(startParking6).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.GREEN).add(startParking6, endParking6));
        googleMap.addMarker(new MarkerOptions().position(endParking6).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));


        googleMap.addMarker(new MarkerOptions().position(startParking7).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.GREEN).add(startParking7, endParking7));
        googleMap.addMarker(new MarkerOptions().position(endParking7).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.location)));



        googleMap.addMarker(new MarkerOptions().position(start_noParking).title("No Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.non_location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.YELLOW).add(start_noParking, end_noParking));
        googleMap.addMarker(new MarkerOptions().position(end_noParking).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.non_location)));

        googleMap.addMarker(new MarkerOptions().position(start_noParking1).title("No Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.non_location)));
        googleMap.addPolyline(new PolylineOptions().width(15).color(Color.YELLOW).add(start_noParking1, end_noParking1));
        googleMap.addMarker(new MarkerOptions().position(end_noParking1).title("Parking").icon(BitmapDescriptorFactory
                .fromResource(R.drawable.non_location)));



        Marker kiel = googleMap.addMarker(new MarkerOptions()
                .position(no_Parking)
                .title("No Parking")
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.non_location)));

        googleMap.addCircle(new CircleOptions()
                .center(no_Parking)
                .radius(15)
                .strokeColor(Color.YELLOW));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(Parking));

        googleMap.addMarker(new MarkerOptions().position(Parking).icon(BitmapDescriptorFactory
                .fromResource(R.drawable.my_location)));

        CameraPosition cameraPosition =
                new CameraPosition.Builder()
                        .target(Parking)
                        .bearing(45)
                        .tilt(90)
                        .zoom(20)
                        .build();
        googleMap.animateCamera(
                CameraUpdateFactory.newCameraPosition(cameraPosition),
                3000,
                null);

        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);


    }
}


