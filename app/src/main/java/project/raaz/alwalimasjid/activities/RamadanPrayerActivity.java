package project.raaz.alwalimasjid.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.PrayerList;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.adapters.RamadanCalendarAdapter;
import project.raaz.alwalimasjid.prayertimeapi.Data;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTime;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeApiService;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeData;
import project.raaz.alwalimasjid.prayertimeapi.Timings;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rasheed on 11/17/2017.
 */

public class RamadanPrayerActivity extends AppCompatActivity {
    TextView fajir, zuhr,ashar,magrib,isha,date, jummah,masjid,sunrise,sunset,iftar,suhoor,qiyamul_layl;
    TextView wfajir, wzuhr,washar,wmagrib,wisha,wdate, wjummah,wmasjid,wsunrise,wsunset,wiftar,wsuhoor,wqiyamul_layl;
    private ProgressDialog pDialog;
    private Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_prayer_timing);
        toolbar = (Toolbar) findViewById(R.id.ramadan_prayer_timming_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Iqama Time");
        Timer t = new Timer();

        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        date = (TextView)findViewById(R.id.ramadan_prayer_currtime);
        fajir = (TextView)findViewById(R.id.ramadan_prayer_fajir);
        zuhr = (TextView)findViewById(R.id.ramadan_prayer_zuhr);
        ashar = (TextView)findViewById(R.id.ramadan_prayer_asr);
        magrib = (TextView)findViewById(R.id.ramadan_prayer_magrib);
        isha = (TextView)findViewById(R.id.ramadan_prayer_isha);
        jummah = (TextView)findViewById(R.id.ramadan_prayer_jumuah);
        masjid = (TextView)findViewById(R.id.ramadan_prayer_masjid);
        sunrise=(TextView)findViewById(R.id.ramadan_prayer_surise);
        sunset = (TextView)findViewById(R.id.ramadan_prayer_sunset);
        suhoor = (TextView)findViewById(R.id.ramadan_prayer_suhoor);
        iftar = (TextView)findViewById(R.id.ramadan_prayer_iftar);
        qiyamul_layl = (TextView)findViewById(R.id.ramadan_prayer_qiyamul_layle);

        wfajir = (TextView)findViewById(R.id.ramadan_prayer_waqt_fajir);
        wzuhr = (TextView)findViewById(R.id.ramadan_prayer_waqt_zuhr);
        washar = (TextView)findViewById(R.id.ramadan_prayer_waqt_asr);
        wmagrib = (TextView)findViewById(R.id.ramadan_prayer_waqt_magrib);
        wisha = (TextView)findViewById(R.id.ramadan_prayer_waqt_isha);
        wjummah = (TextView)findViewById(R.id.ramadan_prayer_waqt_jumma);
        wsunrise=(TextView)findViewById(R.id.ramadan_prayer_waqt_sunrise);
        wsunset = (TextView)findViewById(R.id.ramadan_prayer_waqt_sunset);
        wsuhoor = (TextView)findViewById(R.id.ramadan_prayer_waqt_suhoor);
        wiftar = (TextView)findViewById(R.id.ramadan_prayer_waqt_iftar);
        wqiyamul_layl = (TextView)findViewById(R.id.ramadan_prayer_waqt_tarawih);
        date.setText(currentDateTimeString);
        masjid.setText(AppPreferences.getMajidName(RamadanPrayerActivity.this));

        callWebservice();

    }



    private void callWebservice() {

            pDialog = new ProgressDialog(RamadanPrayerActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            pDialog.setIndeterminate(true);
            pDialog.setMessage("Getting List...");
            pDialog.setCancelable(false);

            showpDialog();

            APIService service = ApiClinet.getClient().create(APIService.class);

            String str_masjid_id = AppPreferences.getMasjidId(RamadanPrayerActivity.this);
            Call<MSG> userCall = service.ramadanPrayerList(str_masjid_id);

            userCall.enqueue(new Callback<MSG>() {
                @Override
                public void onResponse(Call<MSG> call, Response<MSG> response) {
                    hidepDialog();
                    //onSignupSuccess();
                    Log.d("onResponse", "" + response.body().getMessage());


                    if(response.body().getStatus().equals("success")) {
                        if(!response.body().getPrayerlist().isEmpty()) {

                            List<PrayerList> prayerLists = response.body().getPrayerlist();
                                        fajir.setText(prayerLists.get(0).getFajar());
                                        zuhr.setText(prayerLists.get(0).getLuzhar());
                                        ashar.setText(prayerLists.get(0).getAzar());
                                        magrib.setText(prayerLists.get(0).getMaghrib());
                                        isha.setText(prayerLists.get(0).getIsha());
                                        jummah.setText(prayerLists.get(0).getJumma());
                                        suhoor.setText(prayerLists.get(0).getSahar());
                                        iftar.setText(prayerLists.get(0).getIfthar());
                                        qiyamul_layl.setText(prayerLists.get(0).getQiamul());
                                        sunset.setText(prayerLists.get(0).getSunset());
                                        sunrise.setText(prayerLists.get(0).getSunrise());
                                        getPrayerTime();

                        }else{
                            startActivity(new Intent(RamadanPrayerActivity.this,NoDataActivity.class));
                            finish();
                        }


                    }else {
//                        Toast.makeText(PrayerActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                          startActivity(new Intent(RamadanPrayerActivity.this,NoDataActivity.class));
                          finish();
                    }

                }

                @Override
                public void onFailure(Call<MSG> call, Throwable t) {
                    hidepDialog();
                    Log.d("onFailure", t.toString());
                }
            });

        }


    public void getPrayerTime() {
        pDialog = new ProgressDialog(RamadanPrayerActivity.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting Data...");
        pDialog.setCancelable(false);
        showpDialog();
        String ENDPOINT = "http://api.aladhan.com/";

        final PrayerTimeApiService mService;



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mService = retrofit.create(PrayerTimeApiService.class);

        String Lattiude = AppPreferences.getLatitiude(RamadanPrayerActivity.this);
        String Logtitude = AppPreferences.getLogtitude(RamadanPrayerActivity.this);


        mService.getPrayerTime(Lattiude,Logtitude,"2")
                .enqueue(new Callback<PrayerTime>() {


                    @Override
                    public void onResponse(Call<PrayerTime> call, Response<PrayerTime> response) {
                        hidepDialog();
                        Log.v("OnRsponsePrayerTime",response.body().toString());


                        String[] str_fajir = response.body().getData().getTimings().getFajr().split(":");
                        int fajirHours = Integer.parseInt(str_fajir[0]);
                        String fajirMinutes = str_fajir[1];

                        String[] str_wzuhr = response.body().getData().getTimings().getDhuhr().split(":");
                        int wzuhrHours = Integer.parseInt(str_wzuhr[0]);
                        String wzuhrMinutes = str_wzuhr[1];

                        String[] str_washar = response.body().getData().getTimings().getAsr().split(":");
                        int washarHours = Integer.parseInt(str_washar[0]);
                        String washarMinutes = str_washar[1];

                        String[] str_wmagrib = response.body().getData().getTimings().getMaghrib().split(":");
                        int wmagribHours = Integer.parseInt(str_wmagrib[0]);
                        String wmagribMinutes = str_wmagrib[1];

                        String[] str_wisha = response.body().getData().getTimings().getIsha().split(":");
                        int wishaHours = Integer.parseInt(str_wisha[0]);
                        String wishaMinutes = str_wisha[1];

                        String[] str_wsuhoor = response.body().getData().getTimings().getMidnight().split(":");
                        int wsuhoorHours = Integer.parseInt(str_wsuhoor[0]);
                        String wsuhoorMinutes = str_wsuhoor[1];

                        String[] str_wsunset = response.body().getData().getTimings().getSunset().split(":");
                        int wsunsetHours = Integer.parseInt(str_wsunset[0]);
                        String wsunsetMinutes = str_wsunset[1];

                        String[] str_wsunrise = response.body().getData().getTimings().getSunrise().split(":");
                        int wsunriseHours = Integer.parseInt(str_wsunrise[0]);
                        String wsunriseMinutes = str_wsunrise[1];


                        wfajir.setText(getTimeFormate(fajirHours,fajirMinutes));
                        wzuhr.setText(getTimeFormate(wzuhrHours,wzuhrMinutes));
                        washar.setText(getTimeFormate(washarHours,washarMinutes));
                        wmagrib.setText(getTimeFormate(wmagribHours,wmagribMinutes));
                        wisha.setText(getTimeFormate(wishaHours,wishaMinutes));
                        wjummah.setText(getTimeFormate(wzuhrHours,wzuhrMinutes));
                        wsuhoor.setText(getTimeFormate(wsuhoorHours,wsuhoorMinutes));
                        wiftar.setText(getTimeFormate(wsunsetHours,wsunsetMinutes));
                        wqiyamul_layl.setText(getTimeFormate(wishaHours,wishaMinutes));
                        wsunset.setText(getTimeFormate(wsunsetHours,wsunsetMinutes));
                        wsunrise.setText(getTimeFormate(wsunriseHours,wsunriseMinutes));



                    }

                    @Override
                    public void onFailure(Call<PrayerTime> call, Throwable t) {
                        hidepDialog();

                    }

                });
    }




    public String getTimeFormate(int selectedHour, String selectedMinute) {
        String format;
        String time;
        if (selectedHour == 0) {
            selectedHour += 12;
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour == 12) {
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour > 12) {
            selectedHour -= 12;
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else {
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        }
        return time;
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
