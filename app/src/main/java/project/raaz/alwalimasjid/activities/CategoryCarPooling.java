package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import project.raaz.alwalimasjid.CarPooling;
import project.raaz.alwalimasjid.CarPoolingOffer;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;

/**
 * Created by rasheed on 11/14/2017.
 */

public class CategoryCarPooling extends AppCompatActivity implements View.OnClickListener {
    LinearLayout request,offer,request_list, offer_list;

    Activity activity;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_carpooling);
        request = (LinearLayout)findViewById(R.id.catg_pooling_request);
        offer = (LinearLayout)findViewById(R.id.catg_pooling_offer);
        request_list = (LinearLayout)findViewById(R.id.catg_pooling_request_list);
        offer_list = (LinearLayout)findViewById(R.id.catg_pooling_offer_list);


        request.setOnClickListener(this);
        offer_list.setOnClickListener(this);
        request_list.setOnClickListener(this);
        offer.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.catg_pooling_request:

                startActivity(new Intent(CategoryCarPooling.this,CarPoolingRequestRide
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                activity = CategoryCarPooling.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

                break;
            case R.id.catg_pooling_request_list:

                startActivity(new Intent(CategoryCarPooling.this,CarPooling
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);



                activity = CategoryCarPooling.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));
                break;
            case R.id.catg_pooling_offer:
                startActivity(new Intent(CategoryCarPooling.this,CarPoolingOfferRide
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                activity = CategoryCarPooling.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));
                break;
            case R.id.catg_pooling_offer_list:

                startActivity(new Intent(CategoryCarPooling.this,CarPoolingOffer
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                activity = CategoryCarPooling.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));
                break;
        }

    }
}
