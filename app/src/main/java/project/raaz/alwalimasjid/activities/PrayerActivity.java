package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.PrayerList;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.new_activities.JummaListActivity;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTime;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeApiService;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rasheed on 11/17/2017.
 */

public class PrayerActivity extends AppCompatActivity {
    TextView fajir,luzhar,ashar,magrib,isha,date,jumma,masjid;
    TextView adhan_fajir,adhan_luzhar,adhan_ashar,adhan_magrib,adhan_isha,adhan_date,adhan_jumma,adhan_masjid;
    Activity activity;


    LinearLayout layout;
    private ProgressDialog pDialog;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prayer_timing);
        toolbar = (Toolbar) findViewById(R.id.prayer_timming_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Iqama Time");
        Timer t = new Timer();
        activity = PrayerActivity.this;
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        date = (TextView)findViewById(R.id.prayer_currtime);
        fajir = (TextView)findViewById(R.id.prayer_fajir);
        luzhar = (TextView)findViewById(R.id.prayer_luzhar);
        ashar = (TextView)findViewById(R.id.prayer_azar);
        magrib = (TextView)findViewById(R.id.prayer_maghrib);
        isha = (TextView)findViewById(R.id.prayer_isha);
        jumma= (TextView)findViewById(R.id.prayer_jumma);
        masjid = (TextView)findViewById(R.id.prayer_masjid);
        date.setText(currentDateTimeString);
        masjid.setText(AppPreferences.getMajidName(PrayerActivity.this));
        adhan_fajir = (TextView)findViewById(R.id.adhan_fajir);
        adhan_luzhar = (TextView)findViewById(R.id.adhan_luzhar);
        adhan_ashar = (TextView)findViewById(R.id.adhan_azar);
        adhan_magrib = (TextView)findViewById(R.id.adhan_maghrib);
        adhan_isha = (TextView)findViewById(R.id.adhan_isha);
        adhan_jumma= (TextView)findViewById(R.id.adhan_jumma);
        layout = findViewById(R.id.jumma_info);

        callWebservice();
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, JummaListActivity
                        .class));
                activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

    }



    private void callWebservice() {

            pDialog = new ProgressDialog(PrayerActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            pDialog.setIndeterminate(true);
            pDialog.setMessage("Getting List...");
            pDialog.setCancelable(false);

            showpDialog();

            APIService service = ApiClinet.getClient().create(APIService.class);

            String str_masjid_id = AppPreferences.getMasjidId(PrayerActivity.this);
            Call<MSG> userCall = service.prayerList(str_masjid_id);

            userCall.enqueue(new Callback<MSG>() {
                @Override
                public void onResponse(Call<MSG> call, Response<MSG> response) {
                    hidepDialog();
                    //onSignupSuccess();
                    Log.d("onResponse", "" + response.body().getMessage());


                    if(response.body().getStatus().equals("success")) {
                        if(!response.body().getPrayerlist().isEmpty()) {
                            List<PrayerList> prayerLists = response.body().getPrayerlist();
                                        fajir.setText(prayerLists.get(0).getFajar());
                                        luzhar.setText(prayerLists.get(0).getLuzhar());
                                        ashar.setText(prayerLists.get(0).getAzar());
                                        magrib.setText(prayerLists.get(0).getMaghrib());
                                        isha.setText(prayerLists.get(0).getIsha());
                                        jumma.setText(prayerLists.get(0).getJumma());
                                        getPrayerTime();

                        }else{
                            startActivity(new Intent(PrayerActivity.this,NoDataActivity.class));
                            finish();
                        }


                    }else {
//                        Toast.makeText(PrayerActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                          startActivity(new Intent(PrayerActivity.this,NoDataActivity.class));
                          finish();
                    }

                }

                @Override
                public void onFailure(Call<MSG> call, Throwable t) {
                    hidepDialog();
                    Log.d("onFailure", t.toString());
                }
            });

        }


    public void getPrayerTime() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting Data...");
        pDialog.setCancelable(false);
        showpDialog();
        String ENDPOINT = "http://api.aladhan.com/";

        final PrayerTimeApiService mService;



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mService = retrofit.create(PrayerTimeApiService.class);

        String Lattiude = AppPreferences.getLatitiude(activity);
        String Logtitude = AppPreferences.getLogtitude(activity);


        mService.getPrayerTime(Lattiude,Logtitude,"2")
                .enqueue(new Callback<PrayerTime>() {


                    @Override
                    public void onResponse(Call<PrayerTime> call, Response<PrayerTime> response) {
                        hidepDialog();
                        Log.v("OnRsponsePrayerTime",response.body().toString());


                        String[] str_fajir = response.body().getData().getTimings().getFajr().split(":");
                        int fajirHours = Integer.parseInt(str_fajir[0]);
                        String fajirMinutes = str_fajir[1];

                        String[] str_wzuhr = response.body().getData().getTimings().getDhuhr().split(":");
                        int wzuhrHours = Integer.parseInt(str_wzuhr[0]);
                        String wzuhrMinutes = str_wzuhr[1];

                        String[] str_washar = response.body().getData().getTimings().getAsr().split(":");
                        int washarHours = Integer.parseInt(str_washar[0]);
                        String washarMinutes = str_washar[1];

                        String[] str_wmagrib = response.body().getData().getTimings().getMaghrib().split(":");
                        int wmagribHours = Integer.parseInt(str_wmagrib[0]);
                        String wmagribMinutes = str_wmagrib[1];

                        String[] str_wisha = response.body().getData().getTimings().getIsha().split(":");
                        int wishaHours = Integer.parseInt(str_wisha[0]);
                        String wishaMinutes = str_wisha[1];

                        String[] str_wsuhoor = response.body().getData().getTimings().getMidnight().split(":");
                        int wsuhoorHours = Integer.parseInt(str_wsuhoor[0]);
                        String wsuhoorMinutes = str_wsuhoor[1];

                        String[] str_wsunset = response.body().getData().getTimings().getSunset().split(":");
                        int wsunsetHours = Integer.parseInt(str_wsunset[0]);
                        String wsunsetMinutes = str_wsunset[1];

                        String[] str_wsunrise = response.body().getData().getTimings().getSunrise().split(":");
                        int wsunriseHours = Integer.parseInt(str_wsunrise[0]);
                        String wsunriseMinutes = str_wsunrise[1];


                        adhan_fajir.setText(getTimeFormate(fajirHours,fajirMinutes));
                        adhan_luzhar.setText(getTimeFormate(wzuhrHours,wzuhrMinutes));
                        adhan_ashar.setText(getTimeFormate(washarHours,washarMinutes));
                        adhan_magrib.setText(getTimeFormate(wmagribHours,wmagribMinutes));
                        adhan_isha.setText(getTimeFormate(wishaHours,wishaMinutes));
                        adhan_jumma.setText(getTimeFormate(wzuhrHours,wzuhrMinutes));



                    }

                    @Override
                    public void onFailure(Call<PrayerTime> call, Throwable t) {
                        hidepDialog();

                    }

                });
    }


    public String getTimeFormate(int selectedHour, String selectedMinute) {
        String format;
        String time;
        if (selectedHour == 0) {
            selectedHour += 12;
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour == 12) {
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour > 12) {
            selectedHour -= 12;
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else {
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        }
        return time;
    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void clickOnJummaInfo(View view) {


    }
}
