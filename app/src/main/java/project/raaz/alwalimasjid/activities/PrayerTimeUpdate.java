package project.raaz.alwalimasjid.activities;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/24/2017.
 */

public class PrayerTimeUpdate extends AppCompatActivity implements View.OnClickListener{
    TextView fajir,zhuhar,azar,magrib,isha,jumma,iftar;
    Toolbar toolbar;
    Button submit;
    private ProgressDialog pDialog;

    int FLAG_FAJIR  = 1,
        FLAG_ZHUHAR  = 2,
        FLAG_AZAR = 3,
        FLAG_MAGRIB = 4,
        FLAG_ISHA  = 5,
        FLAG_JUMMA  = 6;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prayer_update);
        toolbar= findViewById(R.id.prayer_update_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Iqama Time Update");
        fajir =findViewById(R.id.prayer_update_txt_fajir);
        zhuhar = findViewById(R.id.prayer_update_txt_zhuhar);
        azar = findViewById(R.id.prayer_update_txt_azar);
        magrib = findViewById(R.id.prayer_update_txt_magrib);
        isha = findViewById(R.id.prayer_update_txt_isha);
        jumma = findViewById(R.id.prayer_update_txt_jumma);
        iftar = findViewById(R.id.prayer_update_txt_iftar);
        submit = findViewById(R.id.prayer_update_submit);

        fajir.setOnClickListener(this);
        zhuhar.setOnClickListener(this);
        azar.setOnClickListener(this);
        magrib.setOnClickListener(this);
        isha.setOnClickListener(this);
        jumma.setOnClickListener(this);
        submit.setOnClickListener(this);

    }

    private void callWebservice() {
//        Toast.makeText(PrayerTimeUpdate.this, "Waiting For Webservice", Toast.LENGTH_SHORT).show();

        pDialog = new ProgressDialog(PrayerTimeUpdate.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Updating...");
        pDialog.setCancelable(false);

         showpDialog();
        String str_masjid_id = AppPreferences.getMasjidId(PrayerTimeUpdate.this);
        String str_fajir = fajir.getText().toString();
        String str_zhuhar = zhuhar.getText().toString();
        String str_azar = azar.getText().toString();
        String str_magrib = magrib.getText().toString();
        String str_isha   = isha.getText().toString();
        String str_jumma = jumma.getText().toString();
        String str_iftar = iftar.getText().toString();


        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.prayerUpdate(str_masjid_id,str_fajir,str_zhuhar,str_azar,str_magrib,str_isha,str_jumma,str_iftar);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {
                    finish();

                    Toast.makeText(PrayerTimeUpdate.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(PrayerTimeUpdate.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });



    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.prayer_update_txt_fajir:
                timePicker(FLAG_FAJIR);
                break;
            case R.id.prayer_update_txt_zhuhar:
                timePicker(FLAG_ZHUHAR);
                break;
            case R.id.prayer_update_txt_azar:
                timePicker(FLAG_AZAR);
                break;
            case R.id.prayer_update_txt_magrib:
                timePicker(FLAG_MAGRIB);
                break;
            case R.id.prayer_update_txt_isha:
                timePicker(FLAG_ISHA);
                break;
            case R.id.prayer_update_txt_jumma:
                timePicker(FLAG_JUMMA);
                break;
            case R.id.prayer_update_submit:
                callWebservice();
                break;
        }
    }

    private void timePicker(final int FLAG) {
        // TODO Auto-generated method stub

        final Calendar[] mcurrentTime = {Calendar.getInstance()};
        int hour = mcurrentTime[0].get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime[0].get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(PrayerTimeUpdate.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                switch (FLAG){
                    case 1:
                        fajir.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 2:
                        zhuhar.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                    case 3:
                        azar.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                    case 4:
                        magrib.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;

                    case 5:
                        isha.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                    case 6:
                        jumma.setText(getTimeFormate(selectedHour,selectedMinute));
                        break;
                }



            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }


    public String getTimeFormate(int selectedHour, int selectedMinute) {
        String format;
        String time;
        if (selectedHour == 0) {
            selectedHour += 12;
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour == 12) {
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour > 12) {
            selectedHour -= 12;
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else {
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        }
        return time;
    }
}
