package project.raaz.alwalimasjid.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.pojo_class.MasjidInfoList;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by raazmd on 19/03/18.
 */

public class RamadanCounter extends AppCompatActivity {
    TextView count,total_caount;
    Button increase,decrease,submit;
    EditText name,mobile;
    private ProgressDialog pDialog;
    Activity activity;
    String str_fasting_day, str_date;
    int i = 0;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_count);
        activity = RamadanCounter.this;
        count = findViewById(R.id.ramadam_counter_textView);
        toolbar= findViewById(R.id.ramadan_count_toolbar);
        total_caount = findViewById(R.id.ramadam_counter_total_count);
        increase = findViewById(R.id.ramadam_counter_increase);
        decrease = findViewById(R.id.ramadam_counter_decrease);
        submit = findViewById(R.id.ramadam_counter_btn_submit);
        name = findViewById(R.id.ramadam_counter_ext_name);
        mobile = findViewById(R.id.ramadam_counter_ext_name);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        str_fasting_day = bundle.getString("fastindDay");
        str_date = bundle.getString("date");

        getSupportActionBar().setTitle("Ramadan "+str_fasting_day+"     date : "+str_date);

        count.setText("0");

        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             i++;
             count.setText(""+i);
            }
        });

        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(i!=0){
                    i--;
                    count.setText(""+i);
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(count.getText().toString())){

                }else if(TextUtils.isEmpty(name.getText().toString())){

                }else if(TextUtils.isEmpty(mobile.getText().toString())){

                }else{
                    callWebservice();
                }
            }
        });


    }

    private void callWebservice() {



        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Setting Up...");
        pDialog.setCancelable(false);

        showpDialog();

        String str_name = name.getText().toString();
        String str_mobile = mobile.getText().toString();
        String str_count = count.getText().toString();
        String str_masjidid = AppPreferences.getMasjidId(activity);

        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.AddRamzanMasjidIftarCount(str_masjidid,str_fasting_day,str_count,str_name,str_mobile);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //onSignupSuccess();
                hidepDialog();
                Log.d("onResponse", "" + response.body().getMessage());
                if (response.body().getStatus().equals("success")) {
                    Toast.makeText(activity, "Your Count Added Successfully", Toast.LENGTH_SHORT).show();
                    finish();

                } else {
                    Toast.makeText(activity, "Please try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        callCountWebservices();
    }

    private void callCountWebservices() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Setting Up...");
        pDialog.setCancelable(false);

        showpDialog();

        String str_masjidid = AppPreferences.getMasjidId(activity);

        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.getMasjidIftarCount(str_masjidid,str_fasting_day);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //onSignupSuccess();
                hidepDialog();
                Log.d("onResponse", "" + response.body().getMessage());
                if (response.body().getStatus().equals("success")) {
                    total_caount.setText(response.body().getIftharcount());

                } else {
                    Toast.makeText(activity, "Please try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }

    public void onClickSpecialMenu(View view) {
        startActivity(new Intent(RamadanCounter.this,RamadanFoodSpecial.class).putExtra("fastingDay",str_fasting_day).putExtra("date",str_date));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
