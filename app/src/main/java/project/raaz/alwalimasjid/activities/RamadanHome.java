package project.raaz.alwalimasjid.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.prayertimeapi.Data;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeApiService;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeData;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by raazmd on 19/03/18.
 */

public class RamadanHome extends AppCompatActivity {
    String longitude;
    String latitude;
    public static List<Data> data;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ramadan_home);
        if(getIntent().getExtras()!=null){
            latitude= getIntent().getStringExtra("lat");
            longitude= getIntent().getStringExtra("long");
            getPrayerTime();

        }
    }

    public void onClickCount(View view) {
        startActivity(new Intent(RamadanHome.this,CalendarCount.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void onClickRamadanSponcer(View view) {
        startActivity(new Intent(RamadanHome.this,RamadanSponcerCalendar.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void onClickRamadanIqqamaTimming(View view) {
        startActivity(new Intent(RamadanHome.this,RamadanPrayerActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void onClickRamadamEvents(View view) {
        startActivity(new Intent(RamadanHome.this,RamadanEventListActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

    }

    public void onClickCalander(View view) {
        startActivity(new Intent(RamadanHome.this,CalendarAcrivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }




    public void onClickMasjidData(View view) {
//        Toast.makeText(this, "working", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(RamadanHome.this,RamadanAdmin.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void getPrayerTime() {
        pDialog = new ProgressDialog(RamadanHome.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("getting data...");
        pDialog.setCancelable(false);
        showpDialog();
        String ENDPOINT = "http://api.aladhan.com/";

        final PrayerTimeApiService mService;



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mService = retrofit.create(PrayerTimeApiService.class);

        String Lattiude = AppPreferences.getLatitiude(RamadanHome.this);
        String Logtitude = AppPreferences.getLogtitude(RamadanHome.this);


        mService.getAllPrayerTime(Lattiude,Logtitude,"2","9","1440")
                .enqueue(new Callback<PrayerTimeData>() {

                    @Override
                    public void onResponse(Call<PrayerTimeData> call, Response<PrayerTimeData> response) {
                        hidepDialog();
                        Log.v("ResPonseData",response.body().getStatus());
                        data = response.body().getData();

                    }

                    @Override
                    public void onFailure(Call<PrayerTimeData> call, Throwable t) {

                    }

                });
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
