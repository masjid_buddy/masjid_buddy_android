package project.raaz.alwalimasjid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.ReportListActivity;

/**
 * Created by rasheed on 11/14/2017.
 */

public class CategoryAdmin extends AppCompatActivity implements View.OnClickListener {
    LinearLayout reportList,busSuttle,events,prayerUpdate;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_admin);
        reportList = (LinearLayout)findViewById(R.id.catg_admin_report_list);
        busSuttle = (LinearLayout)findViewById(R.id.catg_admin_bus_suttle);
        events = (LinearLayout)findViewById(R.id.catg_admin_event);
        prayerUpdate = (LinearLayout)findViewById(R.id.catg_admin_prayer);


        reportList.setOnClickListener(this);
        busSuttle.setOnClickListener(this);
        events.setOnClickListener(this);
        prayerUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.catg_admin_report_list:

                startActivity(new Intent(CategoryAdmin.this,ReportListActivity
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case R.id.catg_admin_bus_suttle:

                startActivity(new Intent(CategoryAdmin.this,BusShuttleUpdate
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                break;
            case R.id.catg_admin_prayer:
                startActivity(new Intent(CategoryAdmin.this,PrayerTimeUpdate
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                break;
            case R.id.catg_admin_event:
                startActivity(new Intent(CategoryAdmin.this,EventsUpdate
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

        }

    }
}
