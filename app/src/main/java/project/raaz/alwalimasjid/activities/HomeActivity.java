//package project.raaz.alwalimasjid.activities;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import android.widget.Toast;
//
//import project.raaz.alwalimasjid.CategoryApp;
//import project.raaz.alwalimasjid.R;
//
///**
// * Created by rasheed on 11/24/2017.
// */
//
//public class HomeActivity  extends AppCompatActivity implements View.OnClickListener{
//    Button next,register;
//    LinearLayout defaultMasjid, favoriteMasjid,nearbyMasjid;
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.home_page);
////        next = (Button) findViewById(R.id.home_btn_next);
//        register=(Button)findViewById(R.id.home_btn_register);
//        defaultMasjid = (LinearLayout)findViewById(R.id.home_layout_default);
//        favoriteMasjid = (LinearLayout)findViewById(R.id.home_layout_favorite);
//        nearbyMasjid = (LinearLayout)findViewById(R.id.home_layout_nearby_map);
//
//        next.setOnClickListener(this);
//        register.setOnClickListener(this);
//        defaultMasjid.setOnClickListener(this);
//        favoriteMasjid.setOnClickListener(this);
//        nearbyMasjid.setOnClickListener(this);
//
//    }
//
//    @Override
//    public void onClick(View view) {
//       int i = view.getId();
//       switch(i){
//           case R.id.home_btn_next:
//               startActivity(new Intent(HomeActivity.this,CategoryApp
//                       .class));
//               finish();
//               overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
//               break;
//           case R.id.home_btn_register:
//               Toast.makeText(HomeActivity.this,"Up Coming",Toast.LENGTH_SHORT).show();
//               break;
//           case R.id.home_layout_default:
//               Toast.makeText(HomeActivity.this,"Up Coming",Toast.LENGTH_SHORT).show();
//               break;
//           case R.id.home_layout_favorite:
//               Toast.makeText(HomeActivity.this,"Up Coming",Toast.LENGTH_SHORT).show();
//               break;
//           case R.id.home_layout_nearby_map:
//               Intent intent = new Intent(HomeActivity.this, project.raaz.alwalimasjid.map.HomeActivity.class);
//               startActivity(intent);
////               Toast.makeText(HomeActivity.this,"Up Coming",Toast.LENGTH_SHORT).show();
//               break;
//       }
//    }
//}
