package project.raaz.alwalimasjid.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.EventsList;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.adapters.EventsListAdapter;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/17/2017.
 */

public class EventListActivity extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private ProgressDialog pDialog;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_list);
        toolbar = (Toolbar) findViewById(R.id.event_list_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Events List");
        recyclerView = (RecyclerView)findViewById(R.id.event_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(EventListActivity.this));
        callingReportList();
    }

    private void callingReportList() {
        pDialog = new ProgressDialog(EventListActivity.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);
        String masjid_id = AppPreferences.getMasjidId(EventListActivity.this);

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        String limit = "0",offset="0";
        Call<MSG> userCall = service.eventsList(masjid_id,limit,offset);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                Log.d("onResponse", "" + response.body().getMessage());
                if(response.body().getStatus().equals("success")) {
                    if(!response.body().getEventlist().isEmpty()) {
                        List<EventsList> eventlist = response.body().getEventlist();

                        recyclerView.setAdapter(new EventsListAdapter(eventlist, R.layout.events_list_item, EventListActivity.this));
                    }else{
                        startActivity(new Intent(EventListActivity.this,NoDataActivity.class));
                        finish();
                    }
                }else {
                    startActivity(new Intent(EventListActivity.this,NoDataActivity.class));
                    finish();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
