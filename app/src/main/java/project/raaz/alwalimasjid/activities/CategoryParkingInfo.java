package project.raaz.alwalimasjid.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import project.raaz.alwalimasjid.CarParkingMap;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.Report;
import project.raaz.alwalimasjid.adapters.CustomAdapter;

/**
 * Created by rasheed on 10/16/2017.
 */

public class CategoryParkingInfo extends Activity {
    public  static final int RequestPermissionCode1  = 3 ;
    GridView gridview;

    public static String[] catName = {
            "Parking Info",
            "Car Pooling",
            "Events",
            "Shuttle Service",
            "Iqama Timing",
    "See Somthing Say Somthing"};
    public static int[] catImages = {
            R.drawable.ic_car_parking,
            R.drawable.car_pool,
            R.drawable.ic_events,
            R.drawable.ic_bus_shuttle,
            R.drawable.ic_moon_star,
            R.drawable.ic_no_parking_sign};


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category);




        gridview = (GridView) findViewById(R.id.category_girdview);
        gridview.setAdapter(new CustomAdapter(CategoryParkingInfo.this, catName, catImages));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                openActivity(i);
            }
        });
    }

    private void openActivity(int i) {
        switch (i)
        {
            case 0:
                startActivity(new Intent(CategoryParkingInfo.this,CarParkingMap
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case 1:
                startActivity(new Intent(CategoryParkingInfo.this,CategoryCarPooling
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case 2:
                startActivity(new Intent(CategoryParkingInfo.this,EventListActivity
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case 3:
                startActivity(new Intent(CategoryParkingInfo.this,BusShuttleListActivity
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case 4:
                startActivity(new Intent(CategoryParkingInfo.this,PrayerActivity
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case 5:
                startActivity(new Intent(CategoryParkingInfo.this,Report
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
        }
    }
}


