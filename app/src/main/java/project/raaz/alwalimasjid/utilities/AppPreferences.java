package project.raaz.alwalimasjid.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppPreferences {
	public static final String KEY = "MasjidBuddy";
	public static final String user_id = "user_id";
	public static final String mobile_number = "mobile_number";
	public static final String default_masjid = "default_masjid";
	public static final String masjid_id = "masjid_id";
	public static final String admin_status = "admin_status";
	public static final String masjid_name= "masjid_name";
	public static final String address = "address";
	public static final String random_id = "random_id";
	public static final String latitiude = "latitiude";
	public static final String logtitude = "logtitude";

	public static final String latitiudea = "latitiudea";
	public static final String logtitudea = "logtitudea";
	public static final String user_name = "user_name";

	public static String getUserName(Context context) {
		String username = "";
		if (context.getSharedPreferences(KEY, Context.MODE_PRIVATE) != null) {
			SharedPreferences user_pref = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
			username = user_pref.getString(user_name, "");
		}
		return username;
	}

	public static void setUserName(Context ctx, String username) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(user_name, username);
		edt.commit();
	}

	public static String getUserId(Context context) {
		String userid = "";
		if (context.getSharedPreferences(KEY, Context.MODE_PRIVATE) != null) {
			SharedPreferences user_pref = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
			userid = user_pref.getString(user_id, "");
		}
		return userid;
	}

	public static void setUserId(Context ctx, String userid) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(user_id, userid);
		edt.commit();
	}

	public static String getMasjidId(Context context) {
		String masjidid = "";
		if (context.getSharedPreferences(KEY, Context.MODE_PRIVATE) != null) {
			SharedPreferences user_pref = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
			masjidid = user_pref.getString(masjid_id, "");
		}
		return masjidid;
	}

	public static void setMasjidId(Context ctx, String masjidid) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(masjid_id, masjidid);
		edt.commit();
	}


	public static String getMobileNumber(Context context) {
		String mobilenumber = "";
		if (context.getSharedPreferences(KEY, Context.MODE_PRIVATE) != null) {
			SharedPreferences user_pref = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
			mobilenumber = user_pref.getString(mobile_number, "");
		}
		return mobilenumber;
	}

	public static void setMobileNumber(Context ctx, String mobilenumber) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(mobile_number, mobilenumber);
		edt.commit();
	}


	public static String getAdminStatus(Context context){
		String adminstatus="";
		if(context.getSharedPreferences(KEY,Context.MODE_PRIVATE)!=null){
			SharedPreferences user_pref = context.getSharedPreferences(KEY,Context.MODE_PRIVATE);
			adminstatus = user_pref.getString(admin_status,"");
		}
		return adminstatus;
	}

	public static void setAdminStatus(Context ctx, String adminstatus ) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(admin_status, adminstatus);
		edt.commit();
	}


	public static String getMajidName(Context context){
		String masjidname="";
		if(context.getSharedPreferences(KEY,Context.MODE_PRIVATE)!=null){
			SharedPreferences user_pref = context.getSharedPreferences(KEY,Context.MODE_PRIVATE);
			masjidname = user_pref.getString(masjid_name,"");
		}
		return masjidname;
	}

	public static void setMasjidName(Context ctx, String masjidname ) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(masjid_name, masjidname);
		edt.commit();
	}

	public static String getMajidAddress(Context context){
		String masjidaddress="";
		if(context.getSharedPreferences(KEY,Context.MODE_PRIVATE)!=null){
			SharedPreferences user_pref = context.getSharedPreferences(KEY,Context.MODE_PRIVATE);
			masjidaddress = user_pref.getString(address,"");
		}
		return masjidaddress;
	}

	public static void setMasjidAddress(Context ctx, String masjidaddress ) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(address, masjidaddress);
		edt.commit();
	}


	public static String getRandomId(Context context){
		String randomid="";
		if(context.getSharedPreferences(KEY,Context.MODE_PRIVATE)!=null){
			SharedPreferences user_pref = context.getSharedPreferences(KEY,Context.MODE_PRIVATE);
			randomid = user_pref.getString(random_id,"");
		}
		return randomid;
	}

	public static void setRandomId(Context ctx, String randomid ) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(random_id, randomid);
		edt.commit();
	}

	public static String getLatitiude(Context context){
		String lat="";
		if(context.getSharedPreferences(KEY,Context.MODE_PRIVATE)!=null){
			SharedPreferences user_pref = context.getSharedPreferences(KEY,Context.MODE_PRIVATE);
			lat = user_pref.getString(latitiude,"");
		}
		return lat;
	}

	public static void setLatitiude(Context ctx, String lat ) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(latitiude, lat);
		edt.commit();
	}

	public static String getLogtitude(Context context){
		String log="";
		if(context.getSharedPreferences(KEY,Context.MODE_PRIVATE)!=null){
			SharedPreferences user_pref = context.getSharedPreferences(KEY,Context.MODE_PRIVATE);
			log = user_pref.getString(logtitude,"");
		}
		return log;
	}

	public static void setLogtitude(Context ctx, String log ) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(logtitude, log);
		edt.commit();
	}



	public static String getLatitiudea(Context context){
		String lata="";
		if(context.getSharedPreferences(KEY,Context.MODE_PRIVATE)!=null){
			SharedPreferences user_pref = context.getSharedPreferences(KEY,Context.MODE_PRIVATE);
			lata = user_pref.getString(latitiudea,"");
		}
		return lata;
	}

	public static void setLatitiudea(Context ctx, String lata ) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(latitiudea, lata);
		edt.commit();
	}

	public static String getLogtitudea(Context context){
		String loga="";
		if(context.getSharedPreferences(KEY,Context.MODE_PRIVATE)!=null){
			SharedPreferences user_pref = context.getSharedPreferences(KEY,Context.MODE_PRIVATE);
			loga = user_pref.getString(logtitudea,"");
		}
		return loga;
	}

	public static void setLogtitudea(Context ctx, String loga ) {
		Editor edt = ctx.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
		edt.putString(logtitudea, loga);
		edt.commit();
	}

}