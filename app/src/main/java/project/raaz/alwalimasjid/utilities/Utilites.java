package project.raaz.alwalimasjid.utilities;

import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by rasheed on 12/4/2017.
 */

public class Utilites {
public static String finalTime="";

    public static String timePicker(Context ctx) {
        // TODO Auto-generated method stub

        final Calendar[] mcurrentTime = {Calendar.getInstance()};
        int hour = mcurrentTime[0].get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime[0].get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(ctx, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String format;
                String time;
                if (selectedHour == 0) {
                    selectedHour += 12;
                    format = "AM";
                    time= selectedHour + ":" + selectedMinute + " " + format;
                } else if (selectedHour == 12) {
                    format = "PM";
                    time = selectedHour + ":" + selectedMinute + " " + format;
                } else if (selectedHour > 12) {
                    selectedHour -= 12;
                    format = "PM";
                    time = selectedHour + ":" + selectedMinute + " " + format;
                } else {
                    format = "AM";
                    time = selectedHour + ":" + selectedMinute + " " + format;
                }
                finalTime = time;
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
        return finalTime;
    }


}
