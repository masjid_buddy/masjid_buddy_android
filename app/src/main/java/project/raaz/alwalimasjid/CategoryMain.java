package project.raaz.alwalimasjid;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import project.raaz.alwalimasjid.activities.BusShuttleListActivity;

/**
 * Created by rasheed on 10/16/2017.
 */

public class CategoryMain extends Activity implements View.OnClickListener {
    public  static final int RequestPermissionCode1  = 3 ;

    LinearLayout car_pooling,car_pooling_request,car_parking_map,category_offer,category_report,category_shuttle,category_report_list,category_shuttle_list;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category);
        car_parking_map = findViewById(R.id.catg_main_parking);
        car_pooling = findViewById(R.id.catg_main_pooling);
        car_pooling_request = findViewById(R.id.catg_main_request);
        category_offer = findViewById(R.id.catg_main_offer);
        category_report = findViewById(R.id.catg_main_report);
        category_report_list = findViewById(R.id.catg_main_report_list);
        category_shuttle = findViewById(R.id.catg_main_shuttle_list);
        category_shuttle_list = findViewById(R.id.catg_main_shuttle);


        EnableRuntimePermissionToAccessExternelStorage();


        car_parking_map.setOnClickListener(this);
        car_pooling.setOnClickListener(this);
        car_pooling_request.setOnClickListener(this);
        category_offer.setOnClickListener(this);
        category_report.setOnClickListener(this);
        category_report_list.setOnClickListener(this);
        category_shuttle.setOnClickListener(this);
        category_shuttle_list.setOnClickListener(this);

    }

    private void EnableRuntimePermissionToAccessExternelStorage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(CategoryMain.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
        {

            // Printing toast message after enabling runtime permission.
            Toast.makeText(CategoryMain.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(CategoryMain.this,new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, RequestPermissionCode1);

        }
    }




    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {
            case RequestPermissionCode1:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

//                    Toast.makeText(Report.this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(CategoryMain.this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }




    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){
            case R.id.catg_main_parking:
                startActivity(new Intent(CategoryMain.this,CarParkingMap
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case R.id.catg_main_request:
                startActivity(new Intent(CategoryMain.this,CarPoolingRequest
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case R.id.catg_main_pooling:
                startActivity(new Intent(CategoryMain.this,CarPooling
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case R.id.catg_main_offer:
                startActivity(new Intent(CategoryMain.this,CarPoolingOffer
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case R.id.catg_main_report:
                startActivity(new Intent(CategoryMain.this,Report
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case R.id.catg_main_shuttle:
                startActivity(new Intent(CategoryMain.this,BusShuttle
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case R.id.catg_main_shuttle_list:
                startActivity(new Intent(CategoryMain.this,BusShuttleListActivity
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
            case R.id.catg_main_report_list:
                startActivity(new Intent(CategoryMain.this,ReportListActivity

                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
        }

    }
}


