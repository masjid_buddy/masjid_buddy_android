package project.raaz.alwalimasjid.new_activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.CategoryApp;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.activities.DefaultMasjid;
import project.raaz.alwalimasjid.activities.RamadanNearByMasjid;
import project.raaz.alwalimasjid.adapters.MasjidListAdapter;
import project.raaz.alwalimasjid.map.ApplicationUtils;
import project.raaz.alwalimasjid.map.Http;
import project.raaz.alwalimasjid.map.MapActivity;
import project.raaz.alwalimasjid.map.PermissionUtils;
import project.raaz.alwalimasjid.map.Places;
import project.raaz.alwalimasjid.map.PlacesList;
import project.raaz.alwalimasjid.pojo_class.DefaultMasjidList;
import project.raaz.alwalimasjid.pojo_class.MasjidInfoList;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyMasjid extends AppCompatActivity implements com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {
    public static final String TAG = "GooglePlaces";
    public static final int TYPE_MASJID = 1;
    public static final int TYPE_DEFAULT_MASJID = 2;
    public static final int TYPE_MASJID_PARKING = 3;
    public static final int TYPE_RAMADAN=4;
    String RANDOM_STRING;


    public static int SELECTED_TYPE_FLAG = 0;

    public static final String TYPE_PEACE = "mosque";


    private LocationManager manager;
    String latitude = null, longitude = null;
    private int PROXIMITY_RADIUS = 2000;

    protected static GoogleApiClient mGoogleApiClient;
    protected static LocationSettingsRequest mLocationSettingsRequest;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;

    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected static final long EXPIRE_INTERVAL_IN_MILLISECONDS = 40;
    protected static LocationRequest mLocationRequest;

    protected Location mLastLocation;
    protected static final int REQUEST_CHECK_SETTINGS = 1000;
    private Context ctx;
    private static boolean isNoGPS = false;
    private static final int GET_LOCATION_CODE = 1;
    private ProgressDialog pDialog;
    private String randomString;
    private MyMasjid activity;
    private int SPLASH_DISPLAY_LENGTH =1000;


    private RecyclerView recyclerView;
    List<DefaultMasjidList> placesList;
    private Toolbar toolbar;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_masjid_list);
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        activity = MyMasjid.this;
        context = getApplicationContext();
        toolbar = (Toolbar) findViewById(R.id.masjid_list_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Near By Masjid");


        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showAlertMessageIfNoGps();
        } else {
            if (isNetworkAvailable()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (latitude != null && longitude != null) {
                            SELECTED_TYPE_FLAG = TYPE_DEFAULT_MASJID;
                            // Fetching location from places URL
                            getGooglePlacesData(latitude, longitude, TYPE_PEACE);
                        }
                    }
                }, SPLASH_DISPLAY_LENGTH);


            } else {
                Toast.makeText(MyMasjid.this, getResources().getString(R.string.strNoInternet), Toast.LENGTH_LONG).show();
            }
        }
        recyclerView = (RecyclerView)findViewById(R.id.new_nearby_masjid_recyler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(MyMasjid.this));



    }

    @Override
    protected void onResume() {
        super.onResume();
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        checkLocationSettings();
    }


    private void showAlertMessageIfNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        finish();
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void onMasjidBuddyListener(View view) {

        if (AppPreferences.getRandomId(MyMasjid.this)=="") {
            Toast.makeText(ctx, "Please select your Default Masjid", Toast.LENGTH_SHORT).show();
        } else {
//            callMasjidDetaile();
        }
    }

    private void callMasjidDetaile() {

            pDialog = new ProgressDialog(MyMasjid.this,
                    R.style.AppTheme_Dark_Dialog);
            pDialog.setIndeterminate(true);
            pDialog.setMessage("Getting Info...");
            pDialog.setCancelable(false);

            showpDialog();

            RANDOM_STRING = AppPreferences.getRandomId(MyMasjid.this);

            APIService service = ApiClinet.getClient().create(APIService.class);


            Call<MSG> userCall = service.getMasjidInfo(RANDOM_STRING);
            userCall.enqueue(new Callback<MSG>() {
                @Override
                public void onResponse(Call<MSG> call, Response<MSG> response) {
                    hidepDialog();
                    //onSignupSuccess();
                    Log.d("onResponse", "" + response.body().getMessage());


                    if(response.body().getStatus().equals("success")) {
                       try {
                           List<MasjidInfoList> listList = response.body().getMasjidinfo();
                           AppPreferences.setUserId(MyMasjid.this, listList.get(0).getUser_id());
                           AppPreferences.setMasjidId(MyMasjid.this, listList.get(0).getMasjid_id());
                           AppPreferences.setAdminStatus(MyMasjid.this, listList.get(0).getAdmin_status());
                           AppPreferences.setMasjidName(MyMasjid.this,listList.get(0).getMasjid_name());
                           AppPreferences.setMasjidAddress(MyMasjid.this,listList.get(0).getAddress());
                           Intent intent = new Intent(MyMasjid.this, CategoryApp.class);
                           startActivity(intent);
                           overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                           activity = MyMasjid.this;

                       }catch (NullPointerException e){
                           e.printStackTrace();
                       }

                    }else {
                        Toast.makeText(MyMasjid.this, "Please Select Your Default Masjid" , Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<MSG> call, Throwable t) {
                    hidepDialog();
                    Log.d("onFailure", t.toString());
                }
            });

    }





    private void MobileRegister() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(MyMasjid.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.get_mobile_dialog);
        // Set dialog title
        dialog.setTitle("Mobile Register");
        dialog.show();


        // set values for custom dialog components - text, image and button
        final EditText text = (EditText) dialog.findViewById(R.id.ext_Mobile_Dialog);


        Button declineButton = (Button) dialog.findViewById(R.id.getMobileButton);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!text.getText().toString().isEmpty()) {
                    randomString = text.getText().toString();
                    SELECTED_TYPE_FLAG = TYPE_DEFAULT_MASJID;
                    // Fetching location from places URL
                    getGooglePlacesData(latitude, longitude, TYPE_PEACE);


                    dialog.dismiss();
                } else {
                    Toast.makeText(MyMasjid.this, "Please Enter Your Mobile", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;

        latitude = String.valueOf(mLastLocation.getLatitude());
        longitude = String.valueOf(mLastLocation.getLongitude());
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mGoogleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }

            //if (mGoogleApiClient.isConnected() && location==null){
            if (mLastLocation != null) {
                latitude = String.valueOf(mLastLocation.getLatitude());
                longitude = String.valueOf(mLastLocation.getLongitude());
            }
            getLocationUpdate();
        }
    }


    private void getLocationUpdate() {
        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.getPermissionInstance().requestPermission(this, PERMISSIONS, ApplicationUtils.PERMISSION_REQUEST_LOCATION_ID, ApplicationUtils.PERMISSION_LOCATION_MESSAGE);
        } else {
            try {
                if (!mGoogleApiClient.isConnected())
                    return;
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient,
                        mLocationRequest,
                        this
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "onConnectionSuspended");
        if (mGoogleApiClient != null) mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed");
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {

        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                try {
                    // Show the dialog by calling startResolutionForResult(), and
                    // check the result
                    // in onActivityResult().
                    status.startResolutionForResult(this,
                            REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {
                    //	Log.i(GEO_TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("FETCH", "Location settings are inadequate, and cannot be fixed here");
                break;
        }
    }

    protected void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest
                .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setExpirationDuration(EXPIRE_INTERVAL_IN_MILLISECONDS);
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(mGoogleApiClient,
                        mLocationSettingsRequest);
        result.setResultCallback(this);
    }


    public String readTextFileFromRaw(InputStream inputStream) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte buf[] = new byte[1024];
        int len;
        try {
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            Log.e(TAG, "IOException:" + e.getMessage());
        }
        return outputStream.toString();
    }


    private void getGooglePlacesData(String latitude, String longitude, String TYPES) {

        PROXIMITY_RADIUS = 2000;


        String GOOGLE_API_KEY = getResources().getString(R.string.google_places_api_key);

        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&types=" + TYPES);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&hasNextPage=true");
        googlePlacesUrl.append("&nextPage()=true");
        googlePlacesUrl.append("&key=" + GOOGLE_API_KEY);
        String strPlacesUrl = googlePlacesUrl.toString();
        Log.d(TAG, "getGooglePlacesData-URL:" + strPlacesUrl);
        GooglePlacesReadTask googlePlacesReadTask = new GooglePlacesReadTask(ctx);
        Object[] toPass = new Object[1];
        //toPass[0] = "";
        toPass[0] = strPlacesUrl;
        googlePlacesReadTask.execute(toPass);
    }

    private void getGooglePlacesData(String latitude, String longitude, String TYPES, int radius) {

        PROXIMITY_RADIUS = radius;


        String GOOGLE_API_KEY = getResources().getString(R.string.google_places_api_key);

        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&types=" + TYPES);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&hasNextPage=true");
        googlePlacesUrl.append("&nextPage()=true");
        googlePlacesUrl.append("&key=" + GOOGLE_API_KEY);
        String strPlacesUrl = googlePlacesUrl.toString();
        Log.d(TAG, "getGooglePlacesData-URL:" + strPlacesUrl);
        GooglePlacesReadTask googlePlacesReadTask = new GooglePlacesReadTask(ctx);
        Object[] toPass = new Object[1];
        //toPass[0] = "";
        toPass[0] = strPlacesUrl;
        googlePlacesReadTask.execute(toPass);
    }


    class GooglePlacesReadTask extends AsyncTask<Object, Integer, String> {
        String googlePlacesData = null;
        Context context;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MyMasjid.this);
            dialog.setMessage("Loading...");
            dialog.show();
        }

        public GooglePlacesReadTask(Context cxt) {
            this.context = cxt;
        }


        @Override
        protected String doInBackground(Object... inputObj) {
            try {
                String googlePlacesUrl = (String) inputObj[0];
                Http http = new Http();
                googlePlacesData = http.read(googlePlacesUrl);

            } catch (Exception e) {
                Log.d("GooglePlaces", e.toString());
            }


            return googlePlacesData;
        }


        @Override
        protected void onPostExecute(String result) {
            String state = null;
            Log.d(TAG, "Map result:" + result);

            try {
                JSONObject obj = new JSONObject(result);

                state = obj.getString("status");

                Log.d(TAG, "Status:" + state);


            } catch (JSONException e) {
                e.printStackTrace();
            }


            if(!state.equals("ZERO_RESULTS")) {
                if (dialog != null) {
                    if (dialog.isShowing()) dialog.dismiss();
                }
                if (result != null) {

                    switch (SELECTED_TYPE_FLAG) {
                        case 2:
                            double lat= Double.valueOf(latitude);
                            double lon= Double.valueOf(longitude);
                            showGooglePlaces(result,lat,lon);
                            break;
                    }

                }
            }else {
                if (dialog != null) {
                    if (dialog.isShowing()) dialog.dismiss();
                }
                getGooglePlacesData(latitude, longitude, TYPE_PEACE,20000);
            }
        }
    }

    private void showGooglePlaces(String result, double currentLat, double currentLong) {
        try {
            JSONObject googlePlacesJson;
            googlePlacesJson = new JSONObject(result);
            Places placeJsonParser = new Places();
            List<HashMap<String, String>> googlePlacesList = null;
            googlePlacesList = placeJsonParser.parse(googlePlacesJson);

            placesList = new ArrayList<DefaultMasjidList>();
            for (int i = 0; i < googlePlacesList.size(); i++) {
                HashMap<String, String> googlePlace = googlePlacesList.get(i);
                PlacesList place = getPlaceInfo(googlePlace, currentLat, currentLong, true);
                DefaultMasjidList defaultMasjid = new DefaultMasjidList();
                defaultMasjid.setName(place.masjidName);
                defaultMasjid.setAddress(place.masjidAddress);
                defaultMasjid.setDistance(place.masjidDistance);
                defaultMasjid.setLatitude(place.masjidLatitude);
                defaultMasjid.setLongitude(place.masjidLongitude);
                defaultMasjid.setId(place.masjidUniqueID);
                defaultMasjid.setPlace_id(place.masjidPlaceID);
                placesList.add(defaultMasjid);

            }
            Collections.sort(placesList, new Comparator<DefaultMasjidList>() {

                @Override
                public int compare(DefaultMasjidList t1, DefaultMasjidList t2) {
                    return (t1.getDistance()).compareTo(t2.getDistance());

                }
            });
            recyclerView.setAdapter(new MasjidListAdapter(placesList, R.layout.new_masjid_items,activity,0));
        }catch (Exception ex){
        }
    }
    private PlacesList getPlaceInfo(HashMap<String, String> googlePlace, double currentLat, double currentLong, boolean isMyRest){
        PlacesList place = new PlacesList();
        place.masjidName = googlePlace.get("place_name");
        place.masjidAddress = googlePlace.get("vicinity");
        place.masjidLatitude = googlePlace.get("lat");
        place.masjidLongitude = googlePlace.get("lng");
        place.masjidPlaceID=googlePlace.get("place_id");
        place.masjidUniqueID=googlePlace.get("id");
        place.masjidRating = googlePlace.get("rating");
        place.masjidWorkingStatus = googlePlace.get("is_open");
        place.isMyMasjid = isMyRest;
        double lat = Double.parseDouble(googlePlace.get("lat"));
        double lng = Double.parseDouble(googlePlace.get("lng"));
        Log.d(TAG, "Place name :" + place.masjidName + "--" + place.masjidRating +
                "--" + place.masjidWorkingStatus);
        double dist = 0;
        String distKM = null;
        if (lat != 0 && lng != 0) {
            dist = distanceBtnLocations(currentLat, currentLong, lat, lng);
            dist = dist / 1000;
            distKM = new DecimalFormat("#0.0").format(dist);
        }
        place.masjidDistance = distKM ;
        return place;
    }

    private double distanceBtnLocations(double lat1, double lon1, double lat2, double lon2) {
        Location selected_location=new Location("locationA");
        selected_location.setLatitude(lat1);
        selected_location.setLongitude(lon1);
        Location near_locations=new Location("locationB");
        near_locations.setLatitude(lat2);
        near_locations.setLongitude(lon2);
        double distance=selected_location.distanceTo(near_locations);
        return distance;
    }


    public void callWebservice(int posstion) {
        String name = placesList.get(posstion).getName();
        String address = placesList.get(posstion).getAddress();
        final String Latitiude = placesList.get(posstion).getLatitude();
        final String Logtitude = placesList.get(posstion).getLongitude();
        String place_id = placesList.get(posstion).getPlace_id();
        String unique_id = placesList.get(posstion).getId();

        pDialog = new ProgressDialog(MyMasjid.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Setting Up...");
        pDialog.setCancelable(false);

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.addSearchMasjid(name,address, Latitiude, Logtitude, unique_id, place_id);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //onSignupSuccess();
                hidepDialog();
                Log.d("onResponse", "" + response.body().getMessage());
                if (response.body().getStatus().equals("success")) {
                    try {
                        List<MasjidInfoList> listList = response.body().getMasjidinfo();
                        AppPreferences.setMasjidId(MyMasjid.this, listList.get(0).getMasjid_id());
                        AppPreferences.setAdminStatus(MyMasjid.this, listList.get(0).getAdmin_status());
                        AppPreferences.setMasjidName(MyMasjid.this, listList.get(0).getMasjid_name());
                        AppPreferences.setMasjidAddress(MyMasjid.this, listList.get(0).getAddress());
                        AppPreferences.setLatitiude(MyMasjid.this, Latitiude);
                        AppPreferences.setLogtitude(MyMasjid.this, Logtitude);
                        finish();
                        activity = MyMasjid.this;



                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(MyMasjid.this, "Please try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

//            if(selectedType==2) {
//                String name = placesList.get(posstion).getName();
//                String address = placesList.get(posstion).getAddress();
//                final String Latitiude = placesList.get(posstion).getLatitude();
//                final String Logtitude = placesList.get(posstion).getLongitude();
//                String place_id = placesList.get(posstion).getPlace_id();
//                String unique_id = placesList.get(posstion).getId();
//
//                pDialog = new ProgressDialog(NearByMasjid.this,
//                        R.style.AppTheme_Dark_Dialog);
//                pDialog.setIndeterminate(true);
//                pDialog.setMessage("Setting Up...");
//                pDialog.setCancelable(false);
//
//                showpDialog();
//                if (AppPreferences.getRandomId(NearByMasjid.this) != "") {
//                    RANDOM_STRING = AppPreferences.getRandomId(NearByMasjid.this);
//                }
//
//                APIService service = ApiClinet.getClient().create(APIService.class);
//
//
//                Call<MSG> userCall = service.addDefaultMasjid(name, RANDOM_STRING, address, Latitiude, Logtitude, unique_id, place_id);
//
//                userCall.enqueue(new Callback<MSG>() {
//                    @Override
//                    public void onResponse(Call<MSG> call, Response<MSG> response) {
//                        //onSignupSuccess();
//                        hidepDialog();
//                        Log.d("onResponse", "" + response.body().getMessage());
//                        AppPreferences.setRandomId(NearByMasjid.this, RANDOM_STRING);
//                        if (response.body().getStatus().equals("success")) {
//                            try {
//                                List<MasjidInfoList> listList = response.body().getMasjidinfo();
//                                AppPreferences.setUserId(NearByMasjid.this, listList.get(0).getUser_id());
//                                AppPreferences.setMasjidId(NearByMasjid.this, listList.get(0).getMasjid_id());
//                                AppPreferences.setAdminStatus(NearByMasjid.this, listList.get(0).getAdmin_status());
//                                AppPreferences.setMasjidName(NearByMasjid.this, listList.get(0).getMasjid_name());
//                                AppPreferences.setMasjidAddress(NearByMasjid.this, listList.get(0).getAddress());
//                                AppPreferences.setLatitiude(NearByMasjid.this, Latitiude);
//                                AppPreferences.setLogtitude(NearByMasjid.this, Logtitude);
//                                String s = AppPreferences.getLatitiude(NearByMasjid.this);
//                                String s2 = AppPreferences.getLogtitude(NearByMasjid.this);
//
//
//
//
//                                activity = NearByMasjid.this;
//
//                            } catch (NullPointerException e) {
//                                e.printStackTrace();
//                            }
//
//                        } else {
//                            Toast.makeText(NearByMasjid.this, "Please try Again", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<MSG> call, Throwable t) {
//                        hidepDialog();
//                        Log.d("onFailure", t.toString());
//                    }
//                });
//            }else{
//
//            }


    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}