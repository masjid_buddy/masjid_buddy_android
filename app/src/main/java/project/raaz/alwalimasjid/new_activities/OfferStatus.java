package project.raaz.alwalimasjid.new_activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.OfferInfo;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.RequestList;
import project.raaz.alwalimasjid.new_adapters.CarPoolingMyOfferListAdapter;
import project.raaz.alwalimasjid.new_adapters.CarPoolingMyOfferStatusListAdapter;
import project.raaz.alwalimasjid.new_fragment.MyOffer;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferStatus extends AppCompatActivity {
    Activity activity;

    private Toolbar toolbar;
    Button acceptButton;
    String id,accept_status,accepted_by;
    private ProgressDialog pDialog;
    TextView txt_from,txt_to,txt_stops,txt_date,txt_time,txt_waqth,txt_decrease,txt_increase,txt_seats,txt_name,txt_mobile,txt_email;
    Button btn_join;
    int seat_count=0;
    int available_seat_count=0;

    LinearLayout seatAvailableLayout;

    RecyclerView my_offer_status_recycler_view;

    Spinner PickupPoints;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_my_offer_status);
        toolbar = (Toolbar) findViewById(R.id.my_offer_status_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Join Ride Info");
        activity = OfferStatus.this;
        my_offer_status_recycler_view = findViewById(R.id.my_offer_status_recycler_view);
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("Id");
        callViewOffer();



    }

    private void callViewOffer() {

        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.offer_join_ride_info(id,"","");

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<RequestList> requestLists = response.body().getRequestlist();
                    my_offer_status_recycler_view.setLayoutManager(new LinearLayoutManager(activity));
                    my_offer_status_recycler_view.setAdapter(new CarPoolingMyOfferStatusListAdapter(requestLists, R.layout.new_offer_status_list_itmes, activity));


                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public void onClickJoinRide(View view) {
        if(!txt_seats.getText().equals("0")) {
            String user_id = AppPreferences.getUserId(activity);
            String pickup_point = PickupPoints.getSelectedItem().toString();
            String seat=txt_seats.getText().toString();

            pDialog = new ProgressDialog(activity,
                    R.style.AppTheme_Dark_Dialog);
            pDialog.setIndeterminate(true);
            pDialog.setMessage("Getting List...");
            pDialog.setCancelable(false);

            showpDialog();

            APIService service = ApiClinet.getClient().create(APIService.class);

            Call<MSG> userCall = service.JointRide(user_id, id, pickup_point, seat);

            userCall.enqueue(new Callback<MSG>() {
                @Override
                public void onResponse(Call<MSG> call, Response<MSG> response) {
                    hidepDialog();
                    //onSignupSuccess();
                    Log.d("onResponse", "" + response.body().getMessage());


                    if (response.body().getStatus().equals("success")) {

                        Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MSG> call, Throwable t) {
                    hidepDialog();
                    Log.d("onFailure", t.toString());
                }
            });
        }
    }


}
