package project.raaz.alwalimasjid.new_activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.CategoryApp;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.activities.CategoryParkingInfo;
import project.raaz.alwalimasjid.adapters.MasjidListAdapter;
import project.raaz.alwalimasjid.map.Places;
import project.raaz.alwalimasjid.map.PlacesList;
import project.raaz.alwalimasjid.pojo_class.DefaultMasjidList;
import project.raaz.alwalimasjid.pojo_class.MasjidInfoList;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/28/2017.
 */

public class NearByMasjid extends AppCompatActivity {
    private static final String TAG = "NearByMasjid";

    public static final int TYPE_DEFAULT_MASJID =2;

    boolean isMarkerAdded=false;
    MarkerOptions marker = new MarkerOptions();
    private TextView locationTv;
    String strPlaceResponse=null;
    int selectedType=0;
    public static boolean hasNextToken=true;
    AppCompatActivity activity;
    private Context context;
    private RecyclerView recyclerView;
    List<DefaultMasjidList> placesList;
    private Toolbar toolbar;
    private ProgressDialog pDialog;
    public String RANDOM_STRING;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_masjid_list);
        activity = NearByMasjid.this;
        context = getApplicationContext();
        toolbar = (Toolbar) findViewById(R.id.masjid_list_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Near By Masjid");
        Bundle bundle = getIntent().getExtras();

        char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        RANDOM_STRING = sb.toString();

        recyclerView = (RecyclerView)findViewById(R.id.new_nearby_masjid_recyler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(NearByMasjid.this));
        if(getIntent().getExtras()!=null){
            double latitude= Double.valueOf(getIntent().getStringExtra("latitude"));
            double longitude= Double.valueOf(getIntent().getStringExtra("longitude"));
            strPlaceResponse=getIntent().getStringExtra("Response");
            selectedType= Integer.valueOf(getIntent().getStringExtra("SelectedType"));
            //addingMarkerOnMap(new LatLng(latitude, longitude), "My location!");
            hasNextToken=true;
            showGooglePlaces(strPlaceResponse,latitude,longitude,selectedType);
        }



    }



    private void showGooglePlaces(String result, double currentLat, double currentLong, int selectedType) {
        try {
            JSONObject googlePlacesJson;
            googlePlacesJson = new JSONObject(result);
            Places placeJsonParser = new Places();
            List<HashMap<String, String>> googlePlacesList = null;
            googlePlacesList = placeJsonParser.parse(googlePlacesJson);

            placesList = new ArrayList<DefaultMasjidList>();
            for (int i = 0; i < googlePlacesList.size(); i++) {
                HashMap<String, String> googlePlace = googlePlacesList.get(i);
                PlacesList place = getPlaceInfo(googlePlace, currentLat, currentLong, true);
               DefaultMasjidList defaultMasjid = new DefaultMasjidList();
                defaultMasjid.setName(place.masjidName);
                defaultMasjid.setAddress(place.masjidAddress);
                defaultMasjid.setDistance(place.masjidDistance);
                defaultMasjid.setLatitude(place.masjidLatitude);
                defaultMasjid.setLongitude(place.masjidLongitude);
                defaultMasjid.setId(place.masjidUniqueID);
                defaultMasjid.setPlace_id(place.masjidPlaceID);
                placesList.add(defaultMasjid);

            }
            Collections.sort(placesList, new Comparator<DefaultMasjidList>() {

                @Override
                public int compare(DefaultMasjidList t1, DefaultMasjidList t2) {
                    return (t1.getDistance()).compareTo(t2.getDistance());

                }
            });
            recyclerView.setAdapter(new MasjidListAdapter(placesList, R.layout.new_masjid_items,activity,1));
        }catch (Exception ex){
        }
    }
    private PlacesList getPlaceInfo(HashMap<String, String> googlePlace, double currentLat, double currentLong, boolean isMyRest){
        PlacesList place = new PlacesList();
        place.masjidName = googlePlace.get("place_name");
        place.masjidAddress = googlePlace.get("vicinity");
        place.masjidLatitude = googlePlace.get("lat");
        place.masjidLongitude = googlePlace.get("lng");
        place.masjidPlaceID=googlePlace.get("place_id");
        place.masjidUniqueID=googlePlace.get("id");
        place.masjidRating = googlePlace.get("rating");
        place.masjidWorkingStatus = googlePlace.get("is_open");
        place.isMyMasjid = isMyRest;
        double lat = Double.parseDouble(googlePlace.get("lat"));
        double lng = Double.parseDouble(googlePlace.get("lng"));
        Log.d(TAG, "Place name :" + place.masjidName + "--" + place.masjidRating +
                "--" + place.masjidWorkingStatus);
        double dist = 0;
        String distKM = null;
        if (lat != 0 && lng != 0) {
            dist = distanceBtnLocations(currentLat, currentLong, lat, lng);
            dist = dist / 1000;
            distKM = new DecimalFormat("#0.0").format(dist);
        }
        place.masjidDistance = distKM ;
        return place;
    }

    private double distanceBtnLocations(double lat1, double lon1, double lat2, double lon2) {
        Location selected_location=new Location("locationA");
        selected_location.setLatitude(lat1);
        selected_location.setLongitude(lon1);
        Location near_locations=new Location("locationB");
        near_locations.setLatitude(lat2);
        near_locations.setLongitude(lon2);
        double distance=selected_location.distanceTo(near_locations);
        return distance;
    }


    public void callWebservice(int posstion) {
        String name = placesList.get(posstion).getName();
        String address = placesList.get(posstion).getAddress();
        final String Latitiude = placesList.get(posstion).getLatitude();
        final String Logtitude = placesList.get(posstion).getLongitude();
        String place_id = placesList.get(posstion).getPlace_id();
        String unique_id = placesList.get(posstion).getId();

        pDialog = new ProgressDialog(NearByMasjid.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Setting Up...");
        pDialog.setCancelable(false);

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.addSearchMasjid(name,address, Latitiude, Logtitude, unique_id, place_id);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //onSignupSuccess();
                hidepDialog();
                Log.d("onResponse", "" + response.body().getMessage());
                if (response.body().getStatus().equals("success")) {
                    try {
                        List<MasjidInfoList> listList = response.body().getMasjidinfo();
                        AppPreferences.setMasjidId(NearByMasjid.this, listList.get(0).getMasjid_id());
                        AppPreferences.setAdminStatus(NearByMasjid.this, listList.get(0).getAdmin_status());
                        AppPreferences.setMasjidName(NearByMasjid.this, listList.get(0).getMasjid_name());
                        AppPreferences.setMasjidAddress(NearByMasjid.this, listList.get(0).getAddress());
                        AppPreferences.setLatitiude(NearByMasjid.this, Latitiude);
                        AppPreferences.setLogtitude(NearByMasjid.this, Logtitude);
                        Intent intent = new Intent(NearByMasjid.this, MasjidActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        activity = NearByMasjid.this;
                        finish();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(NearByMasjid.this, "Please try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

//            if(selectedType==2) {
//                String name = placesList.get(posstion).getName();
//                String address = placesList.get(posstion).getAddress();
//                final String Latitiude = placesList.get(posstion).getLatitude();
//                final String Logtitude = placesList.get(posstion).getLongitude();
//                String place_id = placesList.get(posstion).getPlace_id();
//                String unique_id = placesList.get(posstion).getId();
//
//                pDialog = new ProgressDialog(NearByMasjid.this,
//                        R.style.AppTheme_Dark_Dialog);
//                pDialog.setIndeterminate(true);
//                pDialog.setMessage("Setting Up...");
//                pDialog.setCancelable(false);
//
//                showpDialog();
//                if (AppPreferences.getRandomId(NearByMasjid.this) != "") {
//                    RANDOM_STRING = AppPreferences.getRandomId(NearByMasjid.this);
//                }
//
//                APIService service = ApiClinet.getClient().create(APIService.class);
//
//
//                Call<MSG> userCall = service.addDefaultMasjid(name, RANDOM_STRING, address, Latitiude, Logtitude, unique_id, place_id);
//
//                userCall.enqueue(new Callback<MSG>() {
//                    @Override
//                    public void onResponse(Call<MSG> call, Response<MSG> response) {
//                        //onSignupSuccess();
//                        hidepDialog();
//                        Log.d("onResponse", "" + response.body().getMessage());
//                        AppPreferences.setRandomId(NearByMasjid.this, RANDOM_STRING);
//                        if (response.body().getStatus().equals("success")) {
//                            try {
//                                List<MasjidInfoList> listList = response.body().getMasjidinfo();
//                                AppPreferences.setUserId(NearByMasjid.this, listList.get(0).getUser_id());
//                                AppPreferences.setMasjidId(NearByMasjid.this, listList.get(0).getMasjid_id());
//                                AppPreferences.setAdminStatus(NearByMasjid.this, listList.get(0).getAdmin_status());
//                                AppPreferences.setMasjidName(NearByMasjid.this, listList.get(0).getMasjid_name());
//                                AppPreferences.setMasjidAddress(NearByMasjid.this, listList.get(0).getAddress());
//                                AppPreferences.setLatitiude(NearByMasjid.this, Latitiude);
//                                AppPreferences.setLogtitude(NearByMasjid.this, Logtitude);
//                                String s = AppPreferences.getLatitiude(NearByMasjid.this);
//                                String s2 = AppPreferences.getLogtitude(NearByMasjid.this);
//
//
//
//
//                                activity = NearByMasjid.this;
//
//                            } catch (NullPointerException e) {
//                                e.printStackTrace();
//                            }
//
//                        } else {
//                            Toast.makeText(NearByMasjid.this, "Please try Again", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<MSG> call, Throwable t) {
//                        hidepDialog();
//                        Log.d("onFailure", t.toString());
//                    }
//                });
//            }else{
//
//            }


    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}


