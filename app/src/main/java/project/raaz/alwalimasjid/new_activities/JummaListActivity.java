package project.raaz.alwalimasjid.new_activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.activities.WebViewActivity;
import project.raaz.alwalimasjid.adapters.ReportListAdapter;
import project.raaz.alwalimasjid.new_adapters.JummaInfoListAdapter;
import project.raaz.alwalimasjid.pojo_class.PrayerInfo;
import project.raaz.alwalimasjid.pojo_class.ReportList;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/10/2017.
 */

public class JummaListActivity extends AppCompatActivity{
    private RecyclerView recyclerView;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jumma_info_list);
        recyclerView = (RecyclerView)findViewById(R.id.jumma_info_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(JummaListActivity.this));
        callingReportList();
    }

    private void callingReportList() {
        pDialog = new ProgressDialog(JummaListActivity.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        String limit = "0",offset="0";
        Call<MSG> userCall = service.JummaInfo(AppPreferences.getMasjidId(JummaListActivity.this),"",limit,offset);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<PrayerInfo> prayerInfos = response.body().getPrayerinfo();

                    recyclerView.setAdapter(new JummaInfoListAdapter(prayerInfos, R.layout.new_masjid_jumma_items, JummaListActivity.this));

//                    Toast.makeText(ReportListFragment.this, "" + response.body().getReportList(), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(JummaListActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void imageView(String link) {
        Intent intent = new Intent(JummaListActivity.this, WebViewActivity.class);
        intent.putExtra("image_link",link);
        startActivity(intent);
    }
}
