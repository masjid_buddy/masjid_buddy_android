package project.raaz.alwalimasjid.new_activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 11/24/2017.
 */

public class JummaUpdate extends AppCompatActivity {
    private Toolbar toolbar;
    TextInputEditText katheeb_name;
    Button submit;
    TextView time_sala,time_kuthuba,date;

    private ProgressDialog pDialog;

    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_jumma_update);
        toolbar= (Toolbar) findViewById(R.id.jumma_update_tool_bar);
        setSupportActionBar(toolbar);

        katheeb_name = (TextInputEditText)findViewById(R.id.jumma_update_tiet_khateeb);
        date = (TextView) findViewById(R.id.jumma_update_txt_date);
        time_sala = findViewById(R.id.jumma_update_salah_time);
        time_kuthuba = findViewById(R.id.jumma_update_khutbah_time);


        submit = (Button)findViewById(R.id.jumma_update_btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (date.getText().equals("")){
                    Toast.makeText(JummaUpdate.this, "Put all details", Toast.LENGTH_SHORT).show();
                }else if(time_kuthuba.equals("")){
                    Toast.makeText(JummaUpdate.this, "Put all details", Toast.LENGTH_SHORT).show();

                }else if(time_sala.equals("")){
                    Toast.makeText(JummaUpdate.this, "Put all details", Toast.LENGTH_SHORT).show();

                }else if(katheeb_name.equals("")){
                    Toast.makeText(JummaUpdate.this, "Put all details", Toast.LENGTH_SHORT).show();

                }else{
                    callWebservice();

                }
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(JummaUpdate.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        time_kuthuba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(JummaUpdate.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String format;
                        if (selectedHour == 0) {
                            selectedHour += 12;
                            format = "AM";

                            time_kuthuba.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour == 12) {
                            format = "PM";
                            time_kuthuba.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour > 12) {
                            selectedHour -= 12;
                            format = "PM";
                            time_kuthuba.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else {
                            format = "AM";
                            time_kuthuba.setText( selectedHour + ":" + selectedMinute+" "+format);
                        }


                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


        time_sala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(JummaUpdate.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String format;
                        if (selectedHour == 0) {
                            selectedHour += 12;
                            format = "AM";

                            time_sala.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour == 12) {
                            format = "PM";
                            time_sala.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour > 12) {
                            selectedHour -= 12;
                            format = "PM";
                            time_sala.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else {
                            format = "AM";
                            time_sala.setText( selectedHour + ":" + selectedMinute+" "+format);
                        }


                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
    }


    private void callWebservice() {
//        Toast.makeText(PrayerTimeUpdate.this, "Waiting For Webservice", Toast.LENGTH_SHORT).show();

        pDialog = new ProgressDialog(JummaUpdate.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("getting data...");
        pDialog.setCancelable(false);

        showpDialog();
        String str_masjid_id = AppPreferences.getMasjidId(JummaUpdate.this);




        APIService service = ApiClinet.getClient().create(APIService.class);


        Call<MSG> userCall = service.JummaUpdate(str_masjid_id,date.getText().toString(),time_kuthuba.getText().toString(),time_sala.getText().toString(),katheeb_name.getText().toString());
                userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body());


                if(response.body().getStatus().equals("success")) {
                    finish();

                    Toast.makeText(JummaUpdate.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(JummaUpdate.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });



    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
