package project.raaz.alwalimasjid.new_activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.OfferInfo;
import project.raaz.alwalimasjid.OfferList;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.fagments.PoolingListOfferRide;
import project.raaz.alwalimasjid.new_adapters.CarPoolingListOfferAdapter;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JoinRides extends AppCompatActivity {
    Activity activity;

    private Toolbar toolbar;
    Button acceptButton;
    String id,accept_status,accepted_by;
    private ProgressDialog pDialog;
    TextView txt_from,txt_to,txt_stops,txt_date,txt_time,txt_waqth,txt_decrease,txt_increase,txt_seats,txt_name,txt_mobile,txt_email;
    Button btn_join;
    int seat_count=0;
    int available_seat_count=0;

    LinearLayout seatAvailableLayout;

    Spinner PickupPoints;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_join_ride);
        toolbar = (Toolbar) findViewById(R.id.join_ride_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Join Ride Info");
        activity = JoinRides.this;
        txt_from = findViewById(R.id.join_ride_from);
        txt_to = findViewById(R.id.join_ride_to);
        txt_date = findViewById(R.id.join_ride_date);
        txt_time = findViewById(R.id.join_ride_time);
        txt_seats = findViewById(R.id.join_ride_seat);
        txt_decrease = findViewById(R.id.join_ride_seat_decrease);
        txt_increase = findViewById(R.id.join_ride_seat_increase);
        txt_stops = findViewById(R.id.join_ride_stops);
        txt_waqth = findViewById(R.id.join_ride_waqth);
        btn_join = findViewById(R.id.join_ride_btn);
        seatAvailableLayout =findViewById(R.id.available_seat_layout);
        PickupPoints = findViewById(R.id.join_rides_picup_points);
        txt_name = findViewById(R.id.join_raid_name);
        txt_mobile = findViewById(R.id.join_raid_mobile);
        txt_email = findViewById(R.id.join_raid_email);
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("Id");
        callViewOffer();


        txt_decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!txt_seats.getText().equals("0")){
                    int i = Integer.parseInt(txt_seats.getText().toString());
                    i--;
                    txt_seats.setText(""+i);
                }
            }
        });

        txt_increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = Integer.parseInt(txt_seats.getText().toString());
                if (i<available_seat_count){
                    i++;
                    txt_seats.setText(""+i);
                }else{
                    Toast.makeText(activity, "No more seats available to choose", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void callViewOffer() {

        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.ViewOffer(id);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<OfferInfo> offerInfos = response.body().getOfferinfo();

                    txt_from.setText(offerInfos.get(0).getStartingpoint());
                    txt_to.setText(offerInfos.get(0).getDestination());
                    txt_date.setText(offerInfos.get(0).getDate());
                    txt_time.setText(offerInfos.get(0).getTime());
                    txt_waqth.setText(offerInfos.get(0).getPrayer());
                    txt_name.setText(offerInfos.get(0).getName());
                    txt_email.setText(offerInfos.get(0).getEmail());
                    txt_mobile.setText(offerInfos.get(0).getMobile());
                    String picup = offerInfos.get(0).getPickuppoint();

                    List<String> picupPoints = Arrays.asList(picup.split(":"));
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(activity,
                            android.R.layout.simple_spinner_item, picupPoints);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                    PickupPoints.setAdapter(spinnerAdapter);

                    seat_count = Integer.parseInt(offerInfos.get(0).getTotalseats());
                    available_seat_count = Integer.parseInt(offerInfos.get(0).getAvailableseat());

                    for (int i = 0; i < seat_count; i++) {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        final ImageView rowImageView= new ImageView(activity);
                        rowImageView.setLayoutParams(params);
                        rowImageView.setPadding(5, 5, 5, 5);
                        if(i<available_seat_count){
                            rowImageView.setBackgroundResource(R.drawable.new_ic_seat);

                        }else{
                            rowImageView.setBackgroundResource(R.drawable.new_ic_seat_available);

                        }
                        seatAvailableLayout.addView(rowImageView);

                    }





                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public void onClickJoinRide(View view) {
        if(!txt_seats.getText().equals("0")) {
            String user_id = AppPreferences.getUserId(activity);
            String pickup_point = PickupPoints.getSelectedItem().toString();
            String seat=txt_seats.getText().toString();

            pDialog = new ProgressDialog(activity,
                    R.style.AppTheme_Dark_Dialog);
            pDialog.setIndeterminate(true);
            pDialog.setMessage("Getting List...");
            pDialog.setCancelable(false);

            showpDialog();

            APIService service = ApiClinet.getClient().create(APIService.class);

            Call<MSG> userCall = service.JointRide(user_id, id, pickup_point, seat);

            userCall.enqueue(new Callback<MSG>() {
                @Override
                public void onResponse(Call<MSG> call, Response<MSG> response) {
                    hidepDialog();
                    //onSignupSuccess();
                    Log.d("onResponse", "" + response.body().getMessage());


                    if (response.body().getStatus().equals("success")) {

                        Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MSG> call, Throwable t) {
                    hidepDialog();
                    Log.d("onFailure", t.toString());
                }
            });
        }
    }


}
