package project.raaz.alwalimasjid.new_activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.OfferInfo;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.pojo_class.RequestInfo;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcceptRequest extends AppCompatActivity {
    Activity activity;

    private Toolbar toolbar;
    String id;
    private ProgressDialog pDialog;
    TextView txt_from,txt_to,txt_date,txt_time,txt_waqth,txt_seats,txt_name,txt_mobile,txt_email;
    Button btn_join;
    int seat_count=0;
    int available_seat_count=0;


    TextView PickupPoints;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_accept_request);
        toolbar = (Toolbar) findViewById(R.id.accept_request_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Request Info");

        activity = AcceptRequest.this;
        txt_from = findViewById(R.id.accept_request_from);
        txt_to = findViewById(R.id.accept_request_to);
        txt_date = findViewById(R.id.accept_request_date);
        txt_time = findViewById(R.id.accept_request_time);
        txt_seats = findViewById(R.id.accept_request_total);
        txt_waqth = findViewById(R.id.accept_request_waqth);
        txt_name = findViewById(R.id.accept_raid_name);
        txt_email = findViewById(R.id.accept_raid_email);
        txt_mobile = findViewById(R.id.accept_raid_mobile);
        PickupPoints = findViewById(R.id.accept_request_picup_location);
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("Id");
        callViewRequest();

    }



    private void callViewRequest() {


        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(true);
        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.ViewRequest(id);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<RequestInfo> requestInfos = response.body().getRequestinfo();

                    txt_from.setText(requestInfos.get(0).getStartingpoint());
                    txt_to.setText(requestInfos.get(0).getDestination());
                    txt_date.setText(requestInfos.get(0).getDate());
                    txt_time.setText(requestInfos.get(0).getTime());
                    txt_waqth.setText(requestInfos.get(0).getPrayer());
                    txt_seats.setText(requestInfos.get(0).getReqseats());
                    String picup = requestInfos.get(0).getPickuppoint();
                    PickupPoints.setText(picup);
                    txt_name.setText(requestInfos.get(0).getName());
                    txt_mobile.setText(requestInfos.get(0).getMobile());
                    txt_email.setText(requestInfos.get(0).getEmail());



                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public void onClickAcceptRequest(View view) {
            String user_id = AppPreferences.getUserId(activity);


            showpDialog();

            APIService service = ApiClinet.getClient().create(APIService.class);

            Call<MSG> userCall = service.AcceptRequest(user_id,id,"");

            userCall.enqueue(new Callback<MSG>() {
                @Override
                public void onResponse(Call<MSG> call, Response<MSG> response) {
                    hidepDialog();
                    //onSignupSuccess();
                    Log.d("onResponse", "" + response.body().getMessage());


                    if (response.body().getStatus().equals("success")) {

                        Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MSG> call, Throwable t) {
                    hidepDialog();
                    Log.d("onFailure", t.toString());
                }
            });

    }


}
