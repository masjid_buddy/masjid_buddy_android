package project.raaz.alwalimasjid.new_activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.new_fragment.CarPoolOffers;
import project.raaz.alwalimasjid.new_fragment.CarPoolRequestOffer;
import project.raaz.alwalimasjid.new_fragment.DirectionFragment;
import project.raaz.alwalimasjid.new_fragment.MyOffer;
import project.raaz.alwalimasjid.new_fragment.MyRequest;

public class CarPoolActivity extends AppCompatActivity {
    private Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_car_pooling);
//        toolbar = (Toolbar) findViewById(R.id.new_car_pool_toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle("Car Pooling");

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation_carpool);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_alloffer:
                                selectedFragment = CarPoolOffers.newInstance();
                                break;
                            case R.id.action_newoffer:
                                selectedFragment = CarPoolRequestOffer.newInstance();
                                break;
                            case R.id.action_myoffer:
                                selectedFragment = MyOffer.newInstance();
                                break;

                            case R.id.action_myrequest:
                                selectedFragment = MyRequest.newInstance();
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.new_car_pool_frame_layout, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.new_car_pool_frame_layout, CarPoolOffers.newInstance());
        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }
}
