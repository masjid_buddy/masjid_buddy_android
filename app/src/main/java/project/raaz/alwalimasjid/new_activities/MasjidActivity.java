/*
 * Copyright (c) 2016. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package project.raaz.alwalimasjid.new_activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.Login;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.Register;
import project.raaz.alwalimasjid.activities.CategoryAdmin;
import project.raaz.alwalimasjid.activities.RamadanPrayerActivity;
import project.raaz.alwalimasjid.new_compass.CompassActivity;
import project.raaz.alwalimasjid.new_fragment.DirectionFragment;
import project.raaz.alwalimasjid.new_fragment.HomeFragment;
import project.raaz.alwalimasjid.new_fragment.Profile;
import project.raaz.alwalimasjid.new_fragment.SearchFragment;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTime;
import project.raaz.alwalimasjid.prayertimeapi.PrayerTimeApiService;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MasjidActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_masjid_activity);
        toolbar = (Toolbar) findViewById(R.id.masjid_activity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Masjid Buddy");

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                selectedFragment = HomeFragment.newInstance();
                                break;
                            case R.id.action_search:
//                                selectedFragment = SearchFragment.newInstance();
                                Intent intent = new Intent(MasjidActivity.this, MyMasjid.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                                break;
                            case R.id.action_direction:
//                                selectedFragment = DirectionFragment.newInstance();
                                intent = new Intent(MasjidActivity.this, CompassActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                                break;
                            case R.id.action_masjid_admin:

                                if (AppPreferences.getUserId(MasjidActivity.this).equals("")){
                                    startActivity(new Intent(MasjidActivity.this,Login.class).putExtra("activity","admin"));
                                }else{
                                    callMasjidAdminAvailability();


                                }
                                break;
                            case R.id.action_profile:
                                if (AppPreferences.getUserId(MasjidActivity.this).equals("")){
                                    startActivity(new Intent(MasjidActivity.this,Login.class).putExtra("activity","admin"));
                                }else{
                                    selectedFragment = Profile.newInstance();


                                }
                                break;
                        }
                        try {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_layout, selectedFragment);
                            transaction.commit();
                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }
                        return true;
                    }
                });

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, HomeFragment.newInstance());
        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }

    private void callMasjidAdminAvailability() {
        pDialog = new ProgressDialog(MasjidActivity.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);

        showpDialog();




        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.AdminValidation(AppPreferences.getUserId(MasjidActivity.this),AppPreferences.getMasjidId(MasjidActivity.this));

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());
                if(response.body().getStatus().equals("success")) {
                    startActivity(new Intent(MasjidActivity.this,AdminActivity.class));
                }else if(response.body().getStatus().equals("ending")){
                    Toast.makeText(MasjidActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MasjidActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, HomeFragment.newInstance());
        transaction.commit();
    }
}
