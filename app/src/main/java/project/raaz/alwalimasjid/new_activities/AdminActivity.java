package project.raaz.alwalimasjid.new_activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.new_fragment.BusShuttleUpdateFragment;
import project.raaz.alwalimasjid.new_fragment.EventUpdateFragment;
import project.raaz.alwalimasjid.new_fragment.IqamaTimeUpdate;
import project.raaz.alwalimasjid.new_fragment.ReportListFragment;

public class AdminActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_admin_activity);
        toolbar = (Toolbar) findViewById(R.id.admin_activity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Masjid Buddy Admin");

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.admin_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_iqama_update:
                                selectedFragment = IqamaTimeUpdate.newInstance();
                                break;
                            case R.id.action_event_update:
                                selectedFragment = EventUpdateFragment.newInstance();
                                break;
                            case R.id.action_bus_shuttle_update:
                                selectedFragment = BusShuttleUpdateFragment.newInstance();
                                break;
                            case R.id.action_view_complaints:
                                selectedFragment = ReportListFragment.newInstance();
                                break;
                        }
                        try {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.admin_frame_layout, selectedFragment);
                            transaction.commit();
                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }
                        return true;
                    }
                });

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.admin_frame_layout, IqamaTimeUpdate.newInstance());
        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }
}
