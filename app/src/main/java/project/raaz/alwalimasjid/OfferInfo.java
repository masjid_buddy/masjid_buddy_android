package project.raaz.alwalimasjid;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rasheed on 11/3/2017.
 */

public class OfferInfo {
    @SerializedName("id")
    private String id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("startingpoint")
    String startingpoint;
    @SerializedName("destination")
    String destination;
    @SerializedName("pickuppoint")
    String pickuppoint;
    @SerializedName("prayer")
    private String prayer;
    @SerializedName("time")
    private String time;
    @SerializedName("totalseats")
    private String totalseats;

    @SerializedName("availableseat")
    private String availableseat;

    @SerializedName("accept_status")
    private String accept_status;
    @SerializedName("accepted_offer_id")
    private String accepted_offer_id;

    @SerializedName("date")
    private String date;

    @SerializedName("accepted_by")
    private String accepted_by;


    @SerializedName("name")
    private String name;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("email")
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStartingpoint() {
        return startingpoint;
    }

    public void setStartingpoint(String startingpoint) {
        this.startingpoint = startingpoint;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getPickuppoint() {
        return pickuppoint;
    }

    public void setPickuppoint(String pickuppoint) {
        this.pickuppoint = pickuppoint;
    }

    public String getPrayer() {
        return prayer;
    }

    public void setPrayer(String prayer) {
        this.prayer = prayer;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getAccept_status() {
        return accept_status;
    }

    public void setAccept_status(String accept_status) {
        this.accept_status = accept_status;
    }

    public String getAccepted_offer_id() {
        return accepted_offer_id;
    }

    public void setAccepted_offer_id(String accepted_offer_id) {
        this.accepted_offer_id = accepted_offer_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAccepted_by() {
        return accepted_by;
    }

    public void setAccepted_by(String accepted_by) {
        this.accepted_by = accepted_by;
    }

    public String getTotalseats() {
        return totalseats;
    }

    public void setTotalseats(String totalseats) {
        this.totalseats = totalseats;
    }

    public String getAvailableseat() {
        return availableseat;
    }

    public void setAvailableseat(String availableseat) {
        this.availableseat = availableseat;
    }
}
