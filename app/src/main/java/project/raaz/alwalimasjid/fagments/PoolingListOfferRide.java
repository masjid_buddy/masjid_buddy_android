package project.raaz.alwalimasjid.fagments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.OfferInfo;
import project.raaz.alwalimasjid.OfferList;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.RequestList;
import project.raaz.alwalimasjid.activities.CarPoolingDetaileView;
import project.raaz.alwalimasjid.new_activities.JoinRides;
import project.raaz.alwalimasjid.new_adapters.CarPoolingListOfferAdapter;
import project.raaz.alwalimasjid.pojo_class.RequestInfo;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PoolingListOfferRide extends Fragment {

    private ProgressDialog pDialog;
    Activity activity;
    private RecyclerView recyclerView;
    Context context;
    String txt_name;
    String txt_mobile;
    String txt_prayer;
    String txt_time;
    String txt_area;
    String txt_location;
    String txt_seats;
    String acceptMobile;
    String accept_status;

    int flag = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.car_pooling_list, container, false);
        activity = getActivity();
        context = getContext();
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);

        recyclerView = (RecyclerView) view.findViewById(R.id.movies_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));

        callingRequest();
        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        callingRequest();
    }


    private void callingRequest() {
        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        String ride_type = "2",limit = "0",offset="0";
        Call<MSG> userCall = service.OfferList("",limit,offset);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {
                    List<OfferList> requestlist = response.body().getOfferlist();
                    recyclerView.setAdapter(new CarPoolingListOfferAdapter(requestlist, R.layout.new_offer_list_itmes, context,PoolingListOfferRide.this));
                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }




    public void GotoDetaile(final String id) {
        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        String ride_type = id;
            Call<MSG> userCall = service.ViewOffer(id);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<OfferInfo> requestDetails = response.body().getOfferinfo();

                    txt_name = ("");
                    txt_mobile = ("");
                    txt_prayer = (requestDetails.get(0).getPrayer());
                    txt_time = (requestDetails.get(0).getTime());
                    txt_area = (requestDetails.get(0).getStartingpoint());
                    txt_location = (requestDetails.get(0).getDestination());
                    txt_seats = (requestDetails.get(0).getTotalseats());
                    acceptMobile = (requestDetails.get(0).getAccepted_by());
                    accept_status = requestDetails.get(0).getAccept_status();

                    goTo(id);

                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }

    private void goTo(String id) {
        Intent intent = new Intent(activity,JoinRides.class);
        intent.putExtra("Id",id);
        startActivity(intent);


        Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
        Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
        Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
        Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
        Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
        Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
        Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
        Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
        Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
        Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
        Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));
    }
}
