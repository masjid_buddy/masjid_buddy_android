package project.raaz.alwalimasjid.fagments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.OfferList;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyOffer extends Fragment {
    private ProgressDialog pDialog;
    Activity activity;
    private RecyclerView recyclerView;
    Context context;

    public static MyOffer newInstance() {
        MyOffer fragment = new MyOffer();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.car_pooling_list, container, false);
        activity = getActivity();
        context = getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.movies_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));

            callingRequest();

        // Inflate the layout for this fragment
        return view;
    }



    private void callingRequest() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);
        String str_mobile = AppPreferences.getMobileNumber(context);


        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.OfferList(AppPreferences.getUserId(activity),"","");

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<OfferList> requestlist = response.body().getOfferlist();




                    Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                    Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                    Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                    Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                    Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                    Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                    Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                    Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                    Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                    Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                    Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

//                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else {
//                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public void goTo() {

    }
}


