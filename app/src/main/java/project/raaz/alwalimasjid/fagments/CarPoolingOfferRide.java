package project.raaz.alwalimasjid.fagments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class CarPoolingOfferRide extends Fragment {
    Spinner spinner_type;
    TextView time;
    EditText area,street,seat;
    Button request;
    Activity activity;
    ArrayAdapter<CharSequence> spinner_adapter;
    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.car_pooling_request_ride, container, false);
        activity = getActivity();

        spinner_type = view.findViewById(R.id.plg_spinner_type);
        time = view.findViewById(R.id.plg_txt_time);
        seat = view.findViewById(R.id.plg_tiet_seat);
        area = view.findViewById(R.id.plg_tiet_area);
        street = view.findViewById(R.id.plg_tiet_street);
        request = view.findViewById(R.id.plg_btn_request);
        spinner_adapter = ArrayAdapter.createFromResource(activity, R.array.prayer_type,R.layout.spinner_item);
        spinner_type.setSelection(spinner_adapter.getCount());
        spinner_type.setAdapter(spinner_adapter);

        time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String format;
                        if (selectedHour == 0) {
                            selectedHour += 12;
                            format = "AM";

                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour == 12) {
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else if (selectedHour > 12) {
                            selectedHour -= 12;
                            format = "PM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        } else {
                            format = "AM";
                            time.setText( selectedHour + ":" + selectedMinute+" "+format);
                        }

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Request();
            }
        });
        // Inflate the layout for this fragment
        return view;
    }




    private void Request() {
        Log.d(TAG, "Pooling_Request");

        if (validate() == false) {
            onRequestFailed();
            return;
        }
        requestByServer();
    }

    private void requestByServer() {
        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Request...");
        pDialog.setCancelable(false);

        showpDialog();

        final String str_prayer, str_time, str_street, str_area, str_request_type,str_user_name=null,str_user_mobile = null,str_seat;



        str_time = time.getText().toString();
        str_street = street.getText().toString();
        str_area = area.getText().toString();
        str_request_type = "2";
        str_seat = seat.getText().toString();


        String spinnerValue = spinner_type.getSelectedItem().toString();
        if(spinnerValue.equals("Choose Prayer Time"))
        {
            str_prayer="";

        }else{
            str_prayer = spinnerValue;
        }

        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.userAddRequest(str_user_name,str_user_mobile,str_prayer, str_time, str_street, str_area, str_request_type,str_seat);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {
                    activity.finish();

                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });
    }

    private void onRequestFailed() {
        Toast.makeText(activity, "request failed", Toast.LENGTH_LONG).show();
    }

    private boolean validate() {
        boolean valid = true;
        String str_prayer, str_time, str_street, str_area, str_request_type;
        str_time = time.getText().toString();
        str_street = street.getText().toString();
        str_area = area.getText().toString();

        return valid;
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
