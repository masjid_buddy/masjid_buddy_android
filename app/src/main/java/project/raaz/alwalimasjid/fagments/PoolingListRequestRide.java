package project.raaz.alwalimasjid.fagments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.APIService;
import project.raaz.alwalimasjid.ApiClinet;
import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.RequestList;
import project.raaz.alwalimasjid.activities.CarPoolingDetaileView;
import project.raaz.alwalimasjid.new_activities.AcceptRequest;
import project.raaz.alwalimasjid.new_adapters.CarPoolingListRequestAdapter;
import project.raaz.alwalimasjid.pojo_class.RequestInfo;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PoolingListRequestRide extends Fragment {

    private ProgressDialog pDialog;
    Activity activity;
    private RecyclerView recyclerView;
    Context context;
    String txt_name;
    String txt_mobile;
    String txt_prayer;
    String txt_time;
    String txt_area;
    String txt_location;
    String txt_seats;
    String acceptMobile;
    String accept_status;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.car_pooling_list, container, false);
        activity = getActivity();
        context = getContext();

        pDialog = new ProgressDialog(activity,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Getting List...");
        pDialog.setCancelable(false);

        recyclerView = (RecyclerView) view.findViewById(R.id.movies_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));

        callingRequest();
        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        callingRequest();
    }

    private void callingRequest() {

        showpDialog();

        APIService service = ApiClinet.getClient().create(APIService.class);

        String ride_type = "1",limit = "0",offset="0";
        Call<MSG> userCall = service.RequestList("",limit,offset);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());


                if(response.body().getStatus().equals("success")) {

                    List<RequestList> requestlist = response.body().getRequestlist();

                    recyclerView.setAdapter(new CarPoolingListRequestAdapter(requestlist, R.layout.new_offer_list_itmes, context,PoolingListRequestRide.this));

//                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }





    public void goTo(String id) {
        Intent intent = new Intent(activity,AcceptRequest.class);
        intent.putExtra("Id",id);
        startActivity(intent);

    }


}
