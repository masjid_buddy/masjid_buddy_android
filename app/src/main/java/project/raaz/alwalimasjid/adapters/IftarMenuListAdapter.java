package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.pojo_class.Iftharmenu;

public class IftarMenuListAdapter extends RecyclerView.Adapter<IftarMenuListAdapter.ViewHolder> {

    private List<Iftharmenu> iftharmenus;
    private int rowLayout;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView listLayout;
        TextView title;
        TextView description;
        TextView date;
        TextView time;
        TextView location;


        public ViewHolder(View v) {
            super(v);
            listLayout = (CardView) v.findViewById(R.id.iftar_men_list_card_view);
            title = (TextView) v.findViewById(R.id.iftar_menu_list_masjid);
            description = (TextView) v.findViewById(R.id.iftar_menu_item_name);

        }
    }

    public IftarMenuListAdapter(List<Iftharmenu> iftharmenus, int rowLayout, Context context) {
        this.iftharmenus = iftharmenus;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public IftarMenuListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.title.setText(iftharmenus.get(position).getMasjid_name());
                String menu = iftharmenus.get(position).getMenu();
        int j=1;
        List<String> items = Arrays.asList(menu.split(","));
        for(int i=0; i<items.size();i++){

            holder.description.append("\n "+""+j+". "+items.get(i));
            j++;
        }




            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "upcoming", Toast.LENGTH_SHORT).show();
                }
            });


    }

    @Override
    public int getItemCount()
    {
        return iftharmenus.size();
    }
}