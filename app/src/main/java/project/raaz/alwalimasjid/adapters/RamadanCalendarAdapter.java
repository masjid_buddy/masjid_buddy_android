package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import project.raaz.alwalimasjid.MSG;
import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.activities.RamadanHome;
import project.raaz.alwalimasjid.pojo_class.Calendarinfo;
import project.raaz.alwalimasjid.prayertimeapi.Data;
import retrofit2.Callback;

/**
 * Created by rasheed on 11/24/2017.
 */

public class RamadanCalendarAdapter extends BaseAdapter {


    Context context;
    List<Data>data;
    int [] imageId;
    private static LayoutInflater inflater=null;
    public RamadanCalendarAdapter(Context context) {
        // TODO Auto-generated constructor stub
        context=context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public RamadanCalendarAdapter(Context context, List<Data> data) {
        this.context=context;
        this.data=data;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView cal_text,date,iftar,sehri;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.calander_view_items, null);
        holder.cal_text =(TextView) rowView.findViewById(R.id.text_calendar);
        holder.date =(TextView) rowView.findViewById(R.id.calender_view_item_date);
        holder.iftar  =(TextView) rowView.findViewById(R.id.calender_view_item_iftar);
        holder.sehri  =(TextView) rowView.findViewById(R.id.calender_view_item_sehri);
        String iftar = data.get(position).getTimings().getSunset();
        String suhoor =data.get(position).getTimings().getImsak();

        String newIftar = iftar.replace(" (IST)","");
        String newSuhoor = suhoor.replace(" (IST)","");

        String[] str_ifatr = newIftar.split(":");
        int iftarHours = Integer.parseInt(str_ifatr[0]);
        String iftarMinutes = str_ifatr[1];


        String[] str_suhoor = newSuhoor.split(":");
        int suhoorHours = Integer.parseInt(str_suhoor[0]);
        String suhoorMinutes = str_suhoor[1];




        holder.cal_text.setText(""+data.get(position).getDate().getHijri().getDay());
        holder.date.setText(""+data.get(position).getDate().getGregorian().getDate());
        holder.iftar.setText(getTimeFormate(iftarHours,iftarMinutes));
        holder.sehri.setText(getTimeFormate(suhoorHours,suhoorMinutes));

//        rowView.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_SHORT).show();
//            }
//        });

        return rowView;
    }


    public String getTimeFormate(int selectedHour, String selectedMinute) {
        String format;
        String time;
        if (selectedHour == 0) {
            selectedHour += 12;
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour == 12) {
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else if (selectedHour > 12) {
            selectedHour -= 12;
            format = "PM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        } else {
            format = "AM";
            time = selectedHour + ":" + selectedMinute + " " + format;
        }
        return time;
    }
}
