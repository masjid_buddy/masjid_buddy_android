package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import project.raaz.alwalimasjid.R;

/**
 * Created by rasheed on 11/24/2017.
 */

public class CustomAdapter extends BaseAdapter {

    String [] result;
    Context context;
    int [] imageId;
    private static LayoutInflater inflater=null;
    public CustomAdapter(Context context, String[] catName, int[] catImages) {
        // TODO Auto-generated constructor stub
        result=catName;
        context=context;
        imageId=catImages;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView cat_text;
        ImageView cat_img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.grid_view_items, null);
        holder.cat_text =(TextView) rowView.findViewById(R.id.cat_texts);
        holder.cat_img =(ImageView) rowView.findViewById(R.id.cat_images);

        holder.cat_text.setText(result[position]);
        holder.cat_img.setImageResource(imageId[position]);

//        rowView.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_SHORT).show();
//            }
//        });

        return rowView;
    }

}
