package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormatSymbols;
import java.util.List;

import project.raaz.alwalimasjid.EventsList;
import project.raaz.alwalimasjid.R;

public class EventsListAdapter extends RecyclerView.Adapter<EventsListAdapter.ViewHolder> {

    private List<EventsList> ep_list;
    private int rowLayout;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView listLayout;
        TextView title;
        TextView date;
        TextView time;
        TextView location;
        TextView day;


        public ViewHolder(View v) {
            super(v);
            listLayout = (CardView) v.findViewById(R.id.event_list_card_view);
            title = (TextView) v.findViewById(R.id.event_list_title);
            date = (TextView) v.findViewById(R.id.event_list_date);
            time = (TextView) v.findViewById(R.id.event_list_time);
            location = (TextView) v.findViewById(R.id.event_list_location);
            day = v.findViewById(R.id.event_list_day);

        }
    }

    public EventsListAdapter(List<EventsList> movies, int rowLayout, Context context) {
        this.ep_list = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public EventsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String [] dateParts = ep_list.get(position).getDate().split("-");
        String year= dateParts[0];
        String month = dateParts[1];
        String day = dateParts[2];
        String newMonth = getMonthForInt(Integer.parseInt(month)-1);

            holder.date.setText(firstThree(newMonth));
        holder.title.setText(ep_list.get(position).getEvent_title());
        holder.time.setText(ep_list.get(position).getTime());
        holder.location.setText(ep_list.get(position).getLocation());
        holder.day.setText(day);



//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Toast.makeText(context, "upcoming", Toast.LENGTH_SHORT).show();
//                }
//            });


    }

    @Override
    public int getItemCount()
    {
        return ep_list.size();
    }

    String getMonthForInt(int num) {


        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            month = months[num];
        }
        return month;
    }

    public String firstThree(String str) {
        return str.length() < 3 ? str : str.substring(0, 3);
    }
}