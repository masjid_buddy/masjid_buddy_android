package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.pojo_class.ShuttleList;

public class ShuttleListAdapter extends RecyclerView.Adapter<ShuttleListAdapter.ViewHolder> {

    private List<ShuttleList> sp_list;
    private int rowLayout;
    private Context context;
    ShuttleListAdapter myFragment;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView listLayout;
        TextView boading_place;
        TextView boading_time;
        TextView event_name;
        TextView event_place;
        TextView no_stop;
        TextView driver_name;
        TextView driver_number;


        public ViewHolder(View v) {
            super(v);
            listLayout = (CardView) v.findViewById(R.id.bus_shuttle_list_card_layout);
            boading_place = (TextView) v.findViewById(R.id.bus_shuttle_list_boading_place);
            boading_time = (TextView) v.findViewById(R.id.bus_shuttle_list_boading_time);
            event_name = (TextView) v.findViewById(R.id.bus_shuttle_list_event_name);
            event_place = (TextView) v.findViewById(R.id.bus_shuttle_list_event_place);
            no_stop = (TextView) v.findViewById(R.id.bus_shuttle_list_boading_nofstop);
            driver_name = (TextView) v.findViewById(R.id.bus_shuttle_list_boading_driver_name);
            driver_number =(TextView) v.findViewById(R.id.bus_shuttle_list_boading_driver_number);

        }
    }

    public ShuttleListAdapter(List<ShuttleList> shuttleLists, int rowLayout, Context context) {
        this.sp_list = shuttleLists;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public ShuttleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

            holder.event_name.setText(sp_list.get(position).getEventname());
            holder.event_place.setText(sp_list.get(position).getEventplace());
            holder.boading_place.setText(sp_list.get(position).getBoardingplace());
            holder.boading_time.setText(sp_list.get(position).getBoardingtime());
            holder.no_stop.setText(sp_list.get(position).getNoofstops());
            holder.driver_name.setText(sp_list.get(position).getDrivername());
            holder.driver_number.setText(sp_list.get(position).getDrivernumber());




    }



    @Override
    public int getItemCount()
    {
        return sp_list.size();
    }
}