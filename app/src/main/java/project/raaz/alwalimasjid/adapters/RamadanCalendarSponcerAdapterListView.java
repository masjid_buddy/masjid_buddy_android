package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import project.raaz.alwalimasjid.R;

/**
 * Created by rasheed on 11/24/2017.
 */

public class RamadanCalendarSponcerAdapterListView extends BaseAdapter {

    public Integer[] result = {
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30};
    Context context;
    int [] imageId;
    private static LayoutInflater inflater=null;
    public RamadanCalendarSponcerAdapterListView(Context context) {
        // TODO Auto-generated constructor stub
        context=context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView cal_text;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.ramadan_calander_sponcer_items_list, null);
        holder.cal_text =(TextView) rowView.findViewById(R.id.day_list);

        holder.cal_text.setText(""+result[position]);

//        rowView.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_SHORT).show();
//            }
//        });

        return rowView;
    }

}
