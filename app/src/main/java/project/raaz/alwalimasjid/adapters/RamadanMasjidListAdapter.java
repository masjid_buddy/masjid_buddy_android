package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.pojo_class.DefaultMasjidList;

public class RamadanMasjidListAdapter extends RecyclerView.Adapter<RamadanMasjidListAdapter.ViewHolder> {

    private List<DefaultMasjidList> msd_list;
    private int rowLayout;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView listLayout;
        TextView masjid_name;



        public ViewHolder(View v) {
            super(v);
            listLayout = (CardView) v.findViewById(R.id.ramadan_masjid_item_cardview);
            masjid_name = (TextView)v.findViewById(R.id.ramadan_masjid_item_name);


        }
    }

    public RamadanMasjidListAdapter(List<DefaultMasjidList> msg_list, int rowLayout, Context context) {
        this.msd_list = msg_list;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public RamadanMasjidListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

            holder.masjid_name.setText(msd_list.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((project.raaz.alwalimasjid.activities.RamadanNearByMasjid)context).callWebservice(position);

            }
        });

    }

    @Override
    public int getItemCount()
    {
        return msd_list.size();
    }
}