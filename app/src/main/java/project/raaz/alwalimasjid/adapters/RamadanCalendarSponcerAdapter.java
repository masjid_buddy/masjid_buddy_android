package project.raaz.alwalimasjid.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.activities.RamadanHome;
import project.raaz.alwalimasjid.pojo_class.Calendarinfo;

/**
 * Created by rasheed on 11/24/2017.
 */

public class RamadanCalendarSponcerAdapter extends BaseAdapter {

    public Integer[] result = {
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30};
    Context context;
    List<Calendarinfo>calendarinfos;
    int [] imageId;
    private static LayoutInflater inflater=null;
    public RamadanCalendarSponcerAdapter(Context context) {
        // TODO Auto-generated constructor stub
        context=context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public RamadanCalendarSponcerAdapter(Context context, List<Calendarinfo> calendarinfos) {
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.calendarinfos=calendarinfos;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return calendarinfos.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView cal_text,date,share,total_amount;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.ramadan_calander_sponcer_items, null);
        holder.cal_text =(TextView) rowView.findViewById(R.id.ramadan_calender_sponcer_item_fastingday);
        holder.date =(TextView) rowView.findViewById(R.id.ramadan_calender_sponcer_item_date);
        holder.share =(TextView) rowView.findViewById(R.id.ramadan_calender_sponcer_item_sahre);
        holder.total_amount =(TextView) rowView.findViewById(R.id.ramadan_calender_sponcer_item_total_amount);



        holder.cal_text.setText(""+calendarinfos.get(position).getFastingday());
        holder.date.setText(""+ RamadanHome.data.get(position).getDate().getGregorian().getDate());
        holder.share.setText(""+calendarinfos.get(position).getReceivedamount());
        holder.total_amount.setText(""+calendarinfos.get(position).getTotalamount());

//        rowView.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_SHORT).show();
//            }
//        });

        return rowView;
    }

}
