package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.pojo_class.DefaultMasjidList;

public class MasjidListAdapter extends RecyclerView.Adapter<MasjidListAdapter.ViewHolder> {

    private List<DefaultMasjidList> msd_list;
    private int rowLayout;
    private Context context;
    private  int flag;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView listLayout;
        TextView masjid_name;
        TextView masjid_address;
        TextView masjid_distance;
        ImageView submit;



        public ViewHolder(View v) {
            super(v);
            listLayout = (CardView) v.findViewById(R.id.new_masjid_item_cardview);
//            submit = (ImageView) v.findViewById(R.id.default_masjid_item_submit);
            masjid_name = (TextView)v.findViewById(R.id.new_masjid_item_name);
            masjid_address = (TextView)v.findViewById(R.id.new_masjid_item_address);
            masjid_distance=(TextView)v.findViewById(R.id.new_masjid_item_distance);


        }
    }

    public MasjidListAdapter(List<DefaultMasjidList> msg_list, int rowLayout, Context context, int flag) {
        this.msd_list = msg_list;
        this.rowLayout = rowLayout;
        this.context = context;
        this.flag=flag;
    }

    @Override
    public MasjidListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

            holder.masjid_name.setText(msd_list.get(position).getName());
            holder.masjid_address.setText(msd_list.get(position).getAddress());
            holder.masjid_distance.setText(msd_list.get(position).getDistance()+" KM");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(flag==1) {
                    ((project.raaz.alwalimasjid.new_activities.NearByMasjid) context).callWebservice(position);
                }else{
                    ((project.raaz.alwalimasjid.new_activities.MyMasjid) context).callWebservice(position);

                }

            }
        });

    }

    @Override
    public int getItemCount()
    {
        return msd_list.size();
    }
}