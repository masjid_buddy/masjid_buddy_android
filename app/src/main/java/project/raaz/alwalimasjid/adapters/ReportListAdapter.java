package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.ReportListActivity;
import project.raaz.alwalimasjid.pojo_class.ReportList;

public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.ViewHolder> {

    private List<ReportList> rp_list;
    private int rowLayout;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView listLayout;
        ImageView image;
        TextView description;
        TextView date;


        public ViewHolder(View v) {
            super(v);
            listLayout = (CardView) v.findViewById(R.id.report_list_card_view);
            image = (ImageView)v.findViewById(R.id.report_list_image);
            description = (TextView) v.findViewById(R.id.report_list_description);
            date = (TextView) v.findViewById(R.id.report_list_date);
        }
    }

    public ReportListAdapter(List<ReportList> movies, int rowLayout, Context context) {
        this.rp_list = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public ReportListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

            holder.description.setText(rp_list.get(position).getDescription());
            holder.date.setText(rp_list.get(position).getCreated_date());

        Glide.with(context)
                .load(rp_list.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.no_image)
                .into(holder.image);

                final String link = rp_list.get(position).getImage();

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    ((ReportListActivity)context).imageView(link);
                    Toast.makeText(context, "comming soon", Toast.LENGTH_SHORT).show();
                }
            });


    }

    @Override
    public int getItemCount()
    {

        return rp_list.size();
    }
}