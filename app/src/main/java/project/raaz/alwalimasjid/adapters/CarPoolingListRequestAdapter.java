package project.raaz.alwalimasjid.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import project.raaz.alwalimasjid.R;
import project.raaz.alwalimasjid.RequestList;
import project.raaz.alwalimasjid.fagments.PoolingListRequestRide;

public class CarPoolingListRequestAdapter extends RecyclerView.Adapter<CarPoolingListRequestAdapter.ViewHolder> {

    private List<RequestList> rq_list;
    private int rowLayout;
    private Context context;
    PoolingListRequestRide myFragment;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView listLayout;
        TextView name;
        TextView mobile;
        TextView area;
        TextView prayer;
        TextView time;
        TextView accept;
        LinearLayout layout_1,layout_2;
        Button call,message;
//        TextView rating;


        public ViewHolder(View v) {
            super(v);
            listLayout = (CardView) v.findViewById(R.id.car_pooling_list_item_Cardlayout);
            name = (TextView) v.findViewById(R.id.car_plg_rq_name);
            mobile = (TextView) v.findViewById(R.id.car_plg_rq_mobile);
            area = (TextView) v.findViewById(R.id.car_plg_rq_area);
            prayer = (TextView) v.findViewById(R.id.car_plg_rq_prayer);
            time=(TextView) v.findViewById(R.id.car_plg_rq_time);
            call = (Button)v.findViewById(R.id.call);
            message = (Button)v.findViewById(R.id.message);
            accept = (TextView)v.findViewById(R.id.car_plg_rq_accept_mobile);
            layout_1 =(LinearLayout)v.findViewById(R.id.layout_1);
            layout_2 =(LinearLayout)v.findViewById(R.id.layout_2);
        }
    }

    public CarPoolingListRequestAdapter(List<RequestList> movies, int rowLayout, Context context, PoolingListRequestRide myFragment) {
        this.rq_list = movies;
        this.rowLayout = rowLayout;
        this.context = context;
        this.myFragment = myFragment;
    }

    @Override
    public CarPoolingListRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                      int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if(rq_list.get(position).getAccept_status().equals("Pending")) {
            holder.layout_1.setVisibility(View.VISIBLE);
            holder.layout_2.setVisibility(View.INVISIBLE);
//            holder.name.setText(rq_list.get(position).getName());
//            holder.mobile.setText(rq_list.get(position).getMobile());
//            holder.area.setText(rq_list.get(position).getArea());
            holder.prayer.setText(rq_list.get(position).getPrayer());
            holder.time.setText(rq_list.get(position).getTime());
        }else{
            holder.layout_2.setVisibility(View.VISIBLE);
            holder.layout_1.setVisibility(View.INVISIBLE);

//            holder.name.setText(rq_list.get(position).getName());
            holder.accept.setText(rq_list.get(position).getAccepted_by());
            holder.prayer.setText(rq_list.get(position).getPrayer());
            holder.time.setText(rq_list.get(position).getTime());

        }
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(!TextUtils.isEmpty(rq_list.get(position).getMobile())) {
//                    Intent intent = new Intent(Intent.ACTION_DIAL);
//                    String num = rq_list.get(position).getMobile().toString();
//                    intent.setData(Uri.parse("tel:" + num));
//                    context.startActivity(intent);
//
//                }else{
//                    Toast.makeText(context,"There is no number available",Toast.LENGTH_SHORT).show();
//                }
            }
        });

        holder.message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(!TextUtils.isEmpty(rq_list.get(position).getMobile())) {
//                    String num = rq_list.get(position).getMobile().toString();
//                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", num, null)));
//                }else{
//                    Toast.makeText(context,"There is no number available",Toast.LENGTH_SHORT).show();
//                }

            }
        });

//        final String Name = rq_list.get(position).getName().toString();
//        final String Mobile =  rq_list.get(position).getMobile().toString();
//        final String  Prayer =  rq_list.get(position).getPrayer().toString();
//        final String Time =  rq_list.get(position).getTime().toString();
//        final String Street =  rq_list.get(position).getStreet().toString();
//        final String Location =  rq_list.get(position).getArea().toString();
//        final String Seats =  rq_list.get(position).getSeat().toString();
        final String id =rq_list.get(position).getId().toString();
        final String Accept_status = rq_list.get(position).getAccept_status().toString();
        final String Accepted_by = rq_list.get(position).getAccepted_by().toString();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return rq_list.size();
    }
}