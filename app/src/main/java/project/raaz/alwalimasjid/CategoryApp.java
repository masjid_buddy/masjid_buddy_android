package project.raaz.alwalimasjid;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import project.raaz.alwalimasjid.activities.AdminRegister;
import project.raaz.alwalimasjid.activities.BusShuttleListActivity;
import project.raaz.alwalimasjid.activities.CarParkingMapMyMasjid;
import project.raaz.alwalimasjid.activities.CategoryAdmin;
import project.raaz.alwalimasjid.activities.CategoryCarPooling;
import project.raaz.alwalimasjid.activities.EventListActivity;
import project.raaz.alwalimasjid.activities.PrayerActivity;
import project.raaz.alwalimasjid.adapters.CustomAdapter;
import project.raaz.alwalimasjid.utilities.AppPreferences;

/**
 * Created by rasheed on 10/16/2017.
 */

public class CategoryApp extends Activity {
    public  static final int RequestPermissionCode1  = 3 ;
    GridView gridview;
    Activity activity;

    public static String[] catName = {
            "Parking Info",
            "Car Pooling",
            "Events",
            "Shuttle Service",
            "Iqama Timing",
            "See Something Say Somthing",
            "Profile",
            "Admin",
    };
    public static int[] catImages = {
            R.drawable.ic_car_parking,
            R.drawable.car_pool,
            R.drawable.ic_events,
            R.drawable.ic_bus_shuttle,
            R.drawable.ic_moon_star,
            R.drawable.ic_no_parking_sign,
            R.drawable.ic_user,
            R.drawable.ic_admin_with_cogwheels};


    LinearLayout car_pooling,car_parking_map,category_report,category_shuttle,category_admin,category_prayer,category_event;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category);

        EnableRuntimePermissionToAccessExternelStorage();



        gridview = (GridView) findViewById(R.id.category_girdview);
        gridview.setAdapter(new CustomAdapter(this, catName, catImages));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                openActivity(i);
            }
        });
    }

    private void openActivity(int i) {
        switch (i)
        {
            case 0:
                startActivity(new Intent(CategoryApp.this,CarParkingMapMyMasjid
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);



                activity = CategoryApp.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));


                break;
            case 1:
                startActivity(new Intent(CategoryApp.this,CategoryCarPooling
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                activity = CategoryApp.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

                break;
            case 2:
                startActivity(new Intent(CategoryApp.this,EventListActivity
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);



                activity = CategoryApp.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

                break;
            case 3:
                startActivity(new Intent(CategoryApp.this,BusShuttleListActivity
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                activity = CategoryApp.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

                break;
            case 4:
                startActivity(new Intent(CategoryApp.this,PrayerActivity
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                activity = CategoryApp.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

                break;
            case 5:
                startActivity(new Intent(CategoryApp.this,Report
                        .class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                activity = CategoryApp.this;
                Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

                break;
            case 6:

                Toast.makeText(CategoryApp.this, "coming soon",Toast.LENGTH_SHORT).show();
                break;
            case 7:
                if(AppPreferences.getAdminStatus(CategoryApp.this).equals("Y")){
                        startActivity(new Intent(CategoryApp.this,CategoryAdmin.class));
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


                    activity = CategoryApp.this;
                    Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                    Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                    Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                    Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                    Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                    Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                    Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                    Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                    Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                    Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                    Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

                } else if(AppPreferences.getAdminStatus(CategoryApp.this).equals("N")){
                    startActivity(new Intent(CategoryApp.this,AdminRegister.class));



                    activity = CategoryApp.this;
                    Log.e("AdminStatus>>>>>>>>>>>>", AppPreferences.getAdminStatus(activity));
                    Log.e("Lattitiued>>>>>>>>>>>>", AppPreferences.getLatitiude(activity));
                    Log.e("Lattitueda>>>>>>>>>>>>", AppPreferences.getLatitiudea(activity));
                    Log.e("Logtitude>>>>>>>>>>>>", AppPreferences.getLogtitude(activity));
                    Log.e("Logtitudea>>>>>>>>>>>>", AppPreferences.getLogtitudea(activity));
                    Log.e("MasjidAdress>>>>>>>>>>", AppPreferences.getMajidAddress(activity));
                    Log.e("MasjidName>>>>>>>>>>", AppPreferences.getMajidName(activity));
                    Log.e("MasjidID>>>>>>>>>>", AppPreferences.getMasjidId(activity));
                    Log.e("MobileNumber>>>>>>>>>>", AppPreferences.getMobileNumber(activity));
                    Log.e("RandomeID>>>>>>>>>>", AppPreferences.getRandomId(activity));
                    Log.e("UserID>>>>>>>>>>", AppPreferences.getUserId(activity));

                }
                break;
        }
    }

    private void adminValidation() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(CategoryApp.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.admin_pin_dialog);
        // Set dialog title
        dialog.setTitle("Admin");
        dialog.show();


        // set values for custom dialog components - text, image and button
        final EditText text = (EditText) dialog.findViewById(R.id.textDialog);


        Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(text.getText().toString().equals("101")){
                    startActivity(new Intent(CategoryApp.this,CategoryAdmin.class));
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    // Close dialog
                    dialog.dismiss();
                }else{
                    Toast.makeText(CategoryApp.this,"Wroung Password",Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }

            }
        });

    }

    private void EnableRuntimePermissionToAccessExternelStorage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(CategoryApp.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
        {

            // Printing toast message after enabling runtime permission.
            Toast.makeText(CategoryApp.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(CategoryApp.this,new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, RequestPermissionCode1);

        }
    }




    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {
            case RequestPermissionCode1:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

//                    Toast.makeText(Report.this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(CategoryApp.this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }

    }
}


