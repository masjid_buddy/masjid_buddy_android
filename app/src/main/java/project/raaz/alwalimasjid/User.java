package project.raaz.alwalimasjid;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rasheed on 10/20/2017.
 */

public class User {

    @SerializedName("name")
    private String name;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("password")
    private String password;
    @SerializedName("area")
    private String area;
    @SerializedName("email")
    private String email;



    public User(String name, String mobile, String password, String area) {
        this.name = name;
        this.mobile = mobile;
        this.password = password;
        this.area = area;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
