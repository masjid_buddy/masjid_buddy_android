package project.raaz.alwalimasjid;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rasheed on 11/3/2017.
 */

public class EventsList {
    @SerializedName("id")
    private String id;
    @SerializedName("event_title")
    private String event_title;
    @SerializedName("description")
    private String description;
    @SerializedName("date")
    private String date;
    @SerializedName("time")
    private String time;
    @SerializedName("location")
    private String location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
