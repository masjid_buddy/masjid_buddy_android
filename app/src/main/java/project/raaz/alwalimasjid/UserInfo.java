package project.raaz.alwalimasjid;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rasheed on 11/2/2017.
 */

public class UserInfo {
    @SerializedName("userinfo")
    private List<UserValues> userinfo;

    public List<UserValues> getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(List<UserValues> userinfo) {
        this.userinfo = userinfo;
    }
}
