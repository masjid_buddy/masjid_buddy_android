package project.raaz.alwalimasjid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


//import com.google.firebase.iid.FirebaseInstanceId;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.List;

import project.raaz.alwalimasjid.new_activities.AdminActivity;
import project.raaz.alwalimasjid.utilities.AppPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rasheed on 10/16/2017.
 */

public class Login extends Activity{

    private static final String TAG = "Login";
    private static final int REQUEST_SIGNUP = 0;
    private ProgressDialog pDialog;
    TextInputLayout textInputLayout_mobile;
    TextInputEditText textInputEditText_mobile;
    TextInputLayout textInputLayout_password;
    TextInputEditText textInputEditText_passwrd;

    TextView register;
    Button submit;

    String action;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        textInputLayout_mobile = (TextInputLayout)findViewById (R.id.lgn_lMobile);
        textInputEditText_mobile = (TextInputEditText)findViewById (R.id.lgn_txtMobile);
        textInputLayout_password = (TextInputLayout)findViewById (R.id.lgn_lPassword);
        textInputEditText_passwrd = (TextInputEditText)findViewById (R.id.lgn_txtPassword);

        register = findViewById(R.id.lgn_txt_register);
        submit = findViewById(R.id.lgn_btn_submit);

        Bundle bundle = getIntent().getExtras();
        action = bundle.getString("activity");

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,Register.class).putExtra("activity",action));
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                login();
            }

        });

    }

    private void login() {
        Log.d(TAG, "Login");

        if (validate() == false) {
            onLoginFailed();
            return;
        }
        if(textInputEditText_mobile.getText().toString().equals("101"))
        {
            AppPreferences.setUserId(Login.this,"3");


            startActivity(new Intent(Login.this,CategoryMain
                    .class));
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }else{
            loginByServer();
        }

    }

    private void loginByServer() {
        pDialog = new ProgressDialog(Login.this,
                R.style.AppTheme_Dark_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Creating Account...");
        pDialog.setCancelable(false);

        showpDialog();

        final String mobile = textInputEditText_mobile.getText().toString();
        String password = textInputEditText_passwrd.getText().toString();


        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> userCall = service.ProfileLogin(mobile,password);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hidepDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getMessage());
                if(response.body().getStatus().equals("success")) {
                    List<UserValues> re = response.body().getUserinfo();
                    String user_id = re.get(0).user_id;
                    AppPreferences.setUserId(Login.this,user_id);
                    AppPreferences.setMobileNumber(Login.this,mobile);
                    AppPreferences.setUserName(Login.this,"New User");

                    if(action.equals("carpool")){
                        callAddDevice();
//                        finish();
                    }else{
                        callAddDevice();
//                        finish();
//                        startActivity(new Intent(Login.this, AdminActivity.class));
//                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    }
                }else {
                    Toast.makeText(Login.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });
    }


    public void onLoginSuccess() {
        submit.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        submit.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String mobile = textInputEditText_mobile.getText().toString();
        String password = textInputEditText_passwrd.getText().toString();

        if (mobile.isEmpty()) {
            textInputLayout_mobile.setError("Enter Mobile Number");
            requestFocus(textInputEditText_mobile);
            valid = false;
        } else {
            textInputLayout_mobile.setError(null);
        }

        if (password.isEmpty()) {
            textInputLayout_password.setError("Password is empty");
            requestFocus(textInputEditText_passwrd);
            valid = false;
        } else {
            textInputLayout_password.setError(null);
        }

        return valid;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void callAddDevice() {
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();


        APIService service = ApiClinet.getClient().create(APIService.class);

        Call<MSG> usercall = service.adddevice(AppPreferences.getUserId(Login.this), refreshedToken, "android");

        usercall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {

                Log.d("onResponse", "" + response.body());
                if (response.body().getStatus().equals("success")) {
                    finish();
                } else {
                    finish();
                }

            }


            @Override
            public void onFailure(Call<MSG> call, Throwable t) {

            }
        });
    }


}
